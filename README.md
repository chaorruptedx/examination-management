# Examination Management


## Requirements

* Apache
* PHP 7
* MySQL
* Composer - https://getcomposer.org/
* Git - https://git-scm.com/
* Brain


## Installation

* Download source code => ```git clone https://gitlab.com/chaorruptedx/examination-management.git```
* In project directory, open terminal, type => ```init```, then choose ```dev``` as environment 
* In project directory, open terminal, type => ```composer update```
* Import old database from `database/examination_management.sql` into your MySQL
* Update database configuration. Edit the file `common/config/main-local.php` with real data, for example:
```php
return [
    'class' => 'yii\db\Connection',
    // 'dsn' => 'mysql:host=localhost;dbname=examination_management', // For Old Database
    'dsn' => 'mysql:host=localhost;dbname=examination_management_v1', // For New Database
    'username' => 'root',
    'password' => '',
    'charset' => 'utf8',
];
```