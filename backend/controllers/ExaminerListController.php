<?php

namespace backend\controllers;

use Yii;
use common\models\Examiner;
use common\models\ExaminerSearch;
use yii\helpers\Url;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\Json;

/**
 * ExaminerListController implements the CRUD actions for Examiner model.
 */
class ExaminerListController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    // 'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function init()
    {    
        if (Yii::$app->user->identity->category != 'admin') {
            
            return $this->redirect(str_replace('/admin', '', Url::base(true)));
        }
    }

    /**
     * Lists all Examiner models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ExaminerSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Examiner model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Examiner model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $datamodal = ['confirmtxt'=>'Are you sure to save examiners?'];

        $model = new Examiner();

            if ($model->load(Yii::$app->request->post())) {
                
                $model->category = '1st_examiner';
                $model->id_user = $model->first;
                $model->save(false);

                $modelExaminer = new Examiner();
                $modelExaminer->category = '2nd_examiner';
                $modelExaminer->id_course = $model->id_course;
                $modelExaminer->id_session = $model->id_session;
                $modelExaminer->id_user = $model->second;
                $modelExaminer->save(false);

                return $this->redirect(['index']);
            }

        if (Yii::$app->request->isAjax) {
            return $this->renderAjax('create', [
                'model' => $model,
                'datamodal' => $datamodal,
            ]);
        } else {
            return $this->render('create', [
                'model' => $model,
                'datamodal' => $datamodal,
            ]);
        }
    }

    /**
     * Updates an existing Examiner model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Examiner model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        $model->status = '-1';
        $model->save(false);

        return $this->redirect(['index']);
    }

    public function actionGetCourseCodeName()
    {
        $id_course = Yii::$app->request->post('course');
        $id_session = Yii::$app->request->post('session');

        $modelExaminer = Examiner::find()->where(['id_course' => $id_course, 'id_session' => $id_session, 'status' => '1'])->one();

            // for ($i = 0; $i < 10000000; $i++){

            // }

            if(!empty($modelExaminer)) {
                $data = 1;
            } else {
                $data = 0;
            }

        return Json::encode($data);
    }

    /**
     * Finds the Examiner model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Examiner the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Examiner::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
