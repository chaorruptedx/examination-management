<?php

namespace backend\controllers;

use Yii;
use yii\base\Model;
use common\models\Attachment;
use common\models\Examiner;
use common\models\Submission;
use common\models\Vetting;
use common\models\FinalPendingExaminationUnitSubmissionSearch;
use yii\helpers\Url;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * FinalPendingExaminationUnitSubmissionController implements the CRUD actions for Examiner model.
 */
class FinalPendingExaminationUnitSubmissionController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function init()
    {    
        if (Yii::$app->user->identity->category != 'admin') {
            
            return $this->redirect(str_replace('/admin', '', Url::base(true)));
        }
    }

    /**
     * Lists all Examiner models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new FinalPendingExaminationUnitSubmissionSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionExaminationUnitSubmission($id_submission)
    {
        $model = Submission::find()
            ->where([
                'id' => $id_submission
            ])
            ->one();

        $model->scenario = Submission::SCENARIO_EXAMINATION_UNIT_SUBMISSION;

        $modelVettings = Vetting::find()
            ->where([
                'id_submission' => $id_submission,
                'status' => '1',
            ])
            ->orderBy(['questions_set' => SORT_ASC])
            ->all();

        $modelVettings[0]->scenario = Vetting::SCENARIO_EXAMINATION_UNIT_SUBMISSION;
        $modelVettings[1]->scenario = Vetting::SCENARIO_EXAMINATION_UNIT_SUBMISSION;

        $modelAttachmentsView = [new Attachment()]; // Array Model
        $modelAttachmentsView[0] = Attachment::find()
            ->where([
                'id_vetting' => $modelVettings[0]->id,
                'category' => 'question_set_a',
                'status' => '1',
            ])
            ->one(); // Question Set A
        $modelAttachmentsView[1] = Attachment::find()
            ->where([
                'id_vetting' => $modelVettings[0]->id,
                'category' => 'answer_set_a',
                'status' => '1',
            ])
            ->one(); // Answer Set A
        $modelAttachmentsView[2] = Attachment::find()
            ->where([
                'id_vetting' => $modelVettings[1]->id,
                'category' => 'question_set_b',
                'status' => '1',
            ])
            ->one(); // Question Set B
        $modelAttachmentsView[3] = Attachment::find()
            ->where([
                'id_vetting' => $modelVettings[1]->id,
                'category' => 'answer_set_b',
                'status' => '1',
            ])
            ->one(); // Answer Set B
        
        if ($model->load(Yii::$app->request->post()) && Model::loadMultiple($modelVettings, Yii::$app->request->post()) && Model::validateMultiple($modelVettings)) {

            $model->d_date_2 = date('Y-m-d');
            $model->e_date_1 = date('Y-m-d');
            $model->progress_status = 'Examination Unit';
            $model->updated_at = date('Y-m-d h:i:s', time());
            
            $model->save();

            if ($model->chosen_set == 'a') {

                $modelVettings[0]->chosen_set = '1';
                $modelVettings[1]->chosen_set = '0';

            } elseif ($model->chosen_set == 'b') {

                $modelVettings[0]->chosen_set = '0';
                $modelVettings[1]->chosen_set = '1';
            }

            $modelVettings[0]->date_2 = date('Y-m-d');
            $modelVettings[0]->updated_at = date('Y-m-d h:i:s', time());
            
            $modelVettings[0]->save();

            $modelVettings[1]->date_2 = date('Y-m-d');
            $modelVettings[1]->updated_at = date('Y-m-d h:i:s', time());

            $modelVettings[1]->save();
            
            Yii::$app->session->setFlash('success', 'Form has been submitted to Examination Unit.');

            return $this->redirect(['/progress-status/index']);
        }

        return $this->render('examination-unit-submission', [
            'model' => $model,
            'modelVettings' => $modelVettings,
            'modelAttachmentsView' => $modelAttachmentsView,
        ]);
    }

    /**
     * Finds the Examiner model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Examiner the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Examiner::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
