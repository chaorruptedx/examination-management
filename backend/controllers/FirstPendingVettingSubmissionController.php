<?php

namespace backend\controllers;

use Yii;
use yii\helpers\Url;
use common\models\Attachment;
use common\models\Examiner;
use common\models\Submission;
use common\models\Vetting;
use common\models\FirstPendingVettingSubmissionSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * FirstPendingVettingSubmissionController implements the CRUD actions for Examiner model.
 */
class FirstPendingVettingSubmissionController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function init()
    {    
        if (Yii::$app->user->identity->category != 'admin') {
            
            return $this->redirect(str_replace('/admin', '', Url::base(true)));
        }
    }

    /**
     * Lists all Examiner models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new FirstPendingVettingSubmissionSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionVettingSubmission($id_submission)
    {
        $model = Submission::find()
            ->where([
                'id' => $id_submission
            ])
            ->one();

        $model->scenario = Submission::SCENARIO_VETTING_SUBMISSION;

        $modelVettings = Vetting::find()
            ->where([
                'id_submission' => $id_submission,
                'status' => '1',
            ])
            ->orderBy(['questions_set' => SORT_ASC])
            ->all();

        $modelAttachments = [new Attachment()]; // Array Model
        $modelAttachments[0] = Attachment::find()
            ->where([
                'id_vetting' => $modelVettings[0]->id,
                'category' => 'question_set_a',
                'status' => '1',
            ])
            ->one(); // Question Set A
        $modelAttachments[1] = Attachment::find()
            ->where([
                'id_vetting' => $modelVettings[0]->id,
                'category' => 'answer_set_a',
                'status' => '1',
            ])
            ->one(); // Answer Set A
        $modelAttachments[2] = Attachment::find()
            ->where([
                'id_vetting' => $modelVettings[1]->id,
                'category' => 'question_set_b',
                'status' => '1',
            ])
            ->one(); // Question Set B
        $modelAttachments[3] = Attachment::find()
            ->where([
                'id_vetting' => $modelVettings[1]->id,
                'category' => 'answer_set_b',
                'status' => '1',
            ])
            ->one(); // Answer Set B

        if ($model->load(Yii::$app->request->post())) {

            $model->a_date_2 = date('Y-m-d');
            $model->b_date_1 = date('Y-m-d');
            $model->progress_status = 'Vetting';
            $model->updated_at = date('Y-m-d h:i:s', time());

            $model->save();

            Yii::$app->session->setFlash('success', 'Form has been submitted to '.$model->secondExaminer->user->username.' (2nd Examiner).');

            return $this->redirect(['/progress-status/index']);
        }

        return $this->render('vetting-submission', [
            'model' => $model,
            'modelVettings' => $modelVettings,
            'modelAttachments' => $modelAttachments,
        ]);
    }

    /**
     * Finds the Examiner model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Examiner the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Examiner::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
