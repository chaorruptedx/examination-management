<?php

namespace backend\controllers;

use Yii;
use yii\helpers\Url;
use common\models\User;
use common\models\UserListSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use common\models\UserRegister;

/**
 * UserListController implements the CRUD actions for User model.
 */
class UserListController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    // 'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function init()
    {    
        if (Yii::$app->user->identity->category != 'admin') {
            
            return $this->redirect(str_replace('/admin', '', Url::base(true)));
        }
    }

    /**
     * Lists all User models.
     * @return mixed
     */
    public function actionIndex($category='admin')
    {
        $searchModel = new UserListSearch();
        $searchModel->category = $category;

        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('main', [
            'category' => $category,
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single User model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new User model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $datamodal = ['confirmtxt'=>'Are you sure to save this user?'];

        $model = new UserRegister();

        if ($model->load(Yii::$app->request->post()) && $model->signup()) {
            return $this->redirect(['index', 'category' => $model->category]);
        }

        if (Yii::$app->request->isAjax) {
            return $this->renderAjax('create', [
                'model' => $model,
                'datamodal' => $datamodal,
            ]);
        } else {
            return $this->render('create', [
                'model' => $model,
                'datamodal' => $datamodal,
            ]);
        }
    }

    /**
     * Updates an existing User model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $datamodal = ['confirmtxt'=>'Are you sure to save this user?'];

        $model = $this->findModel($id);
        $model->scenario = User::SCENARIO_FORMAT; 

        if ($model->load(Yii::$app->request->post()) && $model->signup()) {
            return $this->redirect(['index', 'category' => $model->category]);
        }

        if (Yii::$app->request->isAjax) {
            return $this->renderAjax('update', [
                'model' => $model,
                'datamodal' => $datamodal,
            ]);
        } else {
            return $this->render('update', [
                'model' => $model,
                'datamodal' => $datamodal,
            ]);
        }
    }

    /**
     * Deletes an existing User model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the User model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return User the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = User::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
