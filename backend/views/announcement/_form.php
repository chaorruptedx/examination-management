<?php

use yii\helpers\Html;
use kartik\form\ActiveForm;
use kartik\select2\Select2;
?>

<div class="announcement-form">

<style>
.btn-form{
    padding-right: 16px;
}
</style>

    <?php 
    
        $form = ActiveForm::begin([
            'id' => 'id_announcement',
            'type' => ActiveForm::TYPE_HORIZONTAL,
            'formConfig' => ['labelSpan' => 3, 'deviceSize' => ActiveForm::SIZE_SMALL],

            'validateOnSubmit' => true,
            'enableAjaxValidation' => false,
            'enableClientValidation'=>true,
        ]);
    
    ?>
    
    <?= $form->field($model, 'announcement')->textArea(['style' => 'height: 174px !important; width: 625px !important; resize: none;', 'value' => $model->announcement])->label('Announcement') ?>

    <?php 
        if(!empty($model->status)) {
            $val = $model->status;
        } else {
            $val = 1;
        }
        
    echo $form->field($model, 'status')->label('Status')->widget(Select2::classname(), [
        'data' => array('1' => 'Active', '-1' => 'Inactive'),
        'options' => [
            'placeholder' => 'Select..',
            'value' => $val,
        ],
        'pluginOptions' => [
            'allowClear' => true,
        ],
    ]); ?>

    <div class="form-group highlight-addon field-course-created_at">
        <label class="control-label has-star col-sm-3" for="course-created_at">Created at</label>
            <div class="col-sm-9">
                <input type="text" id="clock" class="form-control"  disabled>
                <div class="help-block"></div>
            </div>
    </div>

    <div class="form-group text-right btn-form">
        <?= Html::button(Yii::t('app', 'Save'), [
            'type' => 'button',
            'name' => 'btnsave',
            'data-idform' => 'id_announcement',
            'data-confirmcustom' => $datamodal['confirmtxt'],
            'class' => 'btn btn-success btnsavecustom' ])
        ?>
        <?php
            if (Yii::$app->request->isAjax) {
                echo Html::button(Yii::t('app', 'Cancel'), [
                    'data-dismiss'=>"modal",
                    'class' => 'btn btn btn-default'
                ]);
            } else {
                echo Html::a(Yii::t('app', 'Cancel'), ['index'], ['class' => 'btn btn btn-default']);
            }
        ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<?php
$script = <<< JS
  var time = setInterval(function() {
    myTimer();
  }, 1000);

  function myTimer() {
    var d = new Date();
    $('#clock').val(d.toLocaleString());
    // document.getElementById("clock").innerHTML = d.toLocaleString();
  }

JS;
$this->registerJs($script);
?>
