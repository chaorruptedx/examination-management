<?php

use yii\helpers\Html;
use common\components\CustomDialog;

echo CustomDialog::Dialogcus($type ="TYPE_SUCCESS", $title='Confirmation', $btnOKClass='btn-success','krajeeDialogCust');

$this->params['breadcrumbs'][] = ['label' => 'Announcement', 'url' => ['index']];
$this->params['breadcrumbs'][] = 'Update Announcement';
?>
<div class="announcement-update">

    <?= $this->render('_form', [
        'model' => $model,
        'datamodal' => $datamodal,
    ]) ?>

</div>

<?php
$file=\Yii::$app->basePath . '/web/js/custommodal.js';
$file2=file_get_contents($file);

$this->registerJs($file2);
?>