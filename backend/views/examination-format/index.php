<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use yii\widgets\Pjax;

use common\components\CustomDialog;

Pjax::begin(['id' => 'dialogresfresh']);
echo CustomDialog::Dialogcus($type ="TYPE_DANGER", $title='Remove Attachment', $btnOKClass='btn-danger',$libName='krajeeDialogdelete');
Pjax::end();

$this->title = 'Examination Format';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="attachment-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Upload Examination Format', ['create'], ['class' => 'btnmodalcustom btn btn-success', 'data-title' => 'Upload Examination Format']) ?>
    </p>

    <?php Pjax::begin(['id' => 'idPjaxGridViewAttachment', 'timeout' => false, 'enablePushState' => false]); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'id' => 'idPjaxGridViewAttachment',
        'pjax' => true,
        'pjaxSettings' => [
            'options' => [
                'enablePushState' => false,
            ]
        ],

        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,

        'emptyCell' => '-',
        'emptyText' => 'No results found.',
        'formatter' =>  [
                'class' => 'yii\i18n\Formatter',
                'nullDisplay' => '-'
            ],
        'layout'=> Yii::$app->params['gridviewCustomExamination']['layout2'],
        'summary' => 'Showing <b>{begin}-{end}</b> of <b>{totalCount}</b> Attachments.',
        'pager' => Yii::$app->params['pagecustomexamination'],

        'columns' => [
            [
                'header' => 'No.',
                'class' => 'kartik\grid\SerialColumn'
            ],

            [
                'label' => 'File Name',
                'attribute' => 'name',
            ],

            [
                'label' => 'Upload By',
                'attribute' => 'personal_name',
                'value' => 'user.username',
            ],

            [
                
                'header' => 'Actions',
                'class' => 'kartik\grid\ActionColumn',
                'template' => '{delete}',
                'buttons' => [
                    
                    'delete' => function ($url, $model, $key) {
                        $options = [
                            'id'=>'course-delete',
                            'title_delete' => Yii::t('yii', 'Remove'),
                            'aria-label' => Yii::t('yii', 'Remove'),
                            'data-pjax' => '0',

                            'class'=>'deletecustomgrid',
                            'data-confirmcustom'=>'Are you sure to remove this examination format?',
                            'data-url'=>$url,

                        ];

                        return Html::a('<span class="glyphicon glyphicon-trash"></span>', $url, $options);

                    }, 

                ]
            ],
        ],
    ]); ?>

    <?php Pjax::end(); ?>

</div>
