<?php

use yii\helpers\Html;
use kartik\form\ActiveForm;
use yii\helpers\Url;
use kartik\select2\Select2;
use common\models\Bankstatus;
?>

<div class="course-form">

<style>
.btn-form{
    padding-right: 16px;
}
</style>

    <?php 
    
        $form = ActiveForm::begin([
            'id' => 'id_course',
            'type' => ActiveForm::TYPE_HORIZONTAL,
            'formConfig' => ['labelSpan' => 3, 'deviceSize' => ActiveForm::SIZE_SMALL],

            'validateOnSubmit' => true,
            'enableAjaxValidation' => false,
            'enableClientValidation'=>true,
        ]);
    
    ?>

    <div class='examiner_error'></div>

    <div class='course_error'></div>

    <?= $form->field($model, 'id_session')->widget(Select2::classname(), [
        'data' => Bankstatus::listSessions(),
        'options' => ['id' => 'session_check', 'placeholder' => 'Select..'],
        'pluginOptions' => [
            'highlight' => true,
            'allowClear' => true,
        ],
    ])->label('Session'); ?>

    <?= $form->field($model, 'id_course')->widget(Select2::classname(), [
        'data' => \common\models\Bankstatus::listCourse(),
        'pluginOptions' => ['highlight' => true, 'allowClear' => true],
        'options' => ['placeholder' => 'Select..', 'id' => 'course_check']
        ])->label('Course');
    ?>

    <?= $form->field($model, 'first')->widget(Select2::classname(), [
        'data' => \common\models\Bankstatus::listUsers(),
        'pluginOptions' => ['highlight' => true, 'allowClear' => true],
        'options' => ['placeholder' => 'Select..', 'id' => 'examiner_first']
        ])->label('1st Examiner');
    ?>

    <?= $form->field($model, 'second')->widget(Select2::classname(), [
        'data' => \common\models\Bankstatus::listUsers(),
        'pluginOptions' => ['highlight' => true, 'allowClear' => true],
        'options' => ['placeholder' => 'Select..', 'id' => 'examiner_second']
        ])->label('2nd Examiner');
    ?>

    <!-- <div class="form-group highlight-addon field-course-created_at">
        <label class="control-label has-star col-sm-3" for="course-created_at">Created at</label>
            <div class="col-sm-9">
                <input type="text" id="clock" class="form-control"  disabled>
                <div class="help-block"></div>
            </div>
    </div> -->

    <div class="form-group text-right btn-form">
        <?= Html::button(Yii::t('app', 'Save'), [
            'type' => 'button',
            'name' => 'btnsave',
            'data-idform' => 'id_course',
            'data-confirmcustom' => $datamodal['confirmtxt'],
            'class' => 'btn btn-success btnsavecustomv2' ]) 
        ?>
        <?php
            if (Yii::$app->request->isAjax) {
                echo Html::button(Yii::t('app', 'Cancel'), [
                    'data-dismiss'=>"modal",
                    'class' => 'btn btn btn-default'
                ]);
            } else {
                echo Html::a(Yii::t('app', 'Cancel'), ['index'], ['class' => 'btn btn btn-default']);
            }
        ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<?php
$urlCourseCodeName = Url::to(['get-course-code-name']);

$script = <<< JS

var first = '';
var second = '';

var id_session = '';
var id_course = '';

var exist = '';

    $('.field-examiner_first').on('change',function(){

        first = $('.field-examiner_first :selected').val();

    });

    $('.field-examiner_second').on('change',function(){

        second = $('.field-examiner_second :selected').val();

    });

    $('.field-session_check').on('change',function(){

        id_session = $(".field-session_check :selected").val();

    });

    $('.field-course_check').on('change',function(){

        id_course = $('.field-course_check :selected').val();
            
    });

    $('.btnsavecustomv2').unbind().click(function() {

        if(first == second){

            $('.ayam').remove();

            $('.examiner_error').append("<div class='alert alert-danger has-error ayam' role='alert'><strong><span><i class='fa fa-warning'></i></span> Warning!</strong> Name of Examiner cannot be same.</div>");

        } else if(second == first){

            $('.ayam').remove();

            $('.examiner_error').append("<div class='alert alert-danger has-error ayam' role='alert'><strong><span><i class='fa fa-warning'></i></span> Warning!</strong> Name of Examiner cannot be same.</div>");

        } else {

            $('.ayam').remove();
            
        }

            var request = $.ajax({
				url: "$urlCourseCodeName",
				method: "POST",
				data: {course: id_course, session: id_session},
				dataType: "json"
			});

			request.done(function(data) {

                if(data == 1) {

                    $('.itik').remove();

                    $('.course_error').append("<div class='alert alert-danger has-error itik' role='alert'><strong><span><i class='fa fa-warning'></i></span> Warning!</strong> This course already exist in this session.</div>");

                } else {

                    $('.itik').remove();

                }

            });

            var thisBtn = this;

        $.when(request).then(function( data, textStatus, jqXHR ) {
   
        var idform=$( thisBtn ).data( "idform" );
        var confirmcustom=$( thisBtn ).data( "confirmcustom" );

        $('#'+idform).yiiActiveForm('validate', true);
        $('#'+idform).yiiActiveForm('validate');

        $('form#'+idform).on("submit", function(e){
            return false;
        }); 

        $('form#'+idform).one('afterValidate', function(event, message){
        // --- START VALIDATE ---

            if($('#'+idform).find('.has-error').length === 0){
            // --- START ---

                krajeeDialogCust.confirm(confirmcustom, function (result) {
                    $(".bootstrap-dialog-footer-buttons").addClass( "" );

                        if (result) { // ok button was pressed
                        // --- Execute your code for confirmation ---

                            $('#'+idform).unbind('submit').submit();

                                $.blockUI({
                                    message: 'Loading ...',
                                    css: {
                                            border: 'none', 
                                            padding: '15px', 
                                            backgroundColor: '#000', 
                                            '-webkit-border-radius': '10px', 
                                            '-moz-border-radius': '10px', 
                                            opacity: .5, 
                                            color: '#fff' 
                                        },
                                    baseZ: 4000
                                });

                        // --- Execute your code for confirmation ---
                        } else {

                            if($('#dialogresfresh').length > 0){
                                $.pjax.reload({container:'#dialogresfresh'}); //refresh the grid
                            }
                        
                        }

                });

            // --- END ---
            }

        // --- END VALIDATE ---
        });

    });

});

JS;
$this->registerJs($script);
?>
