<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use yii\widgets\Pjax;

use common\components\CustomDialog;

Pjax::begin(['id' => 'dialogresfresh']);
echo CustomDialog::Dialogcus($type ="TYPE_DANGER", $title='Remove Examiner', $btnOKClass='btn-danger',$libName='krajeeDialogdelete');
Pjax::end();

$this->title = 'Examiners';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="examiner-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Examiner', ['create'], ['class' => 'btnmodalcustom btn btn-success', 'data-title' => 'Create Examiner']) ?>

        <?= Html::a('Manage Session', ['session/index'], ['class' => 'btn btn-success', 'data-title' => 'Manage Session']) ?>
    </p>

    <?php Pjax::begin(['id' => 'idPjaxGridViewExaminer', 'timeout' => false, 'enablePushState' => false]); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'id' => 'idPjaxGridViewExaminer',
        'pjax' => true,
        'pjaxSettings' => [
            'options' => [
                'enablePushState' => false,
            ]
        ],

        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,

        'toggleDataOptions' => [
            'all' => Yii::$app->params['gridviewToggleDataOptions']['all'],
            'page' => Yii::$app->params['gridviewToggleDataOptions']['page'],
        ],
        'export' => [
            'header' => Yii::$app->params['gridviewExportCustom']['header'],
            'label' => Yii::$app->params['gridviewExportCustom']['label'],
            'options' => ['class' => Yii::$app->params['gridviewExportCustom']['optionClass']],
            'showConfirmAlert' => Yii::$app->params['gridviewExportCustom']['showConfirmAlert'],
            'target' => GridView::TARGET_SELF
        ],
        'exportConfig' => [
            GridView::EXCEL => [
                'label' => Yii::$app->params['gridviewExportCustom']['labelEXCEL'], 
                'filename' => $this->title,
            ],
        ], 
        'emptyCell' => '-',
        'emptyText' => 'No results found.',
        'formatter' =>  [
                'class' => 'yii\i18n\Formatter',
                'nullDisplay' => '-'
            ],
        'layout'=> Yii::$app->params['gridviewCustomExamination']['layout'],
        'summary' => 'Showing <b>{begin}-{end}</b> of <b>{totalCount}</b> Examiners.',
        'pager' => Yii::$app->params['pagecustomexamination'],

        'columns' => [
            [
                'header' => 'No.',
                'class' => 'kartik\grid\SerialColumn'
            ],

            [
                'label' => 'Session',
                // 'attribute' => 'session',
                'mergeHeader' => true,
                'value' => function($model) {
                    
                    return date("F Y", strtotime($model['session']));

                },
            ],

            [
                'label' => 'Course Code',
                'attribute' => 'course_code',
            ],
            [
                'label' => 'Course Name',
                'attribute' => 'course_name',
            ],
            [
                'label' => '1st Examiner',
                'attribute' => 'examiner_one',
                'value' => function ($model) {

                    return $model['1st_examiner'];
                },
            ],
            [
                'label' => '2nd Examiner',
                'attribute' => 'examiner_two',
                'value' => function ($model) {

                    return $model['2nd_examiner'];
                },
            ],

            [
                
                'header' => 'Actions',
                'class' => 'kartik\grid\ActionColumn',
                'template' => '{delete}', //{view} {update}
                'buttons' => [
                    // 'view' => function ($url, $model, $key) {
                    //     $options = [
                    //         'title' => Yii::t('yii', 'Paparan'),
                    //         'aria-label' => Yii::t('yii', 'Paparan'),
                    //         'data-pjax' => '0',
                    //         'data-title' => 'Paparan Maklumat',
                    //         'class'=>'btnmodalcustom',
                    //     ];

                    //     return Html::a('<span class="glyphicon glyphicon-eye-open"></span>', $url, $options);
                    // },

                    // 'update' => function ($url, $model, $key) {
                    //     $options = [
                    //         'title' => Yii::t('yii', 'Kemaskini'),
                    //         'aria-label' => Yii::t('yii', 'Kemaskini'),
                    //         'data-pjax' => '0',
                    //         'data-title' => 'Kemas Kini Maklumat',
                    //         'class' => 'btnmodalcustom',
                    //     ];
                           
                    //     return Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url, $options);
                    // },     
                    
                    'delete' => function ($url, $model, $key) {
                        $options = [
                            'id'=>'examiner-delete',
                            'title_delete' => Yii::t('yii', 'Remove'),
                            'aria-label' => Yii::t('yii', 'Remove'),
                            'data-pjax' => '0',

                            'class'=>'deletecustomgrid',
                            'data-confirmcustom'=>'Are you sure to remove this examiner?',
                            'data-url'=>$url,

                        ];

                        return Html::a('<span class="glyphicon glyphicon-trash"></span>', $url, $options);

                    }, 

                ]
            ],
        ],
    ]); ?>

    <?php Pjax::end(); ?>

</div>
