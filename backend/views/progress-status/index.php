<?php

use yii\helpers\Html;
use yii\widgets\Pjax;

use kartik\grid\GridView;
/* @var $this yii\web\View */
/* @var $searchModel common\models\ProgressStatusSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Examiners';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="examiner-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <?php Pjax::begin(['id' => 'idPjaxGridViewCourse', 'timeout' => false, 'enablePushState' => false]); ?>
    
    <?= GridView::widget([
        'id' => 'idPjaxGridViewCourse',
        'pjax' => true,
        'pjaxSettings' => [
            'options' => [
                'enablePushState' => false,
            ]
        ],

        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,

        'toggleDataOptions' => [
            'all' => Yii::$app->params['gridviewToggleDataOptions']['all'],
            'page' => Yii::$app->params['gridviewToggleDataOptions']['page'],
        ],
        'export' => [
            'header' => Yii::$app->params['gridviewExportCustom']['header'],
            'label' => Yii::$app->params['gridviewExportCustom']['label'],
            'options' => ['class' => Yii::$app->params['gridviewExportCustom']['optionClass']],
            'showConfirmAlert' => Yii::$app->params['gridviewExportCustom']['showConfirmAlert'],
            'target' => GridView::TARGET_SELF
        ],
        'exportConfig' => [
            GridView::EXCEL => [
                'label' => Yii::$app->params['gridviewExportCustom']['labelEXCEL'], 
                'filename' => $this->title,
            ],
        ], 
        'emptyCell' => '-',
        'emptyText' => 'No results found.',
        'formatter' =>  [
                'class' => 'yii\i18n\Formatter',
                'nullDisplay' => '-'
            ],
        'layout'=> Yii::$app->params['gridviewCustomExamination']['layout'],
        'summary' => 'Showing <b>{begin}-{end}</b> of <b>{totalCount}</b> Examiners.',
        'pager' => Yii::$app->params['pagecustomexamination'],

        'columns' => [
            [
                'header' => 'No.',
                'class' => 'kartik\grid\SerialColumn'
            ],

            [
                'label' => 'Course Code',
                'attribute' => 'course_code',
            ],

            [
                'label' => 'Course Name',
                'attribute' => 'course_name',
            ],

            [
                'label' => '1st Examiner',
                'attribute' => 'examiner_one',
                'value' => function ($model) {

                    return $model['1st_examiner'];
                },
            ],

            [
                'label' => '2nd Examiner',
                'attribute' => 'examiner_two',
                'value' => function ($model) {

                    return $model['2nd_examiner'];
                },
            ],

            [
                'label' => 'Progress Status',
                'attribute' => 'submission_progress_status',
            ],

            // [
                
            //     'header' => 'Actions',
            //     'class' => 'kartik\grid\ActionColumn',
            //     'template' => '{delete}', //{view} {update}
            //     'buttons' => [
                    // 'view' => function ($url, $model, $key) {
                    //     $options = [
                    //         'title' => Yii::t('yii', 'Paparan'),
                    //         'aria-label' => Yii::t('yii', 'Paparan'),
                    //         'data-pjax' => '0',
                    //         'data-title' => 'Paparan Maklumat',
                    //         'class'=>'btnmodalcustom',
                    //     ];

                    //     return Html::a('<span class="glyphicon glyphicon-eye-open"></span>', $url, $options);
                    // },

                    // 'update' => function ($url, $model, $key) {
                    //     $options = [
                    //         'title' => Yii::t('yii', 'Kemaskini'),
                    //         'aria-label' => Yii::t('yii', 'Kemaskini'),
                    //         'data-pjax' => '0',
                    //         'data-title' => 'Kemas Kini Maklumat',
                    //         'class' => 'btnmodalcustom',
                    //     ];
                           
                    //     return Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url, $options);
                    // },     
                    
                    // 'delete' => function ($url, $model, $key) {
                    //     $options = [
                    //         'id'=>'course-delete',
                    //         'title_delete' => Yii::t('yii', 'Remove'),
                    //         'aria-label' => Yii::t('yii', 'Remove'),
                    //         'data-pjax' => '0',

                    //         'class'=>'deletecustomgrid',
                    //         'data-confirmcustom'=>'Are you sure to remove this course?',
                    //         'data-url'=>$url,

                    //     ];

                    //     return Html::a('<span class="glyphicon glyphicon-trash"></span>', $url, $options);

                    // }, 

            //     ]
            // ],
        ],
    ]); ?>

    <?php Pjax::end(); ?>

    <br>
    
    <fieldset>
    <legend><h3><b>Progress Flow :</b></h3></legend>
	<table width="100%" border="0">
	  <tbody>
	    <tr>
	      <td align="center">
		  <font color="darkorange"><b>First Submission</b>&nbsp;</font>
	      <span class="glyphicon glyphicon-arrow-right"></span>
	      <font color="darkorange">&nbsp;<b>Vetting</b>&nbsp;</font>
	      <span class="glyphicon glyphicon-arrow-right"></span>
	      <font color="darkorange">&nbsp;<b>Correction</b>&nbsp;</font>
	      <span class="glyphicon glyphicon-arrow-right"></span>
	      <font color="darkorange">&nbsp;<b>Final Submission</b>&nbsp;</font>
	      <span class="glyphicon glyphicon-arrow-right"></span>
	      <font color="darkorange">&nbsp;<b>Examination Unit</b>&nbsp;</font>
	      <span class="glyphicon glyphicon-arrow-right"></span>
	      <font color="darkorange">&nbsp;<b>Completed</b></font></td>
	    </tr>
	  </tbody>
	</table>
  	</fieldset>

</div>
