<?php

use yii\helpers\Html;
use kartik\form\ActiveForm;
use kartik\date\DatePicker;
?>

<div class="session-form">

<style>
.btn-form{
    padding-right: 16px;
}
</style>

    <?php 
    
        $form = ActiveForm::begin([
            'id' => 'id_session',
            'type' => ActiveForm::TYPE_HORIZONTAL,
            'formConfig' => ['labelSpan' => 3, 'deviceSize' => ActiveForm::SIZE_SMALL],

            'validateOnSubmit' => true,
            'enableAjaxValidation' => false,
            'enableClientValidation'=>true,
        ]);
    
    ?>

    <?= $form->field($model, 'month')->widget(DatePicker::classname(), [ 
        'options' => ['placeholder' => 'Month'],
        'pluginOptions' => [
            'autoclose'=>true,
            'startView' => 'month',
            'minViewMode' => 'months',
            'format' => 'mm'
        ]])->label('Session');
    ?>
    <?= $form->field($model, 'year')->widget(DatePicker::classname(), [ 
        'options' => ['placeholder' => 'Year'],
        'pluginOptions' => [
            'autoclose'=>true,
            'startView' => 'year',
            'minViewMode' => 'years',
            'format' => 'yyyy'
        ]])->label('');
    ?>

    <div class="form-group highlight-addon field-course-created_at">
        <label class="control-label has-star col-sm-3" for="course-created_at">Created at</label>
            <div class="col-sm-9">
                <input type="text" id="clock" class="form-control"  disabled>
                <div class="help-block"></div>
            </div>
    </div>

    <div class="form-group text-right btn-form">
        <?= Html::button(Yii::t('app', 'Save'), [
            'type' => 'button',
            'name' => 'btnsave',
            'data-idform' => 'id_session',
            'data-confirmcustom' => $datamodal['confirmtxt'],
            'class' => 'btn btn-success btnsavecustom' ])
        ?>
        <?php
            if (Yii::$app->request->isAjax) {
                echo Html::button(Yii::t('app', 'Cancel'), [
                    'data-dismiss'=>"modal",
                    'class' => 'btn btn btn-default'
                ]);
            } else {
                echo Html::a(Yii::t('app', 'Cancel'), ['index'], ['class' => 'btn btn btn-default']);
            }
        ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<?php
$script = <<< JS
  var time = setInterval(function() {
    myTimer();
  }, 1000);

  function myTimer() {
    var d = new Date();
    $('#clock').val(d.toLocaleString());
    // document.getElementById("clock").innerHTML = d.toLocaleString();
  }

JS;
$this->registerJs($script);
?>
