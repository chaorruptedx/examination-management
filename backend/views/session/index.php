<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use yii\widgets\Pjax;
use common\components\CustomDialog;

Pjax::begin(['id' => 'dialogresfresh']);
echo CustomDialog::Dialogcus($type ="TYPE_DANGER", $title='Remove Session', $btnOKClass='btn-danger',$libName='krajeeDialogdelete');
Pjax::end();

$this->title = 'Session';
$this->params['breadcrumbs'][] = ['label' => 'Examiners', 'url' => ['examiner-list/index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="session-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Session', ['create'], ['class' => 'btnmodalcustom btn btn-success', 'data-title' => 'Create Session']) ?>
    </p>

    <?php Pjax::begin(['id' => 'idPjaxGridViewSession', 'timeout' => false, 'enablePushState' => false]); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'id' => 'idPjaxGridViewSession',
        'pjax' => true,
        'pjaxSettings' => [
            'options' => [
                'enablePushState' => false,
            ]
        ],

        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,

        'emptyCell' => '-',
        'emptyText' => 'No results found.',
        'formatter' =>  [
                'class' => 'yii\i18n\Formatter',
                'nullDisplay' => '-'
            ],
        'layout'=> Yii::$app->params['gridviewCustomExamination']['layout2'],
        'summary' => 'Showing <b>{begin}-{end}</b> of <b>{totalCount}</b> Sessions.',
        'pager' => Yii::$app->params['pagecustomexamination'],

        'columns' => [
            [
                'header' => 'No.',
                'class' => 'kartik\grid\SerialColumn'
            ],

            [
                'label' => 'Session',
                // 'attribute' => 'session',
                'mergeHeader' => true,
                'value' => function($model) {
                    
                    return date("F Y", strtotime($model['session']));

                },
                // 'filter' => kartik\date\DatePicker::widget([
                //     'type' => kartik\date\DatePicker::TYPE_INPUT,
                //     'name' => 'SessionSearch[session]',
                //     'options' => ["placeholder"=>Yii::$app->request->get('SessionSearch')['session']],
                //     'value' => Yii::$app->request->get('SessionSearch')['session'],
                //     'pluginOptions' => [
                //         'format' => 'dd-mm-yyyy',
                //         'todayHighlight' => true,
                //     ]
                // ]),
            ],

            [
                'label' => 'Status',
                'attribute' => 'status',
                'value' => function($model) {
                    return $model->status == 1 ? 'Active' : 'Inactive';
                },
                'filter' => array('1' => 'Active', '-1' => 'Inactive'),
                'filterType' => GridView::FILTER_SELECT2,
                'filterInputOptions' => ['placeholder' => 'Select..'],
                'filterWidgetOptions' => [
                    'pluginOptions' => ['allowClear' => true],
                ],
                'format'=>'raw',
            ],

            [
                
                'header' => 'Actions',
                'class' => 'kartik\grid\ActionColumn',
                'template' => '{delete}',
                'buttons' => [
                    
                    'delete' => function ($url, $model, $key) {
                        $options = [
                            'id'=>'session-delete',
                            'title_delete' => Yii::t('yii', 'Remove'),
                            'aria-label' => Yii::t('yii', 'Remove'),
                            'data-pjax' => '0',

                            'class'=>'deletecustomgrid',
                            'data-confirmcustom'=>'Are you sure to remove this session?',
                            'data-url'=>$url,

                        ];

                        return Html::a('<span class="glyphicon glyphicon-trash"></span>', $url, $options);

                    }, 

                ]
            ],
        ],
    ]); ?>

    <?php Pjax::end(); ?>

</div>
