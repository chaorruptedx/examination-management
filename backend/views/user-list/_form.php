<?php

use yii\helpers\Html;
use kartik\form\ActiveForm;
use kartik\file\FileInput;
use kartik\select2\Select2;
use common\models\Bankstatus;

$listRole = Bankstatus::listRole();
?>


<style>
.btn-form{
    padding-right: 16px;
}
</style>

<div class="user-form">

    <?php 
    
        $form = ActiveForm::begin([
            'id' => 'user_form',
            'type' => ActiveForm::TYPE_HORIZONTAL,
            'formConfig' => ['labelSpan' => 3, 'deviceSize' => ActiveForm::SIZE_SMALL],

            'validateOnSubmit' => true,
            'enableAjaxValidation' => false,
            'enableClientValidation'=>true,
        ]);
    
    ?>

    <?= $form->field($model, 'username')->textInput(['autofocus' => true]) ?>

    <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>

    <?
    if($typeedit == 'update'){
        echo $form->field($model, 'new_password')->passwordInput()->label('Password');
    } else {
        echo $form->field($model, 'password')->passwordInput();
    }
    ?>

    <?= $form->field($model, 'category')->widget(Select2::classname(), [
        'data' => $listRole,
        'options' => ['placeholder' => 'Select a role ...'],
        'pluginOptions' => [
            'allowClear' => true,
        ],
    ]); ?>

    <div class="form-group text-right btn-form">
        <?= Html::button(Yii::t('app', 'Save'), [
            'type' => 'button',
            'name' => 'btnsave',
            'data-idform' => 'user_form',
            'data-confirmcustom' => $datamodal['confirmtxt'],
            'class' => 'btn btn-success btnsavecustom' ]) 
        ?>
        <?php
            if (Yii::$app->request->isAjax) {
                echo Html::button(Yii::t('app', 'Cancel'), [
                    'data-dismiss'=>"modal",
                    'class' => 'btn btn btn-default'
                ]);
            } else {
                echo Html::a(Yii::t('app', 'Cancel'), ['index'], ['class' => 'btn btn btn-default']);
            }
        ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
