<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use yii\widgets\Pjax;
use common\components\CustomDialog;

Pjax::begin(['id' => 'dialogresfresh']);
echo CustomDialog::Dialogcus($type ="TYPE_DANGER", $title='Remove User', $btnOKClass='btn-danger',$libName='krajeeDialogdelete');
Pjax::end();
?>

<div class="user-index">

    <p>
        <?= Html::a('Create User', ['create'], ['class' => 'btnmodalcustom btn btn-success', 'data-title' => 'Create User']) ?>
    </p>

    <?php Pjax::begin(['id' => 'idPjaxGridViewUser', 'timeout' => false, 'enablePushState' => false]); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'id' => 'idPjaxGridViewUser',
        'pjax' => true,
        'pjaxSettings' => [
            'options' => [
                'enablePushState' => false,
            ]
        ],

        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,

        'toggleDataOptions' => [
            'all' => Yii::$app->params['gridviewToggleDataOptions']['all'],
            'page' => Yii::$app->params['gridviewToggleDataOptions']['page'],
        ],
        'export' => [
            'header' => Yii::$app->params['gridviewExportCustom']['header'],
            'label' => Yii::$app->params['gridviewExportCustom']['label'],
            'options' => ['class' => Yii::$app->params['gridviewExportCustom']['optionClass']],
            'showConfirmAlert' => Yii::$app->params['gridviewExportCustom']['showConfirmAlert'],
            'target' => GridView::TARGET_SELF
        ],
        'exportConfig' => [
            GridView::EXCEL => [
                'label' => Yii::$app->params['gridviewExportCustom']['labelEXCEL'], 
                'filename' => $this->title,
            ],
        ], 
        'emptyCell' => '-',
        'emptyText' => 'No results found.',
        'formatter' =>  [
                'class' => 'yii\i18n\Formatter',
                'nullDisplay' => '-'
            ],
        'layout'=> Yii::$app->params['gridviewCustomExamination']['layout2'],
        'summary' => 'Showing <b>{begin}-{end}</b> of <b>{totalCount}</b> Users.',
        'pager' => Yii::$app->params['pagecustomexamination'],

        'columns' => [
            [
                'header' => 'No.',
                'class' => 'kartik\grid\SerialColumn'
            ],

            [
                'label' => 'Name',
                'attribute' => 'username',
            ],

            [
                'label' => 'E-mail',
                'attribute' => 'email',
            ],

            [
                
                'header' => 'Actions',
                'class' => 'kartik\grid\ActionColumn',
                'template' => '{update} {delete}', //{view} 
                'buttons' => [
                    // 'view' => function ($url, $model, $key) {
                    //     $options = [
                    //         'title' => Yii::t('yii', 'Paparan'),
                    //         'aria-label' => Yii::t('yii', 'Paparan'),
                    //         'data-pjax' => '0',
                    //         'data-title' => 'Paparan Maklumat',
                    //         'class'=>'btnmodalcustom',
                    //     ];

                    //     return Html::a('<span class="glyphicon glyphicon-eye-open"></span>', $url, $options);
                    // },

                    'update' => function ($url, $model, $key) {
                        $options = [
                            'title' => Yii::t('yii', 'Update'),
                            'aria-label' => Yii::t('yii', 'Update'),
                            'data-pjax' => '0',
                            'data-title' => 'Update User',
                            'class' => 'btnmodalcustom',
                        ];
                           
                        return Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url, $options);
                    },     
                    
                    'delete' => function ($url, $model, $key) {
                        $options = [
                            'id'=>'user-delete',
                            'title_delete' => Yii::t('yii', 'Remove'),
                            'aria-label' => Yii::t('yii', 'Remove'),
                            'data-pjax' => '0',

                            'class'=>'deletecustomgrid',
                            'data-confirmcustom'=>'Are you sure to remove this user?',
                            'data-url'=>$url,

                        ];

                        return Html::a('<span class="glyphicon glyphicon-trash"></span>', $url, $options);

                    }, 

                ]
            ],

        ],
    ]); ?>

    <?php Pjax::end(); ?>

</div>
