<?php
use yii\helpers\Html;
use yii\widgets\Pjax;
use yii\helpers\Url;

use kartik\grid\GridView;
use kartik\nav\NavX;

$this->title = Yii::t('app', 'User List');
$this->params['breadcrumbs'][] = $this->title; 

?>

<style>

li.tab-class.active > a {
    background: #0088cc !important;
    /* background: blue !important; */
    border-bottom: 3px solid #000;
    color: #fff !important;
}
.tab-class{
    width: 15%;
    text-align: center;
}
</style>

<div class="graduate-report-index">
    <!-- <div class="panel-body"> -->
        <!-- <div class="col-md-12"> -->        

    <h1><?= Html::encode('User List') ?></h1>

    <?php

        echo NavX::widget([
            'options' => ['class' => 'nav nav-tabs nav-pills '],
            'items' => [ 
                ['label' => 'Admin', 'options' => ['class' => 'tab-class'], 'url' => Url::to(['index', 'category' => 'admin']), 'active' => ($category == 'admin')? true : false ],
                ['label' => 'Lecturer', 'options' => ['class' => 'tab-class'], 'url' => Url::to(['index', 'category' => 'lecturer']), 'active' => ($category == 'lecturer')? true : false ],
                ['label' => 'Examination Unit', 'options' => ['class' => 'tab-class'], 'url' => Url::to(['index', 'category' => 'examination_unit']), 'active' => ($category == 'examination_unit')? true : false ],
            ],
        ]);
        
    ?>

<br>


    <?= $this->render('index', [
            'category' => $category,
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    ?>

        <!-- </div> -->
    <!-- </div> -->
</div>

