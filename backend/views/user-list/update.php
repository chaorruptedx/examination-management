<?php

use yii\helpers\Html;
use common\components\CustomDialog;

echo CustomDialog::Dialogcus($type ="TYPE_SUCCESS", $title='Confirmation', $btnOKClass='btn-success','krajeeDialogCust');

$this->title = 'Update User';
$this->params['breadcrumbs'][] = ['label' => 'User List', 'url' => ['index']];
$this->params['breadcrumbs'][] = 'Update User';
?>
<div class="user-update">

    <?= $this->render('_form', [
        'model' => $model,
        'datamodal' => $datamodal,
        'typeedit' => 'update',
    ]) ?>

</div>

<?php
$file=\Yii::$app->basePath . '/web/js/custommodal.js';
$file2=file_get_contents($file);

$this->registerJs($file2);
?>