<?php

use yii\helpers\Html;
use kartik\form\ActiveForm;
use kartik\file\FileInput;
?>

<div class="attachment-form">

<style>
.btn-form{
    padding-right: 16px;
}
</style>

    <?php 
    
        $form = ActiveForm::begin([
            'id' => 'attachment_form',
            'type' => ActiveForm::TYPE_HORIZONTAL,
            'formConfig' => ['labelSpan' => 3, 'deviceSize' => ActiveForm::SIZE_SMALL],

            'validateOnSubmit' => true,
            'enableAjaxValidation' => false,
            'enableClientValidation'=>true,
        ]);
    
    ?>

        <?= $form->field($model, 'timetable')->widget(FileInput::classname(), [
                'pluginOptions' => [
                    'showPreview' => false,
                    'showUpload' => false, 
                    'showRemove' => true,  
                    'allowedFileExtensions'=>['pdf'],
                ],
            ])->label("Vetting Timetable"); ?>

    <div class="form-group text-right btn-form">
        <?= Html::button(Yii::t('app', 'Save'), [
            'type' => 'button',
            'name' => 'btnsave',
            'data-idform' => 'attachment_form',
            'data-confirmcustom' => $datamodal['confirmtxt'],
            'class' => 'btn btn-success btnsavecustom' ]) 
        ?>
        <?php
            if (Yii::$app->request->isAjax) {
                echo Html::button(Yii::t('app', 'Cancel'), [
                    'data-dismiss'=>"modal",
                    'class' => 'btn btn btn-default'
                ]);
            } else {
                echo Html::a(Yii::t('app', 'Cancel'), ['index'], ['class' => 'btn btn btn-default']);
            }
        ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<?php
$script = <<< JS
  var time = setInterval(function() {
    myTimer();
  }, 1000);

  function myTimer() {
    var d = new Date();
    $('#clock').val(d.toLocaleString());
    // document.getElementById("clock").innerHTML = d.toLocaleString();
  }

JS;
$this->registerJs($script);
?>
