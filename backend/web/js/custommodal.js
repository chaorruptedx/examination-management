$(function() {

    $('.btnmodalcustom').unbind().click(function(e) {
    
        $('.detailcustom').empty();

        e.preventDefault();

        var title = $(this).data('title');
        var url = $(this).attr('href');

        $('#modaltitlecus').html('<h4>'+title+'</h4>');

        $('#modalcustom').modal('show').find('.detailcustom')
            .load($(this).attr('href'));

    });

    $(document).on('pjax:end', function() {
        $('.btnmodalcustom').unbind().click(function(e) {
            
            e.preventDefault();
            var title=$(this).data('title');

            $('#modaltitlecus').html('<h4>'+title+'</h4>');

            $('#modalcustom').modal('show').find('.detailcustom')
                .load($(this).attr('href'));
        });
    });


    $('#modalcustom').on('shown.bs.modal', function() {
        //To relate the z-index make sure backdrop and modal are siblings
        $(this).before($('.modal-backdrop'));

        //Now set z-index of modal greater than backdrop
        $(this).css("z-index", parseInt($('.modal-backdrop').css('z-index')) + 1);
    });
    
$('.btnsavecustom').unbind().click(function() {
   
    var idform=$( this ).data( "idform" );
    var confirmcustom=$( this ).data( "confirmcustom" );

    $('#'+idform).yiiActiveForm('validate', true);
    $('#'+idform).yiiActiveForm('validate');

    $('form#'+idform).on("submit", function(e){
        return false;
    }); 

    $('form#'+idform).one('afterValidate', function(event, message){
    // --- START VALIDATE ---

        if($('#'+idform).find('.has-error').length === 0){
        // --- START ---

            krajeeDialogCust.confirm(confirmcustom, function (result) {
                $(".bootstrap-dialog-footer-buttons").addClass( "" );

                    if (result) { // ok button was pressed
                    // --- Execute your code for confirmation ---

                        $('#'+idform).unbind('submit').submit();

                            $.blockUI({
                                message: 'Loading ...',
                                css: {
                                        border: 'none', 
                                        padding: '15px', 
                                        backgroundColor: '#000', 
                                        '-webkit-border-radius': '10px', 
                                        '-moz-border-radius': '10px', 
                                        opacity: .5, 
                                        color: '#fff' 
                                    },
                                baseZ: 4000
                            });

                    // --- Execute your code for confirmation ---
                    } else {

                        if($('#dialogresfresh').length > 0){
                            $.pjax.reload({container:'#dialogresfresh'}); //refresh the grid
                        }
                    
                    }

            });

        // --- END ---
        }

    // --- END VALIDATE ---
    });

});


$(".deletecustomgrid").unbind().click(function (e) {
    
    e.preventDefault();
    e.stopPropagation();

    var href = $(this).attr('href');
    var msgsend = $(this).data('confirmcustom');

    krajeeDialogdelete.confirm(msgsend, function (result) {

        if (result) {
            
            window.location.href = href;
                $.ajax({
                    method: "POST",
                    url: href,
                })

                .done(function (msg) {
                    
                });

        }
    });

});

    

});

$(document).on('pjax:start', function () { });


