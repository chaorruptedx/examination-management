<?php
namespace common\components;

use yii\helpers\ArrayHelper;

use common\models\Course;

class CourseHelper
{
    public static function get_course_code_name($id_course)
    {
        $data = Course::find()
            ->select(['code', 'name'])
            ->where(['id' => $id_course])
            ->asArray()
            ->one();

        return $data;
    }
}