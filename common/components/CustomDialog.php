<?php
namespace common\components;

use Yii;
use kartik\dialog\Dialog;


class CustomDialog extends \kartik\dialog\Dialog
{
    public function init()
    {
        parent::init();
    }

     public static function Dialogcus($type ="TYPE_INFO", $title='Maklumat', $btnOKClass='btn-primary',$libName)
    {
        
        if($type=='TYPE_PRIMARY'){
            $data['type']= Dialog::TYPE_PRIMARY;

        }elseif($type=='TYPE_WARNING'){

                $data['type']= Dialog::TYPE_WARNING;

        }elseif($type=='TYPE_SUCCESS'){

                $data['type']= Dialog::TYPE_SUCCESS;

        }elseif($type=='TYPE_INFO'){

                $data['type']= Dialog::TYPE_INFO;

        }else{

            $data['type']= Dialog::TYPE_DANGER;
        }

        $data['title']=$title;
        $data['btnOKClass']=$btnOKClass." btn-dialog-custom";
        $data['btnCancelClass']='btn-default pull-right';
        $data['btnCancelLabel']='NO';
        $data['btnOKLabel']='YES';
        $data['draggable']=false;
        $data['closable']=false;

        $dia= new Dialog();
        
            $dialog= $dia->widget([
                'libName' => $libName,
                'overrideYiiConfirm' => true,
                'options' => $data,
            ]);

        return $dialog;
    }

    function DialogcusIncomplete($type ="TYPE_INFO", $title='Maklumat', $libName)
    {
        if($type=='TYPE_PRIMARY'){
            $data['type']= Dialog::TYPE_PRIMARY;
        }elseif($type=='TYPE_WARNING'){
            $data['type']= Dialog::TYPE_WARNING;
        }elseif($type=='TYPE_SUCCESS'){
            $data['type']= Dialog::TYPE_SUCCESS;
        }elseif($type=='TYPE_INFO'){
            $data['type']= Dialog::TYPE_INFO;
        }else{
            $data['type']= Dialog::TYPE_DANGER;
        }

        $data['title']=$title;
        $data['btnOKClass']=false;
        $data['btnOKLabel']=false;
        $data['btnCancelClass']='btn-default pull-right';
        $data['btnCancelLabel']='Tutup';
        $data['draggable']=false;
        $data['closable']=false;

        $dia= new Dialog();

        $dialog= $dia->widget([
            'libName' => $libName,
            'overrideYiiConfirm' => true,
            'options' => $data,
        ]);

        return $dialog;
    }
}

