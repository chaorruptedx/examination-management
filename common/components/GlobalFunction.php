<?php
//created by fakhruz;
//warning! this is now outside the OOP scope of the application, for ease of use debugging function only

	//function debugging and die
    function dd($data, $die=true)
    {
        echo "<pre>";
        print_r($data);
        echo "</pre>";

        die();
    }

    //function debugging only
    function d($data)
    {
        echo "<pre>";
        print_r($data);
        echo "</pre>";
    }
