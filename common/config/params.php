<?php
Yii::setAlias('@upload', realpath(dirname(__FILE__).'/../../').'\upload');
return [
    'adminEmail' => 'admin@example.com',
    'supportEmail' => 'support@example.com',
    'senderEmail' => 'noreply@example.com',
    'senderName' => 'Example.com mailer',
    'user.passwordResetTokenExpire' => 3600,

    'pagecustomexamination'=> [
        'firstPageLabel' => '<<',
        'prevPageLabel' => '<',
        'nextPageLabel' => '>',
        'lastPageLabel'  => '>>',
    ],

    'gridviewCustomExamination' => [
        // 'layout' => "<div class='text-right toolbar-custom' style='padding-bottom:5px;padding-top:10px;'>{toolbar}</div>\n<div class='clearfix'></div>\n{items}\n<div>{summary}</div><div align='right'>{pager}</div><div class='clearfix'></div>",

        'layout' => "<div class='text-right toolbar-custom' style='padding-bottom:5px;padding-top:10px;'>{toolbar}</div><div class='clearfix'></div>\n{items}\n<div class='pull-left summary-custom'>{summary}</div><div align='right'>{pager}</div><div class='clearfix'></div>",

        // 'summary' => "Papar <b>{begin}</b> hingga <b>{end}</b> daripada <b>{totalCount}</b>",
        'layout2' => "<div class='clearfix'></div>\n{items}\n<div class='pull-left summary-custom'>{summary}</div><div align='right'>{pager}</div><div class='clearfix'></div>",
    ],

    'gridviewExportCustom' => [
        'header' => '',
        'label' => 'Download',
        'optionClass' => 'btn btn-default btn-export',
        'showConfirmAlert' => false,
        'labelCSV' => 'CSV',
        'labelEXCEL' => 'EXCEL',
        'labelPDF' => 'PDF',
    ],
    'gridviewToggleDataOptions' => [
        'all' => [
            'icon' => 'resize-full',
            'label' => 'List All Data',
            'class' => 'btn btn-default',
            'title' => 'List All Data'
        ],
        'page' => [
            'icon' => 'resize-small',
            'label' => 'List by Page',
            'class' => 'btn btn-default',
            'title' => 'List by Page'
        ],
    ],

];
