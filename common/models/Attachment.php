<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "attachment".
 *
 * @property int $id
 * @property int $id_user
 * @property int $id_vetting
 * @property string $category timetable | exam_format | answer_format | question_set_a | answer_set_a | question_set_b | answer_set_b
 * @property string $name
 * @property string $type
 * @property string $path
 * @property int $status 1 | 0 | -1
 * @property string $created_at
 * @property string $updated_at
 *
 * @property User $user
 * @property Vetting $vetting
 */
class Attachment extends \yii\db\ActiveRecord
{
    const SCENARIO_FORMAT = 'upload';
    const SCENARIO_TIMETABLE = 'timetable';

    public $question, $answer, $timetable;
    public $question_set_a, $answer_set_a, $question_set_b, $answer_set_b;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'attachment';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_user', 'category', 'name', 'type', 'question_set_a', 'answer_set_a', 'question_set_b', 'answer_set_b'], 'required'],
            [['id_user', 'id_vetting', 'status'], 'integer'],
            [['created_at', 'updated_at', 'question', 'answer', 'question_set_a', 'answer_set_a', 'question_set_b', 'answer_set_b', 'timetable'], 'safe'],
            [['category'], 'string', 'max' => 50],
            [['name'], 'string', 'max' => 100],
            [['type'], 'string', 'max' => 10],
            [['path'], 'string', 'max' => 255],
            [['id_user'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['id_user' => 'id']],
            [['id_vetting'], 'exist', 'skipOnError' => true, 'targetClass' => Vetting::className(), 'targetAttribute' => ['id_vetting' => 'id']],

            [['question'], 'required', 'on' => self::SCENARIO_FORMAT, 'message' => 'Examination Question cannot be blank.'],
            [['answer'], 'required', 'on' => self::SCENARIO_FORMAT, 'message' => 'Answer Scheme Format cannot be blank.'],


            [['timetable'], 'required', 'on' => self::SCENARIO_TIMETABLE, 'message' => 'Timetable cannot be blank.'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_user' => 'Id User',
            'id_vetting' => 'Id Vetting',
            'category' => 'Category',
            'name' => 'Name',
            'type' => 'Type',
            'path' => 'Path',
            'status' => 'Status',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {

            if($insert) {
            //insert
                
                $this->created_at = date( 'Y-m-d',time());

            } else {
            //update
                
                $this->updated_at = date( 'Y-m-d',time());
            }

            return true;

        } else {

            return false;
        
        }
    }

    public function upload($tableInfo)
    {
        if (\yii\helpers\FileHelper::createDirectory(Yii::getAlias('@upload').'/'. $tableInfo[folder]. '/', $mode = 0755, $recursive = true)){

            if(!empty($this->question)){
                $filing_question = $this->question;
            }

            if(!empty($this->answer)){
                $filing_answer = $this->answer;
            }

            if(!empty($this->question_set_a)){
                $filing_question_set_a = $this->question_set_a;
            }

            if(!empty($this->answer_set_a)){
                $filing_answer_set_a = $this->answer_set_a;
            }

            if(!empty($this->question_set_b)){
                $filing_question_set_b = $this->question_set_b;
            }

            if(!empty($this->answer_set_b)){
                $filing_answer_set_b = $this->answer_set_b;
            }

            if(!empty($this->timetable)){
                $filing_timetable = $this->timetable;
            }

                if(sizeof($filing_question) > 0) {
                    
                    foreach ($filing_question as $file) {

                        $singleDoc = self::find()->where(['category' => 'exam_format', 'status' => '1'])->one();

                            if(empty($singleDoc)){
                                
                                $singleDoc = new self();
                                $singleDoc->id_user = $tableInfo[id_user];
                                $singleDoc->category = 'exam_format';
                                $singleDoc->name = $file->baseName;
                                $singleDoc->type = $file->extension;
                                $singleDoc->status = 1;
                                $singleDoc->save(false);

                            } else {

                                $singleDoc->status = '-1';
                                $singleDoc->save(false);
                                
                                $singleDoc = new self();
                                $singleDoc->id_user = $tableInfo[id_user];
                                $singleDoc->category = 'exam_format';
                                $singleDoc->name = $file->baseName;
                                $singleDoc->type = $file->extension;
                                $singleDoc->status = 1;
                                $singleDoc->save(false);
                                
                            }

                            if (\yii\helpers\FileHelper::createDirectory(Yii::getAlias('@upload').'/'. $tableInfo[folder]. '/' .$singleDoc->id. '/', $mode = 0755, $recursive = true)){

                                $filename = Yii::getAlias('@upload').'/' .$tableInfo[folder]. '/' .$singleDoc->id. '/' .$file->baseName . '.' . $file->extension;
                                $path = '/upload/'.$tableInfo[folder]. '/' .$singleDoc->id. '/'.$file->baseName. '.' . $file->extension;
                                $file->saveAs($filename);

                                $singleDoc = self::find()->where(['id' => $singleDoc->id])->one();
                                $singleDoc->path = $path;
                                $singleDoc->save(false);

                            }
                        
                    }
                    
                }

                if(sizeof($filing_answer) > 0) {
                    
                    foreach ($filing_answer as $file) {

                        $singleDoc = self::find()->where(['category' => 'answer_format', 'status' => '1'])->one();

                            if(empty($singleDoc)){
                                
                                $singleDoc = new self();
                                $singleDoc->id_user = $tableInfo[id_user];
                                $singleDoc->category = 'answer_format';
                                $singleDoc->name = $file->baseName;
                                $singleDoc->type = $file->extension;
                                $singleDoc->status = 1;
                                $singleDoc->save(false);

                            } else {

                                $singleDoc->status = '-1';
                                $singleDoc->save(false);
                                
                                $singleDoc = new self();
                                $singleDoc->id_user = $tableInfo[id_user];
                                $singleDoc->category = 'answer_format';
                                $singleDoc->name = $file->baseName;
                                $singleDoc->type = $file->extension;
                                $singleDoc->status = 1;
                                $singleDoc->save(false);
                                
                            }

                            if (\yii\helpers\FileHelper::createDirectory(Yii::getAlias('@upload').'/'. $tableInfo[folder]. '/' .$singleDoc->id. '/', $mode = 0755, $recursive = true)){

                                $filename = Yii::getAlias('@upload').'/' .$tableInfo[folder]. '/' .$singleDoc->id. '/' .$file->baseName . '.' . $file->extension;
                                $path = '/upload/'.$tableInfo[folder]. '/' .$singleDoc->id. '/' .$file->baseName. '.' . $file->extension;
                                $file->saveAs($filename);

                                $singleDoc = self::find()->where(['id' => $singleDoc->id])->one();
                                $singleDoc->path = $path;
                                $singleDoc->save(false);

                            }
                        
                    }
                    
                }

                if(sizeof($filing_question_set_a) > 0) {
                    
                    foreach ($filing_question_set_a as $file) {

                        $singleDoc = self::find()->where(['id_vetting' => $tableInfo[id_vetting], 'category' => 'question_set_a', 'status' => '1'])->one();

                            if(empty($singleDoc)){
                                
                                $singleDoc = new self();
                                $singleDoc->id_user = $tableInfo[id_user];
                                $singleDoc->id_vetting = $tableInfo[id_vetting];
                                $singleDoc->category = 'question_set_a';
                                $singleDoc->name = $file->baseName;
                                $singleDoc->type = $file->extension;
                                $singleDoc->status = 1;
                                $singleDoc->save(false);

                            } else {

                                $singleDoc->status = '-1';
                                $singleDoc->save(false);
                                
                                $singleDoc = new self();
                                $singleDoc->id_user = $tableInfo[id_user];
                                $singleDoc->id_vetting = $tableInfo[id_vetting];
                                $singleDoc->category = 'question_set_a';
                                $singleDoc->name = $file->baseName;
                                $singleDoc->type = $file->extension;
                                $singleDoc->status = 1;
                                $singleDoc->save(false);
                                
                            }

                            if (\yii\helpers\FileHelper::createDirectory(Yii::getAlias('@upload').'/'. $tableInfo[folder]. '/' .$singleDoc->id. '/', $mode = 0755, $recursive = true)){

                                $filename = Yii::getAlias('@upload').'/' .$tableInfo[folder]. '/' .$singleDoc->id. '/' .$file->baseName . '.' . $file->extension;
                                $path = '/upload/'.$tableInfo[folder]. '/' .$singleDoc->id. '/' .$file->baseName. '.' . $file->extension;
                                $file->saveAs($filename);

                                $singleDoc = self::find()->where(['id' => $singleDoc->id])->one();
                                $singleDoc->path = $path;
                                $singleDoc->save(false);

                            }
                        
                    }
                    
                }

                if(sizeof($filing_answer_set_a) > 0) {
                    
                    foreach ($filing_answer_set_a as $file) {

                        $singleDoc = self::find()->where(['id_vetting' => $tableInfo[id_vetting], 'category' => 'answer_set_a', 'status' => '1'])->one();

                            if(empty($singleDoc)){
                                
                                $singleDoc = new self();
                                $singleDoc->id_user = $tableInfo[id_user];
                                $singleDoc->id_vetting = $tableInfo[id_vetting];
                                $singleDoc->category = 'answer_set_a';
                                $singleDoc->name = $file->baseName;
                                $singleDoc->type = $file->extension;
                                $singleDoc->status = 1;
                                $singleDoc->save(false);

                            } else {

                                $singleDoc->status = '-1';
                                $singleDoc->save(false);
                                
                                $singleDoc = new self();
                                $singleDoc->id_user = $tableInfo[id_user];
                                $singleDoc->id_vetting = $tableInfo[id_vetting];
                                $singleDoc->category = 'answer_set_a';
                                $singleDoc->name = $file->baseName;
                                $singleDoc->type = $file->extension;
                                $singleDoc->status = 1;
                                $singleDoc->save(false);
                                
                            }

                            if (\yii\helpers\FileHelper::createDirectory(Yii::getAlias('@upload').'/'. $tableInfo[folder]. '/' .$singleDoc->id. '/', $mode = 0755, $recursive = true)){

                                $filename = Yii::getAlias('@upload').'/' .$tableInfo[folder]. '/' .$singleDoc->id. '/' .$file->baseName . '.' . $file->extension;
                                $path = '/upload/'.$tableInfo[folder]. '/' .$singleDoc->id. '/' .$file->baseName. '.' . $file->extension;
                                $file->saveAs($filename);

                                $singleDoc = self::find()->where(['id' => $singleDoc->id])->one();
                                $singleDoc->path = $path;
                                $singleDoc->save(false);

                            }
                        
                    }
                    
                }

                if(sizeof($filing_question_set_b) > 0) {
                    
                    foreach ($filing_question_set_b as $file) {

                        $singleDoc = self::find()->where(['id_vetting' => $tableInfo[id_vetting], 'category' => 'question_set_b', 'status' => '1'])->one();

                            if(empty($singleDoc)){
                                
                                $singleDoc = new self();
                                $singleDoc->id_user = $tableInfo[id_user];
                                $singleDoc->id_vetting = $tableInfo[id_vetting];
                                $singleDoc->category = 'question_set_b';
                                $singleDoc->name = $file->baseName;
                                $singleDoc->type = $file->extension;
                                $singleDoc->status = 1;
                                $singleDoc->save(false);

                            } else {

                                $singleDoc->status = '-1';
                                $singleDoc->save(false);
                                
                                $singleDoc = new self();
                                $singleDoc->id_user = $tableInfo[id_user];
                                $singleDoc->id_vetting = $tableInfo[id_vetting];
                                $singleDoc->category = 'question_set_b';
                                $singleDoc->name = $file->baseName;
                                $singleDoc->type = $file->extension;
                                $singleDoc->status = 1;
                                $singleDoc->save(false);
                                
                            }

                            if (\yii\helpers\FileHelper::createDirectory(Yii::getAlias('@upload').'/'. $tableInfo[folder]. '/' .$singleDoc->id. '/', $mode = 0755, $recursive = true)){

                                $filename = Yii::getAlias('@upload').'/' .$tableInfo[folder]. '/' .$singleDoc->id. '/' .$file->baseName . '.' . $file->extension;
                                $path = '/upload/'.$tableInfo[folder]. '/' .$singleDoc->id. '/' .$file->baseName. '.' . $file->extension;
                                $file->saveAs($filename);

                                $singleDoc = self::find()->where(['id' => $singleDoc->id])->one();
                                $singleDoc->path = $path;
                                $singleDoc->save(false);

                            }
                        
                    }
                    
                }

                if(sizeof($filing_answer_set_b) > 0) {
                    
                    foreach ($filing_answer_set_b as $file) {

                        $singleDoc = self::find()->where(['id_vetting' => $tableInfo[id_vetting], 'category' => 'answer_set_b', 'status' => '1'])->one();

                            if(empty($singleDoc)){
                                
                                $singleDoc = new self();
                                $singleDoc->id_user = $tableInfo[id_user];
                                $singleDoc->id_vetting = $tableInfo[id_vetting];
                                $singleDoc->category = 'answer_set_b';
                                $singleDoc->name = $file->baseName;
                                $singleDoc->type = $file->extension;
                                $singleDoc->status = 1;
                                $singleDoc->save(false);

                            } else {

                                $singleDoc->status = '-1';
                                $singleDoc->save(false);
                                
                                $singleDoc = new self();
                                $singleDoc->id_user = $tableInfo[id_user];
                                $singleDoc->id_vetting = $tableInfo[id_vetting];
                                $singleDoc->category = 'answer_set_b';
                                $singleDoc->name = $file->baseName;
                                $singleDoc->type = $file->extension;
                                $singleDoc->status = 1;
                                $singleDoc->save(false);
                                
                            }

                            if (\yii\helpers\FileHelper::createDirectory(Yii::getAlias('@upload').'/'. $tableInfo[folder]. '/' .$singleDoc->id. '/', $mode = 0755, $recursive = true)){

                                $filename = Yii::getAlias('@upload').'/' .$tableInfo[folder]. '/' .$singleDoc->id. '/' .$file->baseName . '.' . $file->extension;
                                $path = '/upload/'.$tableInfo[folder]. '/' .$singleDoc->id. '/' .$file->baseName. '.' . $file->extension;
                                $file->saveAs($filename);

                                $singleDoc = self::find()->where(['id' => $singleDoc->id])->one();
                                $singleDoc->path = $path;
                                $singleDoc->save(false);

                            }
                        
                    }
                    
                }

                if(sizeof($filing_timetable) > 0) {
                    
                    foreach ($filing_timetable as $file) {

                        $singleDoc = self::find()->where(['category' => 'timetable', 'status' => '1'])->one();

                            if(empty($singleDoc)){
                                
                                $singleDoc = new self();
                                $singleDoc->id_user = $tableInfo[id_user];
                                $singleDoc->category = 'timetable';
                                $singleDoc->name = $file->baseName;
                                $singleDoc->type = $file->extension;
                                $singleDoc->status = 1;
                                $singleDoc->save(false);

                            } else {

                                $singleDoc->status = '-1';
                                $singleDoc->save(false);
                                
                                $singleDoc = new self();
                                $singleDoc->id_user = $tableInfo[id_user];
                                $singleDoc->category = 'timetable';
                                $singleDoc->name = $file->baseName;
                                $singleDoc->type = $file->extension;
                                $singleDoc->status = 1;
                                $singleDoc->save(false);
                                
                            }

                            if (\yii\helpers\FileHelper::createDirectory(Yii::getAlias('@upload').'/'. $tableInfo[folder]. '/' .$singleDoc->id. '/', $mode = 0755, $recursive = true)){

                                $filename = Yii::getAlias('@upload').'/' .$tableInfo[folder]. '/' .$singleDoc->id. '/' .$file->baseName . '.' . $file->extension;
                                $path = '/upload/'.$tableInfo[folder]. '/' .$singleDoc->id. '/' .$file->baseName. '.' . $file->extension;
                                $file->saveAs($filename);

                                $singleDoc = self::find()->where(['id' => $singleDoc->id])->one();
                                $singleDoc->path = $path;
                                $singleDoc->save(false);

                            }
                        
                    }
                    
                }

        }
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'id_user']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVetting()
    {
        return $this->hasOne(Vetting::className(), ['id' => 'id_vetting']);
    }
}
