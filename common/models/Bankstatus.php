<?php
namespace common\models;

use yii\helpers\ArrayHelper;

class Bankstatus extends \yii\base\Model
{
    public function listRole()
    {
        $listData = array(
            'admin' => 'Admin',
            'lecturer' => 'Lecturer',
            'examination_unit' => 'Examination Unit'
        );
        
        return $listData;
    }

    public function listProgramme()
    {
        $listData = array(
            'MOS 100 - Diploma in Open Source Computing' => 'MOS 100 - Diploma in Open Source Computing',
        );
        
        return $listData;
    }

    public function listSession()
    {
        $listData = array(
            '1' => '1',
            '2' => '2',
        );
        
        return $listData;
    }

    public function listSessions()
    {
        $data = Session::find()
            ->where(['status' => '1'])
            ->asArray()
            ->all();
            
        foreach ($data as $value) {

            $listData[$value['id']] = date("F Y", strtotime($value['session'])); 
        }
        
        return $listData;
    }

    public function listCourse()
    {
        $data = Course::find()
            ->where(['status' => '1'])
            ->asArray()
            ->all();

        foreach ($data as $value) {

            $listData[$value['id']] = $value['code'].' - '.$value['name']; 
        }
        
        return $listData;
    }

    public function listVettingSubmissionCourse($id_user) // First Submission [Lecturer / 1st Examiner]
    {
        $session = Session::find()
            ->select(['MAX(session) as max_session'])
            ->where([
                'session.status' => '1'
            ])
            ->asArray()
            ->one();

        $data = Course::find()
            ->joinWith(['examiners.session'])
            ->where([
                'course.status' => '1',
                'examiner.category' => '1st_examiner',
                'examiner.id_user' => $id_user,
                'examiner.status' => '1',
                'examiner.id_submission' => null,
                'session.session' => $session['max_session'],
                'session.status' => '1',
            ])
            ->asArray()
            ->all();

            foreach ($data as $value) {

                $listData[$value['id']] = $value['code'].' - '.$value['name']; 
            }
        
        return $listData;
    }

    public function listUsers()
    {
        $data = User::find()
            ->where(['status' => '1'])
            ->andWhere(['category' => 'lecturer'])
            ->asArray()
            ->all();

        foreach ($data as $value) {

            $listData[$value['id']] = $value['username']; 
        }
        
        return $listData;
    }
}