<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "course".
 *
 * @property int $id
 * @property string $code
 * @property string $name
 * @property int $status 1 | 0 | -1
 * @property string $created_at
 * @property string $updated_at
 *
 * @property Examiner[] $examiners
 */
class Course extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'course';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['code', 'name'], 'required'],
            [['status'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['code'], 'string', 'max' => 10, ],
            [['name'], 'string', 'max' => 100],
            // [['code'], 'unique'],
            [['code'], 'existFunct'],
        ];
    }

    function existfunct() {

        $returnVal = True;

        $courseExist = Course::find()->where(['like', 'code', $this->code])->andWhere(['status' => 1])->one();
        
            if ( preg_match("/[\'^£$%&*()}{@#~?><>,|=_+!-]/", $this->code) ) {

                $this->addError('code', 'Code subject cannot contain special characters.');

                $returnVal = False;

            } else if ( !empty($courseExist) ) {

                if( $courseExist->id != $this->id ) {

                    $this->addError('code', 'Code ' .$this->code. ' has already been taken.');

                    $returnVal = False;

                }

            }

            return $returnVal;

    }

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {

            if($insert) {
            //insert
                
                $this->created_at = date( 'Y-m-d',time());

            } else {
            //update
                
                $this->updated_at = date( 'Y-m-d',time());
            }

            return true;

        } else {

            return false;
        
        }
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'code' => 'Code',
            'name' => 'Name',
            'status' => 'Status',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getExaminers()
    {
        return $this->hasMany(Examiner::className(), ['id_course' => 'id']);
    }
}
