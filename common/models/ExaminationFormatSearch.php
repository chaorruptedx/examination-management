<?php

namespace common\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Attachment;

/**
 * ExaminationFormatSearch represents the model behind the search form of `common\models\Attachment`.
 */
class ExaminationFormatSearch extends Attachment
{
    public $personal_name;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'id_user', 'id_vetting', 'status'], 'integer'],
            [['category', 'name', 'type', 'path', 'created_at', 'updated_at', 'personal_name'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Attachment::find()->where(['status' => 1])->andWhere(['in', 'category', ['exam_format', 'answer_format']]);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'id_user' => $this->id_user,
            'id_vetting' => $this->id_vetting,
            'status' => $this->status,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'category', $this->category])
            ->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'type', $this->type])
            ->andFilterWhere(['like', 'path', $this->path]);

            if( !empty($this->personal_name)) {

                $query->joinwith('user');
                $query->andFilterWhere(['like', 'user.username', $this->personal_name]);

            }

        return $dataProvider;
    }
}
