<?php

namespace common\models;

use yii\db\Query;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Examiner;

/**
 * ExaminationPendingSearch represents the model behind the search form of `common\models\Examiner`.
 */
class ExaminationPendingSearch extends Examiner
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'id_user', 'id_course', 'id_session', 'id_submission', 'status'], 'integer'],
            [['category', 'created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = (new Query)
            ->select(['*'])
            ->from(["(
                SELECT course.code AS course_code, course.name AS course_name, submission.id AS id_submission, submission.progress_status AS submission_progress_status,

                    CASE 
                        WHEN examiner1.category = '1st_examiner' THEN user1.username
                    END AS 1st_examiner,

                    CASE 
                        WHEN examiner2.category = '2nd_examiner' THEN user2.username
                    END AS 2nd_examiner

                FROM examiner

                LEFT JOIN examiner examiner1 ON examiner.id_course = examiner1.id_course AND examiner.id_session = examiner1.id_session AND '1st_examiner' = examiner1.category
                LEFT JOIN examiner examiner2 ON examiner.id_course = examiner2.id_course AND examiner.id_session = examiner2.id_session AND '2nd_examiner' = examiner2.category
                LEFT JOIN user user1 ON examiner1.id_user = user1.id
                LEFT JOIN user user2 ON examiner2.id_user = user2.id
                LEFT JOIN course ON examiner.id_course = course.id
                LEFT JOIN submission ON examiner.id_submission = submission.id

                WHERE
                    examiner.status = 1
                    AND submission.progress_status = 'Examination Unit'

                GROUP BY
                    examiner.id_course, examiner.id_session, examiner.id_submission
            ) AS examiners"]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            // 'sort' => [
            //     'attributes' => [
            //         'ic_number' => [
            //             'asc' => ['report.ic_number' => SORT_ASC],
            //             'desc' => ['report.ic_number' => SORT_DESC],
            //         ],
            //         'personal_name' => [
            //             'asc' => ['report.personal_name' => SORT_ASC],
            //             'desc' => ['report.personal_name' => SORT_DESC],
            //         ],
            //         'description' => [
            //             'asc' => ['report.description' => SORT_ASC],
            //             'desc' => ['report.description' => SORT_DESC],
            //         ],
            //         'code' => [
            //             'asc' => ['report.code' => SORT_ASC],
            //             'desc' => ['report.code' => SORT_DESC],
            //         ],
            //     ],
            // ],
        ]);

        $this->load($params);

        return $dataProvider;
    }
}
