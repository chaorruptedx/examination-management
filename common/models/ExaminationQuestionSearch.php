<?php

namespace common\models;

use yii\db\Query;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Examiner;

/**
 * ExaminationQuestionSearch represents the model behind the search form of `common\models\Examiner`.
 */
class ExaminationQuestionSearch extends Examiner
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'id_user', 'id_course', 'id_session', 'id_submission', 'status'], 'integer'],
            [['category', 'created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = (new Query)
            ->select(['*'])
            ->from(["(
                SELECT course.code AS course_code, course.name AS course_name, submission.id AS id_submission, submission.programme_code AS submission_programme_code, submission.progress_status AS submission_progress_status, attachment_question.id AS question_id, attachment_question.path AS question_path, attachment_question.name AS question_name, attachment_question.type AS question_type, attachment_answer.id AS answer_id, attachment_answer.path AS answer_path, attachment_answer.name AS answer_name, attachment_answer.type AS answer_type,

                    CASE 
                        WHEN examiner1.category = '1st_examiner' THEN user1.username
                    END AS 1st_examiner,

                    CASE 
                        WHEN examiner2.category = '2nd_examiner' THEN user2.username
                    END AS 2nd_examiner

                FROM examiner

                LEFT JOIN examiner examiner1 ON examiner.id_course = examiner1.id_course AND examiner.id_session = examiner1.id_session AND '1st_examiner' = examiner1.category
                LEFT JOIN examiner examiner2 ON examiner.id_course = examiner2.id_course AND examiner.id_session = examiner2.id_session AND '2nd_examiner' = examiner2.category
                LEFT JOIN user user1 ON examiner1.id_user = user1.id
                LEFT JOIN user user2 ON examiner2.id_user = user2.id
                LEFT JOIN course ON examiner.id_course = course.id
                LEFT JOIN submission ON examiner.id_submission = submission.id
                LEFT JOIN vetting vetting_question ON submission.id = vetting_question.id_submission
                LEFT JOIN vetting vetting_answer ON submission.id = vetting_answer.id_submission
                LEFT JOIN attachment attachment_question ON vetting_question.id = attachment_question.id_vetting AND 'question_set_a' = attachment_question.category OR 'question_set_b' = attachment_question.category
                LEFT JOIN attachment attachment_answer ON vetting_answer.id = attachment_answer.id_vetting AND 'answer_set_a' = attachment_answer.category OR 'answer_set_b' = attachment_answer.category

                WHERE
                    examiner.status = 1
                    AND submission.progress_status = 'Completed'
                    AND vetting_question.chosen_set = '1'
                    AND vetting_answer.chosen_set = '1'
                    AND attachment_question.status = '1'
                    AND attachment_answer.status = '1'

                GROUP BY
                    examiner.id_course, examiner.id_session, examiner.id_submission
            ) AS examiners"]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            // 'sort' => [
            //     'attributes' => [
            //         'ic_number' => [
            //             'asc' => ['report.ic_number' => SORT_ASC],
            //             'desc' => ['report.ic_number' => SORT_DESC],
            //         ],
            //         'personal_name' => [
            //             'asc' => ['report.personal_name' => SORT_ASC],
            //             'desc' => ['report.personal_name' => SORT_DESC],
            //         ],
            //         'description' => [
            //             'asc' => ['report.description' => SORT_ASC],
            //             'desc' => ['report.description' => SORT_DESC],
            //         ],
            //         'code' => [
            //             'asc' => ['report.code' => SORT_ASC],
            //             'desc' => ['report.code' => SORT_DESC],
            //         ],
            //     ],
            // ],
        ]);

        $this->load($params);

        return $dataProvider;
    }
}
