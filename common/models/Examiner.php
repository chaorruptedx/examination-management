<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "examiner".
 *
 * @property int $id
 * @property string $category 1st_examiner | 2nd_examiner
 * @property int $id_user
 * @property int $id_course
 * @property int $id_session
 * @property int $id_submission
 * @property int $status 1 | 0 | -1
 * @property string $created_at
 * @property string $updated_at
 *
 * @property User $user
 * @property Course $course
 * @property Session $session
 * @property Submission $submission
 */
class Examiner extends \yii\db\ActiveRecord
{
    public $first, $second;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'examiner';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['category'], 'required'],
            [['id_user', 'id_course', 'id_session', 'id_submission', 'status'], 'integer'],
            [['created_at', 'updated_at', 'first', 'second'], 'safe'],
            [['category'], 'string', 'max' => 20],

            [['first'], 'required', 'message' => 'Examiner cannot be blank.'],
            [['second'], 'required', 'message' => 'Examiner cannot be blank.'],
            [['id_course'], 'required', 'message' => 'Course cannot be blank.'],
            [['id_session'], 'required', 'message' => 'Session cannot be blank.'],

            [['id_user'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['id_user' => 'id']],
            [['id_course'], 'exist', 'skipOnError' => true, 'targetClass' => Course::className(), 'targetAttribute' => ['id_course' => 'id']],
            [['id_session'], 'exist', 'skipOnError' => true, 'targetClass' => Session::className(), 'targetAttribute' => ['id_session' => 'id']],
            [['id_submission'], 'exist', 'skipOnError' => true, 'targetClass' => Submission::className(), 'targetAttribute' => ['id_submission' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'category' => 'Category',
            'id_user' => 'Id User',
            'id_course' => 'Id Course',
            'id_session' => 'Id Session',
            'id_submission' => 'Id Submission',
            'status' => 'Status',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {

            if($insert) {
            //insert
                
                $this->created_at = date( 'Y-m-d',time());

            } else {
            //update
                
                $this->updated_at = date( 'Y-m-d',time());
            }

            return true;

        } else {

            return false;
        
        }
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'id_user']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCourse()
    {
        return $this->hasOne(Course::className(), ['id' => 'id_course']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSession()
    {
        return $this->hasOne(Session::className(), ['id' => 'id_session']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSubmission()
    {
        return $this->hasOne(Submission::className(), ['id' => 'id_submission']);
    }
}