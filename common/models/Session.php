<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "session".
 *
 * @property int $id
 * @property string $session
 * @property int $status 1 | 0 | -1
 * @property string $created_at
 * @property string $updated_at
 *
 * @property Examiner[] $examiners
 */
class Session extends \yii\db\ActiveRecord
{
    public $month, $year;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'session';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['session'], 'required'],
            [['session', 'created_at', 'updated_at', 'month', 'year'], 'safe'],
            [['status'], 'integer'],
            [['session'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'session' => 'Session',
            'status' => 'Status',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {

            if($insert) {
            //insert
                
                $this->created_at = date( 'Y-m-d',time());

                    if(!empty($this->session)){

                        $this->session = date('Y-m-d H:i:s', strtotime(str_replace("/","-",$this->session)));
                    }

            } else {
            //update
                
                $this->updated_at = date( 'Y-m-d',time());

                    if(!empty($this->session)){

                        $this->session = date('Y-m-d H:i:s', strtotime(str_replace("/","-",$this->session)));
                    }
                    
            }

            return true;

        } else {

            return false;
        
        }
    }

    public function afterFind()
    {
        if(!empty($this->session)){
            $this->session = Yii::$app->formatter->asDatetime(strtotime( $this->session),'php:d-m-Y');
        }

        parent::afterFind();
        return true;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getExaminers()
    {
        return $this->hasMany(Examiner::className(), ['id_session' => 'id']);
    }
}