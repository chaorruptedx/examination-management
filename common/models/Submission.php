<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "submission".
 *
 * @property int $id
 * @property string $programme_code MOS 100 - Diploma in Open Source Computing
 * @property int $number_of_set 2
 * @property int $a_signature_1 1
 * @property string $a_name_1
 * @property string $a_position_1 Admin | Lecturer (1st Examiner) | Lecturer (2nd Examiner) | Examination Unit
 * @property string $a_date_1
 * @property int $a_signature_2 1
 * @property string $a_name_2
 * @property string $a_position_2 Admin | Lecturer (1st Examiner) | Lecturer (2nd Examiner) | Examination Unit
 * @property string $a_date_2
 * @property int $b_signature_1 1
 * @property string $b_name_1
 * @property string $b_position_1 Admin | Lecturer (1st Examiner) | Lecturer (2nd Examiner) | Examination Unit
 * @property string $b_date_1
 * @property int $b_signature_2 1
 * @property string $b_name_2
 * @property string $b_position_2 Admin | Lecturer (1st Examiner) | Lecturer (2nd Examiner) | Examination Unit
 * @property string $b_date_2
 * @property int $c_signature_1 1
 * @property string $c_name_1
 * @property string $c_position_1 Admin | Lecturer (1st Examiner) | Lecturer (2nd Examiner) | Examination Unit
 * @property string $c_date_1
 * @property int $c_signature_2 1
 * @property string $c_name_2
 * @property string $c_position_2 Admin | Lecturer (1st Examiner) | Lecturer (2nd Examiner) | Examination Unit
 * @property string $c_date_2
 * @property int $d_signature_1 1
 * @property string $d_name_1
 * @property string $d_position_1 Admin | Lecturer (1st Examiner) | Lecturer (2nd Examiner) | Examination Unit
 * @property string $d_date_1
 * @property int $d_signature_2 1
 * @property string $d_name_2
 * @property string $d_position_2 Admin | Lecturer (1st Examiner) | Lecturer (2nd Examiner) | Examination Unit
 * @property string $d_date_2
 * @property int $e_signature_1 1
 * @property string $e_name_1
 * @property string $e_position_1 Admin | Lecturer (1st Examiner) | Lecturer (2nd Examiner) | Examination Unit
 * @property string $e_date_1
 * @property int $e_signature_2 1
 * @property string $e_name_2
 * @property string $e_position_2 Admin | Lecturer (1st Examiner) | Lecturer (2nd Examiner) | Examination Unit
 * @property string $e_date_2
 * @property string $progress_status First Submission | Vetting | Correction | Final Submission | Examination Unit | Completed
 * @property int $status 1 | 0 | -1
 * @property string $created_at
 * @property string $updated_at
 *
 * @property Examiner[] $examiners
 * @property Vetting[] $vettings
 */
class Submission extends \yii\db\ActiveRecord
{
    const SCENARIO_FIRST_SUBMISSION = 'first_submission';
    const SCENARIO_VETTING_SUBMISSION = 'vetting_submission';
    const SCENARIO_CORRECTION_SUBMISSION = 'correction_submission';
    const SCENARIO_FINAL_SUBMISSION = 'final_submission';
    const SCENARIO_EXAMINATION_UNIT_SUBMISSION = 'examination_unit_submission';

    public $lecturer_name;
    public $course;
    public $chosen_set;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'submission';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['number_of_set', 'a_signature_1', 'a_signature_2', 'b_signature_1', 'b_signature_2', 'c_signature_1', 'c_signature_2', 'd_signature_1', 'd_signature_2', 'e_signature_1', 'e_signature_2', 'status'], 'integer'],
            [['programme_code', 'a_signature_1', 'a_name_1', 'a_position_1', 'a_date_1', 'progress_status', 'created_at'], 'required'],
            [['a_date_1', 'a_date_2', 'b_date_1', 'b_date_2', 'c_date_1', 'c_date_2', 'd_date_1', 'd_date_2', 'e_date_1', 'e_date_2', 'created_at', 'updated_at', 'lecturer_name', 'course', 'chosen_set'], 'safe'],
            [['programme_code'], 'string', 'max' => 100],
            [['a_name_1', 'a_position_1', 'a_name_2', 'a_position_2', 'b_name_1', 'b_position_1', 'b_name_2', 'b_position_2', 'c_name_1', 'c_position_1', 'c_name_2', 'c_position_2', 'd_name_1', 'd_position_1', 'd_name_2', 'd_position_2', 'e_name_1', 'e_position_1', 'e_name_2', 'e_position_2'], 'string', 'max' => 50],
            [['progress_status'], 'string', 'max' => 25],
            ['a_signature_1', 'required', 'requiredValue' => 1, 'message' => '{attribute} cannot be blank.'],

            [['course'], 'required', 'on' => 'first_submission'],

            [['a_signature_2', 'b_signature_1'], 'required', 'requiredValue' => 1, 'on' => 'vetting_submission', 'message' => '{attribute} cannot be blank.'],

            [['b_signature_2', 'c_signature_1'], 'required', 'requiredValue' => 1, 'on' => 'correction_submission', 'message' => '{attribute} cannot be blank.'],

            [['c_signature_2', 'd_signature_1'], 'required', 'requiredValue' => 1, 'on' => 'final_submission', 'message' => '{attribute} cannot be blank.'],

            [['d_signature_2', 'e_signature_1'], 'required', 'requiredValue' => 1, 'on' => 'examination_unit_submission', 'message' => '{attribute} cannot be blank.'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'programme_code' => 'Programme Code',
            'number_of_set' => 'Number Of Set',
            'a_signature_1' => 'Signature',
            'a_name_1' => 'Name',
            'a_position_1' => 'Position',
            'a_date_1' => 'Date',
            'a_signature_2' => 'Signature',
            'a_name_2' => 'Name',
            'a_position_2' => 'Position',
            'a_date_2' => 'Date',
            'b_signature_1' => 'Signature',
            'b_name_1' => 'Name',
            'b_position_1' => 'Position',
            'b_date_1' => 'Date',
            'b_signature_2' => 'Signature',
            'b_name_2' => 'Name',
            'b_position_2' => 'Position',
            'b_date_2' => 'Date',
            'c_signature_1' => 'Signature',
            'c_name_1' => 'Name',
            'c_position_1' => 'Position',
            'c_date_1' => 'Date',
            'c_signature_2' => 'Signature',
            'c_name_2' => 'Name',
            'c_position_2' => 'Position',
            'c_date_2' => 'Date',
            'd_signature_1' => 'Signature',
            'd_name_1' => 'Name',
            'd_position_1' => 'Position',
            'd_date_1' => 'Date',
            'd_signature_2' => 'Signature',
            'd_name_2' => 'Name',
            'd_position_2' => 'Position',
            'd_date_2' => 'Date',
            'e_signature_1' => 'Signature',
            'e_name_1' => 'Name',
            'e_position_1' => 'Position',
            'e_date_1' => 'Date',
            'e_signature_2' => 'Signature',
            'e_name_2' => 'Name',
            'e_position_2' => 'Position',
            'e_date_2' => 'Date',
            'progress_status' => 'Progress Status',
            'status' => 'Status',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'lecturer_name' => 'Lecturer Name',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getExaminers()
    {
        return $this->hasMany(Examiner::className(), ['id_submission' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFirstExaminer()
    {
        return $this->hasOne(Examiner::className(), ['id_submission' => 'id'])->andWhere(['category' => '1st_examiner']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSecondExaminer()
    {
        return $this->hasOne(Examiner::className(), ['id_submission' => 'id'])->andWhere(['category' => '2nd_examiner']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVettings()
    {
        return $this->hasMany(Vetting::className(), ['id_submission' => 'id']);
    }
}
