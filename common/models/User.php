<?php

namespace common\models;

use Yii;
use yii\web\IdentityInterface;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "user".
 *
 * @property int $id
 * @property string $category admin | lecturer | examination_unit
 * @property string $email
 * @property string $username
 * @property string $password
 * @property int $status 1 | 0 | -1
 * @property string $created_at
 * @property string $updated_at
 * @property string $auth_key
 * @property Announcement[] $announcements
 * @property Attachment[] $attachments
 * @property Examiner[] $examiners
 */
class User extends \yii\db\ActiveRecord implements IdentityInterface
{
    const SCENARIO_FORMAT = 'update';

    private $auth_key;

    public $new_password;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'user';
    }

    /**
     * {@inheritdoc}
     */
    // public function behaviors()
    // {
    //     return [
    //         TimestampBehavior::className(),
    //     ];
    // }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['category', 'email', 'username', 'password', 'created_at'], 'required'],
            [['status'], 'integer'],
            [['created_at', 'updated_at', 'auth_key', 'new_password'], 'safe'],
            [['category', 'email', 'username'], 'string', 'max' => 50],
            [['password'], 'string', 'max' => 255],
            [['email'], 'unique'],
            [['email'], 'email'],

            [['new_password'], 'required', 'on' => self::SCENARIO_FORMAT, 'message' => 'Password cannot be blank.'],
            // [['email'], 'emailFunction'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'category' => 'Category',
            'email' => 'Email',
            'username' => 'Username',
            'password' => 'Password',
            'status' => 'Status',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    public function signup()
    {
        if (!$this->validate()) {
            return null;
        }

        $existing = self::find()->where(['id' => $this->id])->one();

        $existing->category = $this->category;
        $existing->username = $this->username;
        $existing->email = $this->email;
        $existing->password = $this->setPassword($this->new_password);
        $existing->status = '1';
        $existing->updated_at = date('Y-m-d h:i:s', time());

        return $existing->save();
    }

    /**
     * Generates password hash from password and sets it to the model
     *
     * @param string $password
     */
    public function setPassword($password)
    {
        return Yii::$app->security->generatePasswordHash($password);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAnnouncements()
    {
        return $this->hasMany(Announcement::className(), ['id_user' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAttachments()
    {
        return $this->hasMany(Attachment::className(), ['id_user' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getExaminers()
    {
        return $this->hasMany(Examiner::className(), ['id_user' => 'id']);
    }

    /**
     * {@inheritdoc}
     */
    public static function findIdentity($id)
    {
        return static::findOne(['id' => $id, 'status' => '1']);
    }

    /**
     * {@inheritdoc}
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        throw new NotSupportedException('"findIdentityByAccessToken" is not implemented.');
    }

    /**
     * Finds user by username
     *
     * @param string $username
     * @return static|null
     */
    public static function findByEmail($email)
    {
        return static::findOne(['email' => $email, 'status' => '1']);
    }

    /**
     * {@inheritdoc}
     */
    public function getId()
    {
        return $this->getPrimaryKey();
    }

    /**
     * {@inheritdoc}
     */
    public function getAuthKey()
    {
        return $this->auth_key;
    }

    /**
     * {@inheritdoc}
     */
    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return bool if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return Yii::$app->security->validatePassword($password, $this->password);
    }

    /**
     * Generates "remember me" authentication key
     */
    public function generateAuthKey()
    {
        $this->auth_key = Yii::$app->security->generateRandomString();
    }
}
