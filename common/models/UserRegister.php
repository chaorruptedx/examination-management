<?php

namespace common\models;

use Yii;
use yii\base\Model;

/**
 * This is the model class for table "user".
 *
 * @property string $email
 * @property string $username
 * @property string $password
 */
class UserRegister extends Model
{
    public $username;
    public $email;
    public $password;
    public $category;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['category', 'email', 'username', 'password'], 'required'],
            [['category', 'email', 'username'], 'string', 'max' => 50],
            [['password'], 'string', 'max' => 255],
            [['email'], 'unique', 'targetClass' => '\common\models\User', 'message' => '{attribute} already exist.'],
            [['email'], 'email'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'email' => 'Email',
            'username' => 'Username',
            'password' => 'Password',
            'category' => 'Role',
        ];
    }

    /**
     * Signs user up.
     *
     * @return bool whether the creating new account was successful and email was sent
     */
    public function signup()
    {
        if (!$this->validate()) {
            return null;
        }
        
        $modelUser = new User();
        $modelUser->category = $this->category;
        $modelUser->username = $this->username;
        $modelUser->email = $this->email;
        $modelUser->password = $this->setPassword($this->password);
        $modelUser->status = '1';
        $modelUser->created_at = date('Y-m-d h:i:s', time());

        return $modelUser->save();
    }

    /**
     * Generates password hash from password and sets it to the model
     *
     * @param string $password
     */
    public function setPassword($password)
    {
        return Yii::$app->security->generatePasswordHash($password);
    }
}
