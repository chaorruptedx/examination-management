<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "vetting".
 *
 * @property int $id
 * @property int $id_submission
 * @property int $session 1 | 2
 * @property string $examination The Date of Examination
 * @property string $questions_set Set A | Set B
 * @property string $lecturer 1st Examiner Name
 * @property string $course_name_yn Yes | No
 * @property string $course_name_remarks
 * @property string $course_code_yn Yes | No
 * @property string $course_code_remarks
 * @property string $examination_yn Yes | No
 * @property string $examination_remarks
 * @property string $examination_duration_yn Yes | No
 * @property string $examination_duration_remarks
 * @property string $cover_page_instructions_yn Yes | No
 * @property string $cover_page_instructions_remarks
 * @property string $questions_instruction_parts_yn Yes | No
 * @property string $questions_instruction_parts_remarks
 * @property string $mark_distribution_yn Yes | No
 * @property string $mark_distribution_remarks
 * @property string $page_number_yn Yes | No
 * @property string $page_number_remarks
 * @property string $questions_numbering_yn Yes | No
 * @property string $questions_numbering_remarks
 * @property string $course_objectives_ana Appropriate | Not Appropriate
 * @property string $course_objectives_remarks
 * @property string $course_synopsis_content_ana Appropriate | Not Appropriate
 * @property string $course_synopsis_content_remarks
 * @property string $time_given_ana Appropriate | Not Appropriate
 * @property string $time_given_remarks
 * @property string $language_standard_ana Appropriate | Not Appropriate
 * @property string $language_standard_remarks
 * @property string $comment
 * @property int $declaration 1 | 2 | 3
 * @property string $head Admin Name
 * @property int $signature_1 1
 * @property string $members 2nd Examiner Name
 * @property int $signature_2
 * @property string $date_1
 * @property int $signature_3 1
 * @property string $date_2
 * @property int $chosen_set 1 | 0
 * @property int $status 1 | 0 | -1
 * @property string $created_at
 * @property string $updated_at
 *
 * @property Attachment[] $attachments
 * @property Submission $submission
 */
class Vetting extends \yii\db\ActiveRecord
{
    const SCENARIO_CORRECTION_SUBMISSION = 'correction_submission';
    const SCENARIO_EXAMINATION_UNIT_SUBMISSION = 'examination_unit_submission';

    public $session_session;
    public $course_code;
    public $course_name;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'vetting';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_submission', 'session', 'examination', 'questions_set', 'lecturer', 'created_at'], 'required'],
            [['id_submission', 'session', 'declaration', 'signature_1', 'signature_2', 'signature_3', 'chosen_set', 'status'], 'integer'],
            [['comment'], 'string'],
            [['date_1', 'date_2', 'created_at', 'updated_at', 'session_session', 'course_code', 'course_name'], 'safe'],
            [['examination', 'course_objectives_ana', 'course_synopsis_content_ana', 'time_given_ana', 'language_standard_ana'], 'string', 'max' => 25],
            [['questions_set'], 'string', 'max' => 10],
            [['lecturer', 'course_name_remarks', 'course_code_remarks', 'examination_remarks', 'examination_duration_remarks', 'cover_page_instructions_remarks', 'questions_instruction_parts_remarks', 'mark_distribution_remarks', 'page_number_remarks', 'questions_numbering_remarks', 'course_objectives_remarks', 'course_synopsis_content_remarks', 'time_given_remarks', 'language_standard_remarks', 'head', 'members'], 'string', 'max' => 50],
            [['course_name_yn', 'course_code_yn', 'examination_yn', 'examination_duration_yn', 'cover_page_instructions_yn', 'questions_instruction_parts_yn', 'mark_distribution_yn', 'page_number_yn', 'questions_numbering_yn'], 'string', 'max' => 5],
            [['id_submission'], 'exist', 'skipOnError' => true, 'targetClass' => Submission::className(), 'targetAttribute' => ['id_submission' => 'id']],

            // [['course_name_yn', 'course_code_yn', 'examination_yn', 'examination_duration_yn', 'cover_page_instructions_yn', 'questions_instruction_parts_yn', 'mark_distribution_yn', 'page_number_yn', 'questions_numbering_yn', 'course_objectives_ana', 'course_synopsis_content_ana', 'time_given_ana', 'language_standard_ana'], 'required', 'on' => 'correction_submission', 'message' => '{attribute} cannot be blank.'],
            [['signature_2'], 'required', 'requiredValue' => 1, 'on' => 'correction_submission', 'message' => '{attribute} cannot be blank.'],

            [['signature_1', 'signature_3'], 'required', 'requiredValue' => 1, 'on' => 'examination_unit_submission', 'message' => '{attribute} cannot be blank.'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_submission' => 'ID Submission',
            'session' => 'Session',
            'examination' => 'Examination',
            'questions_set' => 'Questions Set',
            'lecturer' => 'Lecturer',
            'course_name_yn' => 'Course Name',
            'course_name_remarks' => 'Course Name Remarks',
            'course_code_yn' => 'Course Code',
            'course_code_remarks' => 'Course Code Remarks',
            'examination_yn' => 'Examination',
            'examination_remarks' => 'Examination Remarks',
            'examination_duration_yn' => 'Examination Duration',
            'examination_duration_remarks' => 'Examination Duration Remarks',
            'cover_page_instructions_yn' => 'Cover Page Instructions',
            'cover_page_instructions_remarks' => 'Cover Page Instructions Remarks',
            'questions_instruction_parts_yn' => 'Questions Instruction Parts',
            'questions_instruction_parts_remarks' => 'Questions Instruction Parts Remarks',
            'mark_distribution_yn' => 'Mark Distribution',
            'mark_distribution_remarks' => 'Mark Distribution Remarks',
            'page_number_yn' => 'Page Number',
            'page_number_remarks' => 'Page Number Remarks',
            'questions_numbering_yn' => 'Questions Numbering',
            'questions_numbering_remarks' => 'Questions Numbering Remarks',
            'course_objectives_ana' => 'Course Objectives',
            'course_objectives_remarks' => 'Course Objectives Remarks',
            'course_synopsis_content_ana' => 'Course Synopsis Content',
            'course_synopsis_content_remarks' => 'Course Synopsis Content Remarks',
            'time_given_ana' => 'Time Given',
            'time_given_remarks' => 'Time Given Remarks',
            'language_standard_ana' => 'Language Standard',
            'language_standard_remarks' => 'Language Standard Remarks',
            'comment' => 'Comment',
            'declaration' => 'Declaration',
            'head' => 'Head',
            'signature_1' => 'Signature',
            'members' => 'Members',
            'signature_2' => 'Signature',
            'date_1' => 'Date',
            'signature_3' => 'Signature',
            'date_2' => 'Date',
            'chosen_set' => 'Chosen Set',
            'status' => 'Status',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'session_session' => 'Session',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAttachments()
    {
        return $this->hasMany(Attachment::className(), ['id_vetting' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSubmission()
    {
        return $this->hasOne(Submission::className(), ['id' => 'id_submission']);
    }
}
