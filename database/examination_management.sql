-- phpMyAdmin SQL Dump
-- version 4.6.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Oct 22, 2017 at 06:53 AM
-- Server version: 5.7.14
-- PHP Version: 5.6.25

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `examination_management`
--
CREATE DATABASE IF NOT EXISTS `examination_management` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `examination_management`;

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `admin_id` int(10) NOT NULL,
  `email` varchar(100) NOT NULL,
  `username` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`admin_id`, `email`, `username`, `password`) VALUES
(2, 'AmeerulZaki@yahoo.com.my', 'Ameerul Zaki', 'Zaki0123'),
(4, 'aminnurizzat@gmail.com', 'Amin Nur Izzat', '12233445'),
(5, 'ridhuan1997@gmail.com', 'Muhamad Ridhuan', '12233445');

-- --------------------------------------------------------

--
-- Table structure for table `announcement`
--

CREATE TABLE `announcement` (
  `announcement_id` int(11) NOT NULL,
  `announcement` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `announcement`
--

INSERT INTO `announcement` (`announcement_id`, `announcement`) VALUES
(1, 'Please submit the examination question for vetting before 1 August, 2017.'),
(2, 'The examination question and answer scheme set must be upload in <b>.pdf</b> format.');

-- --------------------------------------------------------

--
-- Table structure for table `assign_lecturer`
--

CREATE TABLE `assign_lecturer` (
  `assign_lecturer_id` int(11) NOT NULL,
  `lecturer_id1` int(10) NOT NULL,
  `1st_examiner` varchar(100) NOT NULL,
  `course_code` varchar(10) NOT NULL,
  `course_name` varchar(50) NOT NULL,
  `lecturer_id2` int(10) NOT NULL,
  `2nd_examiner` varchar(100) NOT NULL,
  `session` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `assign_lecturer`
--

INSERT INTO `assign_lecturer` (`assign_lecturer_id`, `lecturer_id1`, `1st_examiner`, `course_code`, `course_name`, `lecturer_id2`, `2nd_examiner`, `session`) VALUES
(13, 3, 'Aris Shaiful', 'UNI 1012', 'English 1', 1, 'Azura', 'June 2017'),
(14, 1, 'Azura', 'DOS 3023', 'Innovation Management', 3, 'Aris Shaiful', 'June 2017'),
(15, 4, 'Hilmi', 'DOS 3033', 'Open Source Game Development', 3, 'Aris Shaiful', 'June 2017'),
(16, 5, 'Faris', 'MPU 2223', 'Study Skill', 4, 'Hilmi', 'June 2017'),
(17, 6, 'Anaqie', 'UNI 1022', 'English 2', 3, 'Aris Shaiful', 'June 2017'),
(18, 7, 'Wan', 'TNN 1233', 'Tech', 6, 'Anaqie', 'June 2017'),
(19, 8, 'Muzzammil', 'DOS 2113', 'Linux System Administration', 4, 'Hilmi', 'June 2017'),
(20, 9, 'Shahrul', 'MAT 1023', 'Mathematics for IT', 8, 'Muzzammil', 'June 2017'),
(21, 10, 'Abu', 'MPU 2213', 'Bahasa Kebangsaan A', 11, 'Ali', 'June 2017'),
(22, 12, 'Adam', 'DOS 1033', 'Introduction to Computer Programming', 13, 'Johnson', 'June 2017');

-- --------------------------------------------------------

--
-- Table structure for table `course`
--

CREATE TABLE `course` (
  `course_id` int(11) NOT NULL,
  `course_code` varchar(10) NOT NULL,
  `course_name` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `course`
--

INSERT INTO `course` (`course_id`, `course_code`, `course_name`) VALUES
(4, 'MPU 2213', 'Bahasa Kebangsaan A'),
(5, 'MPU 2223', 'Study Skill'),
(6, 'MPU 2163', 'Pengajian Malaysia 2'),
(7, 'MAT 1023', 'Mathematics for IT'),
(8, 'UNI 1012', 'English 1'),
(9, 'DOS 1013', 'Introduction to Open Source Applications'),
(10, 'DOS 1023', 'Application Information Technology'),
(11, 'MPU 2123', 'Integration and Moral Practice'),
(12, 'UNI 1022', 'English 2'),
(14, 'DOS 1033', 'Introduction to Computer Programming'),
(15, 'DOS 1043', 'Introduction to Open Source Software'),
(16, 'DGM 1033', 'System Analysis and Design'),
(17, 'DOS 2013', 'Computer Network and Communication'),
(18, 'DOS 2023', 'Designing Open Source Database'),
(19, 'DOS 2033', 'Open Source Web Programming'),
(20, 'DOS 2043', 'Software Development Open Source Approach'),
(21, 'DOS 2053', 'Object Oriented Programming'),
(22, 'DOS 2063', 'Introduction to Open Source Multimedia'),
(23, 'DOS 2073', 'Enterprise Resource Planning'),
(24, 'DOS 2083', 'Managing Open Source Project'),
(25, 'DOS 2093', 'Internet and Web Publishing'),
(26, 'DOS 2103', 'Interactive Media Authoring'),
(27, 'DOS 2113', 'Linux System Administration'),
(28, 'DOS 2123', 'Mobile Computing Programming'),
(29, 'MGT 2123', 'Fundamental of Entrepreneurship'),
(30, 'DOS 3013', 'Content Management System'),
(31, 'DOS 3023', 'Innovation Management'),
(32, 'DOS 3033', 'Open Source Game Development'),
(33, 'TNN 1233', 'Tech');

-- --------------------------------------------------------

--
-- Table structure for table `examination_format`
--

CREATE TABLE `examination_format` (
  `examination_format_id` int(11) NOT NULL,
  `file` varchar(100) NOT NULL,
  `type` varchar(100) NOT NULL,
  `size` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `examination_format`
--

INSERT INTO `examination_format` (`examination_format_id`, `file`, `type`, `size`) VALUES
(1, 'examination_format.pdf', 'application/pdf', 1056),
(2, 'answer_format.pdf', 'application/pdf', 420);

-- --------------------------------------------------------

--
-- Table structure for table `examination_paper`
--

CREATE TABLE `examination_paper` (
  `examination_paper_id` int(11) NOT NULL,
  `filea1` varchar(100) NOT NULL,
  `filea2` varchar(100) DEFAULT NULL,
  `fileb1` varchar(100) DEFAULT NULL,
  `fileb2` varchar(100) DEFAULT NULL,
  `status` varchar(50) DEFAULT NULL,
  `chosen_set` varchar(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `examination_paper`
--

INSERT INTO `examination_paper` (`examination_paper_id`, `filea1`, `filea2`, `fileb1`, `fileb2`, `status`, `chosen_set`) VALUES
(24, '50355-diploma-in-open-source-computing-(mos100).pdf', '47240-examination-slip.pdf', '77420-student-calendar.pdf', '5589-vetting_timetable.pdf', 'Completed', 'Set A'),
(25, '43814-vetting_timetable.pdf', '96883-student-calendar.pdf', '7819-examination-slip.pdf', '25139-diploma-in-open-source-computing-(mos100).pdf', 'Completed', 'Set B'),
(26, '61099-student-calendar.pdf', '24097-examination-slip.pdf', '48427-diploma-in-open-source-computing-(mos100).pdf', '56793-vetting_timetable.pdf', 'Completed', 'Set B'),
(27, '18662-vetting_timetable.pdf', '23692-student-calendar.pdf', '38330-specifications.pdf', '91629-specifications-(4752-series).pdf', 'Completed', 'Set A'),
(28, '99865-coway-chp-06dl---user-manual.pdf', '80713-diploma-in-open-source-computing-(mos100).pdf', '89199-examination-slip.pdf', '96949-student-calendar.pdf', 'Completed', 'Set B'),
(29, '57836---chaorrupted-x---(profile-picture).png', '52062---chaorrupted-x---(oldest-profile-picture).jpg', '64694---chaorrupted-x---(older-profile-picture).png', '35451---chaorrupted-x---(older-profile-cover).png', 'Examination Unit', 'Set A'),
(30, '13372-vetting_timetable.pdf', '39515-student-calendar.pdf', '10344-specifications.pdf', '46760-specifications-(4752-series).pdf', 'Correction', NULL),
(31, '24079-vetting_timetable.pdf', '80483-student-calendar.pdf', '33233-specifications.pdf', '29526-specifications-(4752-series).pdf', 'Completed', 'Set A'),
(32, '50847-diploma-in-open-source-computing-(mos100).pdf', '34403-specifications-(4752-series).pdf', '40956-coway-chp-06dl---user-manual.pdf', '35944-vetting_timetable.pdf', 'Completed', 'Set B');

-- --------------------------------------------------------

--
-- Table structure for table `examination_unit`
--

CREATE TABLE `examination_unit` (
  `examination_unit_id` int(10) NOT NULL,
  `email` varchar(100) NOT NULL,
  `username` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `examination_unit`
--

INSERT INTO `examination_unit` (`examination_unit_id`, `email`, `username`, `password`) VALUES
(1, 'syakir@gmail.com', 'Syakir', '654321');

-- --------------------------------------------------------

--
-- Table structure for table `lecturer`
--

CREATE TABLE `lecturer` (
  `lecturer_id` int(10) NOT NULL,
  `email` varchar(100) NOT NULL,
  `username` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `lecturer`
--

INSERT INTO `lecturer` (`lecturer_id`, `email`, `username`, `password`) VALUES
(1, 'azura@gmail.com', 'Azura', '123456'),
(3, 'arisabdul1997@gmail.com', 'Aris Shaiful', '123456'),
(4, 'hilmi@gmail.com', 'Hilmi', '123456'),
(5, 'faris@gmail.com', 'Faris', '123456'),
(6, 'anaqie@gmail.com', 'Anaqie', '123456'),
(7, 'wan@gmail.com', 'Wan', '123456'),
(8, 'muzzammil@gmail.com', 'Muzzammil', '123456'),
(9, 'Shahrul@gmail.com', 'Shahrul', '123456'),
(10, 'Abu@gmail.com', 'Abu', '123456'),
(11, 'Ali@gmail.com', 'Ali', '123456'),
(12, 'adam@gmail.com', 'Adam', '123456'),
(13, 'johnson@gmail.com', 'Johnson', '123456');

-- --------------------------------------------------------

--
-- Table structure for table `session`
--

CREATE TABLE `session` (
  `session_id` int(11) NOT NULL,
  `session` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `session`
--

INSERT INTO `session` (`session_id`, `session`) VALUES
(3, 'December 2017'),
(1, 'June 2017');

-- --------------------------------------------------------

--
-- Table structure for table `submission_form`
--

CREATE TABLE `submission_form` (
  `submission_form_id` int(11) NOT NULL,
  `examination_paper_id` int(11) NOT NULL,
  `lecturer_id1` int(11) NOT NULL,
  `lecturer_id2` int(11) DEFAULT NULL,
  `lecturer_name` varchar(100) NOT NULL,
  `programme_code` varchar(100) NOT NULL,
  `course_code` varchar(10) NOT NULL,
  `number_of_set` varchar(10) NOT NULL,
  `asignature1` varchar(10) NOT NULL,
  `aname1` varchar(100) NOT NULL,
  `aposition1` varchar(50) NOT NULL,
  `adate1` date NOT NULL,
  `asignature2` varchar(10) DEFAULT NULL,
  `aname2` varchar(100) DEFAULT NULL,
  `aposition2` varchar(50) DEFAULT NULL,
  `adate2` date DEFAULT NULL,
  `bsignature1` varchar(10) DEFAULT NULL,
  `bname1` varchar(100) DEFAULT NULL,
  `bposition1` varchar(50) DEFAULT NULL,
  `bdate1` date DEFAULT NULL,
  `bsignature2` varchar(10) DEFAULT NULL,
  `bname2` varchar(100) DEFAULT NULL,
  `bposition2` varchar(50) DEFAULT NULL,
  `bdate2` date DEFAULT NULL,
  `csignature1` varchar(10) DEFAULT NULL,
  `cname1` varchar(100) DEFAULT NULL,
  `cposition1` varchar(50) DEFAULT NULL,
  `cdate1` date DEFAULT NULL,
  `csignature2` varchar(10) DEFAULT NULL,
  `cname2` varchar(100) DEFAULT NULL,
  `cposition2` varchar(50) DEFAULT NULL,
  `cdate2` date DEFAULT NULL,
  `dsignature1` varchar(10) DEFAULT NULL,
  `dname1` varchar(100) DEFAULT NULL,
  `dposition1` varchar(50) DEFAULT NULL,
  `ddate1` date DEFAULT NULL,
  `dsignature2` varchar(10) DEFAULT NULL,
  `dname2` varchar(100) DEFAULT NULL,
  `dposition2` varchar(50) DEFAULT NULL,
  `ddate2` date DEFAULT NULL,
  `esignature1` varchar(10) DEFAULT NULL,
  `ename1` varchar(100) DEFAULT NULL,
  `eposition1` varchar(50) DEFAULT NULL,
  `edate1` date DEFAULT NULL,
  `esignature2` varchar(10) DEFAULT NULL,
  `ename2` varchar(100) DEFAULT NULL,
  `eposition2` varchar(50) DEFAULT NULL,
  `edate2` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `submission_form`
--

INSERT INTO `submission_form` (`submission_form_id`, `examination_paper_id`, `lecturer_id1`, `lecturer_id2`, `lecturer_name`, `programme_code`, `course_code`, `number_of_set`, `asignature1`, `aname1`, `aposition1`, `adate1`, `asignature2`, `aname2`, `aposition2`, `adate2`, `bsignature1`, `bname1`, `bposition1`, `bdate1`, `bsignature2`, `bname2`, `bposition2`, `bdate2`, `csignature1`, `cname1`, `cposition1`, `cdate1`, `csignature2`, `cname2`, `cposition2`, `cdate2`, `dsignature1`, `dname1`, `dposition1`, `ddate1`, `dsignature2`, `dname2`, `dposition2`, `ddate2`, `esignature1`, `ename1`, `eposition1`, `edate1`, `esignature2`, `ename2`, `eposition2`, `edate2`) VALUES
(16, 24, 3, 1, 'Aris Shaiful', 'MOS 100 - Diploma in Open Source Computing', 'UNI 1012', '2', 'Signed', 'Aris Shaiful', 'Lecturer (1st Examiner)', '2017-09-19', 'Signed', 'Ameerul Zaki', 'Admin', '2017-09-19', 'Signed', 'Ameerul Zaki', 'Admin', '2017-09-19', 'Signed', 'Azura', 'Lecturer (2nd Examiner)', '2017-09-19', 'Signed', 'Azura', 'Lecturer (2nd Examiner)', '2017-09-19', 'Signed', 'Aris Shaiful', 'Lecturer (1st Examiner)', '2017-09-20', 'Signed', 'Aris Shaiful', 'Lecturer (1st Examiner)', '2017-09-20', 'Signed', 'Ameerul Zaki', 'Admin', '2017-09-20', 'Signed', 'Ameerul Zaki', 'Admin', '2017-09-20', 'Signed', 'Syakir', 'Examination Unit', '2017-09-20'),
(17, 25, 1, 3, 'Azura', 'MOS 100 - Diploma in Open Source Computing', 'DOS 3023', '2', 'Signed', 'Azura', 'Lecturer (1st Examiner)', '2017-09-21', 'Signed', 'Ameerul Zaki', 'Admin', '2017-09-21', 'Signed', 'Ameerul Zaki', 'Admin', '2017-09-21', 'Signed', 'Aris Shaiful', 'Lecturer (2nd Examiner)', '2017-09-21', 'Signed', 'Aris Shaiful', 'Lecturer (2nd Examiner)', '2017-09-21', 'Signed', 'Azura', 'Lecturer (1st Examiner)', '2017-09-21', 'Signed', 'Azura', 'Lecturer (1st Examiner)', '2017-09-21', 'Signed', 'Ameerul Zaki', 'Admin', '2017-09-21', 'Signed', 'Ameerul Zaki', 'Admin', '2017-09-21', 'Signed', 'Syakir', 'Examination Unit', '2017-09-21'),
(18, 26, 4, 3, 'Hilmi', 'MOS 100 - Diploma in Open Source Computing', 'DOS 3033', '2', 'Signed', 'Hilmi', 'Lecturer (1st Examiner)', '2017-09-21', 'Signed', 'Ameerul Zaki', 'Admin', '2017-09-21', 'Signed', 'Ameerul Zaki', 'Admin', '2017-09-21', 'Signed', 'Aris Shaiful', 'Lecturer (2nd Examiner)', '2017-09-21', 'Signed', 'Aris Shaiful', 'Lecturer (2nd Examiner)', '2017-09-21', 'Signed', 'Hilmi', 'Lecturer (1st Examiner)', '2017-09-21', 'Signed', 'Hilmi', 'Lecturer (1st Examiner)', '2017-09-21', 'Signed', 'Ameerul Zaki', 'Admin', '2017-09-21', 'Signed', 'Ameerul Zaki', 'Admin', '2017-09-21', 'Signed', 'Syakir', 'Examination Unit', '2017-09-21'),
(19, 27, 5, 4, 'Faris', 'MOS 100 - Diploma in Open Source Computing', 'MPU 2223', '2', 'Signed', 'Faris', 'Lecturer (1st Examiner)', '2017-09-27', 'Signed', 'Ameerul Zaki', 'Admin', '2017-09-27', 'Signed', 'Ameerul Zaki', 'Admin', '2017-09-27', 'Signed', 'Hilmi', 'Lecturer (2nd Examiner)', '2017-09-27', 'Signed', 'Hilmi', 'Lecturer (2nd Examiner)', '2017-09-27', 'Signed', 'Faris', 'Lecturer (1st Examiner)', '2017-09-27', 'Signed', 'Faris', 'Lecturer (1st Examiner)', '2017-09-27', 'Signed', 'Ameerul Zaki', 'Admin', '2017-09-27', 'Signed', 'Ameerul Zaki', 'Admin', '2017-09-27', 'Signed', 'Syakir', 'Examination Unit', '2017-09-27'),
(20, 28, 7, 6, 'Wan', 'MOS 100 - Diploma in Open Source Computing', 'TNN 1233', '2', 'Signed', 'Wan', 'Lecturer (1st Examiner)', '2017-09-27', 'Signed', 'Ameerul Zaki', 'Admin', '2017-09-27', 'Signed', 'Ameerul Zaki', 'Admin', '2017-09-27', 'Signed', 'Anaqie', 'Lecturer (2nd Examiner)', '2017-09-27', 'Signed', 'Anaqie', 'Lecturer (2nd Examiner)', '2017-09-27', 'Signed', 'Wan', 'Lecturer (1st Examiner)', '2017-09-27', 'Signed', 'Wan', 'Lecturer (1st Examiner)', '2017-09-27', 'Signed', 'Ameerul Zaki', 'Admin', '2017-09-27', 'Signed', 'Ameerul Zaki', 'Admin', '2017-09-27', 'Signed', 'Syakir', 'Examination Unit', '2017-09-27'),
(21, 29, 8, 4, 'Muzzammil', 'MOS 100 - Diploma in Open Source Computing', 'DOS 2113', '2', 'Signed', 'Muzzammil', 'Lecturer (1st Examiner)', '2017-10-05', 'Signed', 'Ameerul Zaki', 'Admin', '2017-10-05', 'Signed', 'Ameerul Zaki', 'Admin', '2017-10-05', 'Signed', 'Hilmi', 'Lecturer (2nd Examiner)', '2017-10-05', 'Signed', 'Hilmi', 'Lecturer (2nd Examiner)', '2017-10-05', 'Signed', 'Muzzammil', 'Lecturer (1st Examiner)', '2017-10-19', 'Signed', 'Muzzammil', 'Lecturer (1st Examiner)', '2017-10-19', 'Signed', 'Ameerul Zaki', 'Admin', '2017-10-19', 'Signed', 'Ameerul Zaki', 'Admin', '2017-10-19', NULL, NULL, NULL, NULL),
(22, 30, 9, 8, 'Shahrul', 'MOS 100 - Diploma in Open Source Computing', 'MAT 1023', '2', 'Signed', 'Shahrul', 'Lecturer (1st Examiner)', '2017-10-05', 'Signed', 'Ameerul Zaki', 'Admin', '2017-10-05', 'Signed', 'Ameerul Zaki', 'Admin', '2017-10-05', 'Signed', 'Muzzammil', 'Lecturer (2nd Examiner)', '2017-10-05', 'Signed', 'Muzzammil', 'Lecturer (2nd Examiner)', '2017-10-05', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(23, 31, 10, 11, 'Abu', 'MOS 100 - Diploma in Open Source Computing', 'MPU 2213', '2', 'Signed', 'Abu', 'Lecturer (1st Examiner)', '2017-10-05', 'Signed', 'Ameerul Zaki', 'Admin', '2017-10-05', 'Signed', 'Ameerul Zaki', 'Admin', '2017-10-05', 'Signed', 'Ali', 'Lecturer (2nd Examiner)', '2017-10-05', 'Signed', 'Ali', 'Lecturer (2nd Examiner)', '2017-10-05', 'Signed', 'Abu', 'Lecturer (1st Examiner)', '2017-10-05', 'Signed', 'Abu', 'Lecturer (1st Examiner)', '2017-10-05', 'Signed', 'Ameerul Zaki', 'Admin', '2017-10-05', 'Signed', 'Ameerul Zaki', 'Admin', '2017-10-05', 'Signed', 'Syakir', 'Examination Unit', '2017-10-05'),
(24, 32, 12, 13, 'Adam', 'MOS 100 - Diploma in Open Source Computing', 'DOS 1033', '2', 'Signed', 'Adam', 'Lecturer (1st Examiner)', '2017-10-05', 'Signed', 'Ameerul Zaki', 'Admin', '2017-10-05', 'Signed', 'Ameerul Zaki', 'Admin', '2017-10-05', 'Signed', 'Johnson', 'Lecturer (2nd Examiner)', '2017-10-05', 'Signed', 'Johnson', 'Lecturer (2nd Examiner)', '2017-10-05', 'Signed', 'Adam', 'Lecturer (1st Examiner)', '2017-10-05', 'Signed', 'Adam', 'Lecturer (1st Examiner)', '2017-10-05', 'Signed', 'Ameerul Zaki', 'Admin', '2017-10-05', 'Signed', 'Ameerul Zaki', 'Admin', '2017-10-05', 'Signed', 'Syakir', 'Examination Unit', '2017-10-05');

-- --------------------------------------------------------

--
-- Table structure for table `vetting_form`
--

CREATE TABLE `vetting_form` (
  `vetting_form_id` int(11) NOT NULL,
  `examination_paper_id` int(11) NOT NULL,
  `lecturer_id1` int(11) NOT NULL,
  `lecturer_id2` int(11) DEFAULT NULL,
  `semester` varchar(30) NOT NULL,
  `session` varchar(15) NOT NULL,
  `examination` varchar(15) NOT NULL,
  `course_code` varchar(10) NOT NULL,
  `course_name` varchar(100) NOT NULL,
  `questions_set` varchar(10) NOT NULL,
  `lecturer` varchar(100) NOT NULL,
  `course_name_yn` varchar(5) DEFAULT NULL,
  `course_name_remarks` varchar(50) DEFAULT NULL,
  `course_code_yn` varchar(5) DEFAULT NULL,
  `course_code_remarks` varchar(50) DEFAULT NULL,
  `examination_yn` varchar(5) DEFAULT NULL,
  `examination_remarks` varchar(50) DEFAULT NULL,
  `examination_duration_yn` varchar(5) DEFAULT NULL,
  `examination_duration_remarks` varchar(50) DEFAULT NULL,
  `cover_page_instructions_yn` varchar(5) DEFAULT NULL,
  `cover_page_instructions_remarks` varchar(50) DEFAULT NULL,
  `questions_instruction_parts_yn` varchar(5) DEFAULT NULL,
  `questions_instruction_parts_remarks` varchar(50) DEFAULT NULL,
  `mark_distribution_yn` varchar(5) DEFAULT NULL,
  `mark_distribution_remarks` varchar(50) DEFAULT NULL,
  `page_number_yn` varchar(5) DEFAULT NULL,
  `page_number_remarks` varchar(50) DEFAULT NULL,
  `questions_numbering_yn` varchar(5) DEFAULT NULL,
  `questions_numbering_remarks` varchar(50) DEFAULT NULL,
  `course_objectives_ana` varchar(20) DEFAULT NULL,
  `course_objectives_remarks` varchar(50) DEFAULT NULL,
  `course_synopsis_content_ana` varchar(20) DEFAULT NULL,
  `course_synopsis_content_remarks` varchar(50) DEFAULT NULL,
  `time_given_ana` varchar(20) DEFAULT NULL,
  `time_given_remarks` varchar(50) DEFAULT NULL,
  `language_standard_ana` varchar(20) DEFAULT NULL,
  `language_standard_remarks` varchar(50) DEFAULT NULL,
  `comment` varchar(1000) DEFAULT NULL,
  `declaration` varchar(10) DEFAULT NULL,
  `head` varchar(50) DEFAULT NULL,
  `signature1` varchar(10) DEFAULT NULL,
  `members` varchar(100) DEFAULT NULL,
  `signature2` varchar(10) DEFAULT NULL,
  `date1` date DEFAULT NULL,
  `signature3` varchar(10) DEFAULT NULL,
  `date2` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `vetting_form`
--

INSERT INTO `vetting_form` (`vetting_form_id`, `examination_paper_id`, `lecturer_id1`, `lecturer_id2`, `semester`, `session`, `examination`, `course_code`, `course_name`, `questions_set`, `lecturer`, `course_name_yn`, `course_name_remarks`, `course_code_yn`, `course_code_remarks`, `examination_yn`, `examination_remarks`, `examination_duration_yn`, `examination_duration_remarks`, `cover_page_instructions_yn`, `cover_page_instructions_remarks`, `questions_instruction_parts_yn`, `questions_instruction_parts_remarks`, `mark_distribution_yn`, `mark_distribution_remarks`, `page_number_yn`, `page_number_remarks`, `questions_numbering_yn`, `questions_numbering_remarks`, `course_objectives_ana`, `course_objectives_remarks`, `course_synopsis_content_ana`, `course_synopsis_content_remarks`, `time_given_ana`, `time_given_remarks`, `language_standard_ana`, `language_standard_remarks`, `comment`, `declaration`, `head`, `signature1`, `members`, `signature2`, `date1`, `signature3`, `date2`) VALUES
(28, 24, 3, 1, 'June 2017', '1', 'October 2017', 'UNI 1012', 'English 1', 'Set A', 'Aris Shaiful', 'Yes', '1', 'Yes', '2', 'No', '3', 'No', '4', 'Yes', '5', 'Yes', '6', 'No', '7', 'No', '8', 'Yes', '9', 'Appropriate', '10', 'Appropriate', '11', 'Not Appropriate', '12', 'Not Appropriate', '13', 'hai', 'Declare2', 'Ameerul Zaki', 'Signed', 'Azura', 'Signed', '2017-09-19', 'Signed', '2017-09-20'),
(29, 24, 3, 1, 'June 2017', '1', 'October 2017', 'UNI 1012', 'English 1', 'Set B', 'Aris Shaiful', 'No', '14', 'No', '15', 'Yes', '16', 'Yes', '17', 'No', '18', 'No', '19', 'Yes', '20', 'Yes', '21', 'No', '22', 'Appropriate', '23', 'Appropriate', '24', 'Not Appropriate', '25', 'Not Appropriate', '26', 'hello', 'Declare3', 'Ameerul Zaki', 'Signed', 'Azura', 'Signed', '2017-09-19', 'Signed', '2017-09-20'),
(30, 25, 1, 3, 'June 2017', '1', 'October 2017', 'DOS 3023', 'Innovation Management', 'Set A', 'Azura', 'No', '1', 'No', '2', 'Yes', '3', 'Yes', '4', 'No', '5', 'No', '6', 'Yes', '7', 'Yes', '8', 'No', '9', 'Appropriate', '10', 'Appropriate', '11', 'Not Appropriate', '12', 'Not Appropriate', '13', 'hai', 'Declare3', 'Ameerul Zaki', 'Signed', 'Aris Shaiful', 'Signed', '2017-09-21', 'Signed', '2017-09-21'),
(31, 25, 1, 3, 'June 2017', '1', 'October 2017', 'DOS 3023', 'Innovation Management', 'Set B', 'Azura', 'Yes', '', 'Yes', '', 'No', '', 'No', '', 'Yes', '', 'Yes', '', 'No', '', 'No', '', 'Yes', '', 'Not Appropriate', '', 'Not Appropriate', '', 'Appropriate', '', 'Appropriate', '', 'hello', 'Declare2', 'Ameerul Zaki', 'Signed', 'Aris Shaiful', 'Signed', '2017-09-21', 'Signed', '2017-09-21'),
(32, 26, 4, 3, 'June 2017', '1', 'October 2017', 'DOS 3033', 'Open Source Game Development', 'Set A', 'Hilmi', 'Yes', '', 'Yes', '', 'No', 'hai', 'No', '', 'Yes', '', 'Yes', '', 'No', '', 'No', '', 'Yes', '', 'Not Appropriate', '', 'Not Appropriate', '', 'Not Appropriate', '', 'Appropriate', '', '', 'Declare3', 'Ameerul Zaki', 'Signed', 'Aris Shaiful', 'Signed', '2017-09-21', 'Signed', '2017-09-21'),
(33, 26, 4, 3, 'June 2017', '1', 'October 2017', 'DOS 3033', 'Open Source Game Development', 'Set B', 'Hilmi', 'Yes', '', 'Yes', '', 'Yes', '', 'Yes', '', 'Yes', '', 'Yes', '', 'Yes', '', 'Yes', '', 'Yes', '', 'Appropriate', '', 'Appropriate', '', 'Appropriate', '', 'Appropriate', '', '', 'Declare2', 'Ameerul Zaki', 'Signed', 'Aris Shaiful', 'Signed', '2017-09-21', 'Signed', '2017-09-21'),
(34, 27, 5, 4, 'June 2017', '1', 'October 2017', 'MPU 2223', 'Study Skill', 'Set A', 'Faris', 'Yes', 'good', 'Yes', '', 'Yes', '', 'Yes', '', 'Yes', '', 'Yes', '', 'Yes', '', 'Yes', '', 'Yes', '', 'Appropriate', '', 'Appropriate', '', 'Appropriate', '', 'Appropriate', '', 'hi', 'Declare2', 'Ameerul Zaki', 'Signed', 'Hilmi', 'Signed', '2017-09-27', 'Signed', '2017-09-27'),
(35, 27, 5, 4, 'June 2017', '1', 'October 2017', 'MPU 2223', 'Study Skill', 'Set B', 'Faris', 'Yes', '', 'Yes', '', 'No', '', 'No', '', 'No', '', 'Yes', '', 'Yes', '', 'Yes', '', 'Yes', '', 'Appropriate', '', 'Not Appropriate', '', 'Appropriate', '', 'Appropriate', '', '', 'Declare3', 'Ameerul Zaki', 'Signed', 'Hilmi', 'Signed', '2017-09-27', 'Signed', '2017-09-27'),
(36, 28, 7, 6, 'June 2017', '1', 'October 2017', 'TNN 1233', 'Tech', 'Set A', 'Wan', 'No', '', 'No', 'li', 'Yes', '', 'Yes', '', 'No', '', 'No', '', 'Yes', '', 'Yes', '', 'Yes', '', 'Appropriate', '', 'Not Appropriate', '', 'Not Appropriate', '', 'Appropriate', '', '', 'Declare3', 'Ameerul Zaki', 'Signed', 'Anaqie', 'Signed', '2017-09-27', 'Signed', '2017-09-27'),
(37, 28, 7, 6, 'June 2017', '1', 'October 2017', 'TNN 1233', 'Tech', 'Set B', 'Wan', 'Yes', '', 'Yes', '', 'Yes', '', 'Yes', '', 'Yes', '', 'Yes', '', 'Yes', '', 'Yes', '', 'Yes', '', 'Appropriate', '', 'Appropriate', '', 'Appropriate', '', 'Appropriate', '', '', 'Declare2', 'Ameerul Zaki', 'Signed', 'Anaqie', 'Signed', '2017-09-27', 'Signed', '2017-09-27'),
(38, 29, 8, 4, 'June 2017', '1', 'October 2017', 'DOS 2113', 'Linux System Administration', 'Set A', 'Muzzammil', 'Yes', '', 'Yes', '', 'Yes', '', 'Yes', 'test', 'Yes', '', 'Yes', '', 'Yes', '', 'Yes', '', 'Yes', '', 'Appropriate', '', 'Appropriate', '', 'Appropriate', '', 'Appropriate', '', 'Hai', 'Declare2', 'Ameerul Zaki', 'Signed', 'Hilmi', 'Signed', '2017-10-05', 'Signed', '2017-10-19'),
(39, 29, 8, 4, 'June 2017', '1', 'October 2017', 'DOS 2113', 'Linux System Administration', 'Set B', 'Muzzammil', 'Yes', '', 'Yes', '', 'Yes', '', 'No', '', 'No', '', 'No', '', 'No', '', 'Yes', '', 'Yes', '', 'Appropriate', '', 'Not Appropriate', '', 'Not Appropriate', '', 'Not Appropriate', '', 'no', 'Declare3', 'Ameerul Zaki', 'Signed', 'Hilmi', 'Signed', '2017-10-05', 'Signed', '2017-10-19'),
(40, 30, 9, 8, 'June 2017', '1', 'October 2017', 'MAT 1023', 'Mathematics for IT', 'Set A', 'Shahrul', 'Yes', '', 'Yes', '', 'Yes', '', 'Yes', '', 'Yes', '', 'Yes', '', 'Yes', '', 'Yes', '', 'Yes', '', 'Appropriate', '', 'Appropriate', '', 'Appropriate', '', 'Appropriate', 'hai', 'la', NULL, NULL, NULL, 'Muzzammil', 'Signed', '2017-10-05', NULL, NULL),
(41, 30, 9, 8, 'June 2017', '1', 'October 2017', 'MAT 1023', 'Mathematics for IT', 'Set B', 'Shahrul', 'Yes', '', 'Yes', '', 'No', '', 'No', '', 'No', '', 'No', '', 'No', '', 'Yes', '', 'Yes', '', 'Not Appropriate', '', 'Not Appropriate', '', 'Not Appropriate', '', 'Not Appropriate', '', 'hello', NULL, NULL, NULL, 'Muzzammil', 'Signed', '2017-10-05', NULL, NULL),
(42, 31, 10, 11, 'June 2017', '1', 'October 2017', 'MPU 2213', 'Bahasa Kebangsaan A', 'Set A', 'Abu', 'Yes', '', 'Yes', '', 'Yes', '', 'Yes', '', 'Yes', '', 'Yes', '', 'Yes', '', 'Yes', '', 'Yes', '', 'Appropriate', 'lol', 'Appropriate', '', 'Appropriate', '', 'Appropriate', '', 'Hai', 'Declare1', 'Ameerul Zaki', 'Signed', 'Ali', 'Signed', '2017-10-05', 'Signed', '2017-10-05'),
(43, 31, 10, 11, 'June 2017', '1', 'October 2017', 'MPU 2213', 'Bahasa Kebangsaan A', 'Set B', 'Abu', 'Yes', '', 'Yes', '', 'Yes', '', 'No', '', 'No', '', 'No', '', 'No', '', 'No', '', 'Yes', '', 'Not Appropriate', '', 'Not Appropriate', '', 'Not Appropriate', '', 'Not Appropriate', '', 'hello', 'Declare3', 'Ameerul Zaki', 'Signed', 'Ali', 'Signed', '2017-10-05', 'Signed', '2017-10-05'),
(44, 32, 12, 13, 'June 2017', '1', 'October 2017', 'DOS 1033', 'Introduction to Computer Programming', 'Set A', 'Adam', 'No', '', 'No', '', 'No', '', 'No', '', 'No', '', 'Yes', 'remarks', 'Yes', '', 'No', '', 'No', '', 'Appropriate', '', 'Not Appropriate', '', 'Not Appropriate', '', 'Not Appropriate', '', 'comment', 'Declare3', 'Ameerul Zaki', 'Signed', 'Johnson', 'Signed', '2017-10-05', 'Signed', '2017-10-05'),
(45, 32, 12, 13, 'June 2017', '1', 'October 2017', 'DOS 1033', 'Introduction to Computer Programming', 'Set B', 'Adam', 'Yes', '', 'Yes', '', 'Yes', '', 'Yes', '', 'Yes', '', 'Yes', '', 'Yes', '', 'Yes', '', 'Yes', '', 'Appropriate', '', 'Appropriate', '', 'Appropriate', '', 'Appropriate', '', 'hello', 'Declare1', 'Ameerul Zaki', 'Signed', 'Johnson', 'Signed', '2017-10-05', 'Signed', '2017-10-05');

-- --------------------------------------------------------

--
-- Table structure for table `vetting_timetable`
--

CREATE TABLE `vetting_timetable` (
  `vetting_timetable_id` int(11) NOT NULL DEFAULT '1',
  `file` varchar(100) NOT NULL,
  `type` varchar(100) NOT NULL,
  `size` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `vetting_timetable`
--

INSERT INTO `vetting_timetable` (`vetting_timetable_id`, `file`, `type`, `size`) VALUES
(1, 'vetting_timetable.pdf', 'application/pdf', 349);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`admin_id`),
  ADD UNIQUE KEY `email` (`email`);

--
-- Indexes for table `announcement`
--
ALTER TABLE `announcement`
  ADD PRIMARY KEY (`announcement_id`);

--
-- Indexes for table `assign_lecturer`
--
ALTER TABLE `assign_lecturer`
  ADD PRIMARY KEY (`assign_lecturer_id`),
  ADD KEY `course_code` (`course_code`),
  ADD KEY `lecturer_id1` (`lecturer_id1`),
  ADD KEY `assign_lecturer_id2` (`lecturer_id2`);

--
-- Indexes for table `course`
--
ALTER TABLE `course`
  ADD PRIMARY KEY (`course_id`),
  ADD UNIQUE KEY `course_code` (`course_code`);

--
-- Indexes for table `examination_format`
--
ALTER TABLE `examination_format`
  ADD PRIMARY KEY (`examination_format_id`);

--
-- Indexes for table `examination_paper`
--
ALTER TABLE `examination_paper`
  ADD PRIMARY KEY (`examination_paper_id`);

--
-- Indexes for table `examination_unit`
--
ALTER TABLE `examination_unit`
  ADD PRIMARY KEY (`examination_unit_id`),
  ADD UNIQUE KEY `email` (`email`);

--
-- Indexes for table `lecturer`
--
ALTER TABLE `lecturer`
  ADD PRIMARY KEY (`lecturer_id`),
  ADD UNIQUE KEY `email` (`email`);

--
-- Indexes for table `session`
--
ALTER TABLE `session`
  ADD PRIMARY KEY (`session_id`),
  ADD UNIQUE KEY `session` (`session`);

--
-- Indexes for table `submission_form`
--
ALTER TABLE `submission_form`
  ADD PRIMARY KEY (`submission_form_id`),
  ADD KEY `examination_paper_id` (`examination_paper_id`),
  ADD KEY `lecturer_id1` (`lecturer_id1`),
  ADD KEY `lecturer_id2` (`lecturer_id2`);

--
-- Indexes for table `vetting_form`
--
ALTER TABLE `vetting_form`
  ADD PRIMARY KEY (`vetting_form_id`),
  ADD KEY `course_code` (`course_code`),
  ADD KEY `examination_paper_id` (`examination_paper_id`),
  ADD KEY `lecturer_id1` (`lecturer_id1`),
  ADD KEY `lecturer_id2` (`lecturer_id2`);

--
-- Indexes for table `vetting_timetable`
--
ALTER TABLE `vetting_timetable`
  ADD PRIMARY KEY (`vetting_timetable_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `admin_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `announcement`
--
ALTER TABLE `announcement`
  MODIFY `announcement_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `assign_lecturer`
--
ALTER TABLE `assign_lecturer`
  MODIFY `assign_lecturer_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;
--
-- AUTO_INCREMENT for table `course`
--
ALTER TABLE `course`
  MODIFY `course_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;
--
-- AUTO_INCREMENT for table `examination_paper`
--
ALTER TABLE `examination_paper`
  MODIFY `examination_paper_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;
--
-- AUTO_INCREMENT for table `examination_unit`
--
ALTER TABLE `examination_unit`
  MODIFY `examination_unit_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `lecturer`
--
ALTER TABLE `lecturer`
  MODIFY `lecturer_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `session`
--
ALTER TABLE `session`
  MODIFY `session_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `submission_form`
--
ALTER TABLE `submission_form`
  MODIFY `submission_form_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;
--
-- AUTO_INCREMENT for table `vetting_form`
--
ALTER TABLE `vetting_form`
  MODIFY `vetting_form_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=46;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `assign_lecturer`
--
ALTER TABLE `assign_lecturer`
  ADD CONSTRAINT `assign_course_code` FOREIGN KEY (`course_code`) REFERENCES `course` (`course_code`),
  ADD CONSTRAINT `assign_lecturer_id1` FOREIGN KEY (`lecturer_id1`) REFERENCES `lecturer` (`lecturer_id`),
  ADD CONSTRAINT `assign_lecturer_id2` FOREIGN KEY (`lecturer_id2`) REFERENCES `lecturer` (`lecturer_id`);

--
-- Constraints for table `submission_form`
--
ALTER TABLE `submission_form`
  ADD CONSTRAINT `submission_examination_paper` FOREIGN KEY (`examination_paper_id`) REFERENCES `examination_paper` (`examination_paper_id`),
  ADD CONSTRAINT `submission_lecturer_id1` FOREIGN KEY (`lecturer_id1`) REFERENCES `lecturer` (`lecturer_id`),
  ADD CONSTRAINT `submission_lecturer_id2` FOREIGN KEY (`lecturer_id2`) REFERENCES `lecturer` (`lecturer_id`);

--
-- Constraints for table `vetting_form`
--
ALTER TABLE `vetting_form`
  ADD CONSTRAINT `vetting_course_code` FOREIGN KEY (`course_code`) REFERENCES `course` (`course_code`),
  ADD CONSTRAINT `vetting_examination_paper_id` FOREIGN KEY (`examination_paper_id`) REFERENCES `examination_paper` (`examination_paper_id`),
  ADD CONSTRAINT `vetting_lecturer_id1` FOREIGN KEY (`lecturer_id1`) REFERENCES `lecturer` (`lecturer_id`),
  ADD CONSTRAINT `vetting_lecturer_id2` FOREIGN KEY (`lecturer_id2`) REFERENCES `lecturer` (`lecturer_id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
