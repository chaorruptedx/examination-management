-- phpMyAdmin SQL Dump
-- version 4.4.15.9
-- https://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Oct 31, 2019 at 10:40 AM
-- Server version: 5.6.37
-- PHP Version: 7.1.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `examination_management_v1`
--
CREATE DATABASE IF NOT EXISTS `examination_management_v1` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `examination_management_v1`;

-- --------------------------------------------------------

--
-- Table structure for table `announcement`
--

CREATE TABLE IF NOT EXISTS `announcement` (
  `id` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `announcement` text NOT NULL,
  `status` tinyint(2) NOT NULL DEFAULT '1' COMMENT '1 | 0 | -1',
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `attachment`
--

CREATE TABLE IF NOT EXISTS `attachment` (
  `id` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `id_vetting` int(11) DEFAULT NULL,
  `category` varchar(50) NOT NULL COMMENT 'timetable | exam_format | answer_format | question_set_a | answer_set_a | question_set_b | answer_set_b',
  `name` varchar(100) NOT NULL,
  `type` varchar(10) NOT NULL,
  `path` varchar(255) NOT NULL,
  `status` tinyint(2) DEFAULT '1' COMMENT '1 | 0 | -1',
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `attachment`
--

INSERT INTO `attachment` (`id`, `id_user`, `id_vetting`, `category`, `name`, `type`, `path`, `status`, `created_at`, `updated_at`) VALUES
(7, 3, NULL, 'exam_format', 'examination_format', 'pdf', '/upload/examination_format/examination_format.pdf', 1, '2019-10-31 00:00:00', NULL),
(8, 3, NULL, 'answer_format', 'answer_format', 'pdf', '/upload/examination_format/answer_format.pdf', 1, '2019-10-31 00:00:00', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `course`
--

CREATE TABLE IF NOT EXISTS `course` (
  `id` int(11) NOT NULL,
  `code` varchar(10) NOT NULL,
  `name` varchar(100) NOT NULL,
  `status` tinyint(2) NOT NULL DEFAULT '1' COMMENT '1 | 0 | -1',
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `course`
--

INSERT INTO `course` (`id`, `code`, `name`, `status`, `created_at`, `updated_at`) VALUES
(1, 'MPU 2213', 'Bahasa Kebangsaan A', 1, '2019-10-28 00:00:00', NULL),
(2, 'MPU 2223', 'Study Skill', 1, '2019-10-28 00:00:00', NULL),
(3, 'TEST 123', 'Testing', -1, '2019-10-28 00:00:00', '2019-10-28 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `examiner`
--

CREATE TABLE IF NOT EXISTS `examiner` (
  `id` int(11) NOT NULL,
  `category` varchar(20) NOT NULL COMMENT '1st_examiner | 2nd_examiner',
  `id_user` int(11) NOT NULL,
  `id_course` int(11) NOT NULL,
  `id_session` int(11) NOT NULL,
  `id_submission` int(11) DEFAULT NULL,
  `status` tinyint(2) NOT NULL DEFAULT '1' COMMENT '1 | 0 | -1',
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `examiner`
--

INSERT INTO `examiner` (`id`, `category`, `id_user`, `id_course`, `id_session`, `id_submission`, `status`, `created_at`, `updated_at`) VALUES
(1, '1st_examiner', 11, 1, 1, 2, 1, '2019-10-28 00:00:00', '2019-10-29 04:28:37'),
(2, '2nd_examiner', 12, 1, 1, 2, 1, '2019-10-28 00:00:00', '2019-10-29 04:28:37');

-- --------------------------------------------------------

--
-- Table structure for table `session`
--

CREATE TABLE IF NOT EXISTS `session` (
  `id` int(11) NOT NULL,
  `session` date NOT NULL,
  `status` tinyint(2) NOT NULL DEFAULT '1' COMMENT '1 | 0 | -1',
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `session`
--

INSERT INTO `session` (`id`, `session`, `status`, `created_at`, `updated_at`) VALUES
(1, '2017-06-01', 1, '2019-10-28 00:00:00', NULL),
(2, '2016-12-01', 1, '2019-10-28 00:00:00', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `submission`
--

CREATE TABLE IF NOT EXISTS `submission` (
  `id` int(11) NOT NULL,
  `programme_code` varchar(100) NOT NULL DEFAULT 'MOS 100 - Diploma in Open Source Computing' COMMENT 'MOS 100 - Diploma in Open Source Computing',
  `number_of_set` tinyint(2) NOT NULL DEFAULT '2' COMMENT '2',
  `a_signature_1` tinyint(1) NOT NULL COMMENT '1',
  `a_name_1` varchar(50) NOT NULL,
  `a_position_1` varchar(50) NOT NULL COMMENT 'Admin | Lecturer (1st Examiner) | Lecturer (2nd Examiner) | Examination Unit',
  `a_date_1` date NOT NULL,
  `a_signature_2` tinyint(1) DEFAULT NULL COMMENT '1',
  `a_name_2` varchar(50) DEFAULT NULL,
  `a_position_2` varchar(50) DEFAULT NULL COMMENT 'Admin | Lecturer (1st Examiner) | Lecturer (2nd Examiner) | Examination Unit',
  `a_date_2` date DEFAULT NULL,
  `b_signature_1` tinyint(1) DEFAULT NULL COMMENT '1',
  `b_name_1` varchar(50) DEFAULT NULL,
  `b_position_1` varchar(50) DEFAULT NULL COMMENT 'Admin | Lecturer (1st Examiner) | Lecturer (2nd Examiner) | Examination Unit',
  `b_date_1` date DEFAULT NULL,
  `b_signature_2` tinyint(1) DEFAULT NULL COMMENT '1',
  `b_name_2` varchar(50) DEFAULT NULL,
  `b_position_2` varchar(50) DEFAULT NULL COMMENT 'Admin | Lecturer (1st Examiner) | Lecturer (2nd Examiner) | Examination Unit',
  `b_date_2` date DEFAULT NULL,
  `c_signature_1` tinyint(1) DEFAULT NULL COMMENT '1',
  `c_name_1` varchar(50) DEFAULT NULL,
  `c_position_1` varchar(50) DEFAULT NULL COMMENT 'Admin | Lecturer (1st Examiner) | Lecturer (2nd Examiner) | Examination Unit',
  `c_date_1` date DEFAULT NULL,
  `c_signature_2` tinyint(1) DEFAULT NULL COMMENT '1',
  `c_name_2` varchar(50) DEFAULT NULL,
  `c_position_2` varchar(50) DEFAULT NULL COMMENT 'Admin | Lecturer (1st Examiner) | Lecturer (2nd Examiner) | Examination Unit',
  `c_date_2` date DEFAULT NULL,
  `d_signature_1` tinyint(1) DEFAULT NULL COMMENT '1',
  `d_name_1` varchar(50) DEFAULT NULL,
  `d_position_1` varchar(50) DEFAULT NULL COMMENT 'Admin | Lecturer (1st Examiner) | Lecturer (2nd Examiner) | Examination Unit',
  `d_date_1` date DEFAULT NULL,
  `d_signature_2` tinyint(1) DEFAULT NULL COMMENT '1',
  `d_name_2` varchar(50) DEFAULT NULL,
  `d_position_2` varchar(50) DEFAULT NULL COMMENT 'Admin | Lecturer (1st Examiner) | Lecturer (2nd Examiner) | Examination Unit',
  `d_date_2` date DEFAULT NULL,
  `e_signature_1` tinyint(1) DEFAULT NULL COMMENT '1',
  `e_name_1` varchar(50) DEFAULT NULL,
  `e_position_1` varchar(50) DEFAULT NULL COMMENT 'Admin | Lecturer (1st Examiner) | Lecturer (2nd Examiner) | Examination Unit',
  `e_date_1` date DEFAULT NULL,
  `e_signature_2` tinyint(4) DEFAULT NULL COMMENT '1',
  `e_name_2` varchar(50) DEFAULT NULL,
  `e_position_2` varchar(50) DEFAULT NULL COMMENT 'Admin | Lecturer (1st Examiner) | Lecturer (2nd Examiner) | Examination Unit',
  `e_date_2` date DEFAULT NULL,
  `progress_status` varchar(25) NOT NULL COMMENT 'First Submission | Vetting | Correction | Final Submission | Examination Unit | Completed',
  `status` tinyint(2) NOT NULL DEFAULT '1' COMMENT '1 | 0 | -1',
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `submission`
--

INSERT INTO `submission` (`id`, `programme_code`, `number_of_set`, `a_signature_1`, `a_name_1`, `a_position_1`, `a_date_1`, `a_signature_2`, `a_name_2`, `a_position_2`, `a_date_2`, `b_signature_1`, `b_name_1`, `b_position_1`, `b_date_1`, `b_signature_2`, `b_name_2`, `b_position_2`, `b_date_2`, `c_signature_1`, `c_name_1`, `c_position_1`, `c_date_1`, `c_signature_2`, `c_name_2`, `c_position_2`, `c_date_2`, `d_signature_1`, `d_name_1`, `d_position_1`, `d_date_1`, `d_signature_2`, `d_name_2`, `d_position_2`, `d_date_2`, `e_signature_1`, `e_name_1`, `e_position_1`, `e_date_1`, `e_signature_2`, `e_name_2`, `e_position_2`, `e_date_2`, `progress_status`, `status`, `created_at`, `updated_at`) VALUES
(2, 'MOS 100 - Diploma in Open Source Computing', 2, 1, 'Shahrul Nizam', 'Lecturer (1st Examiner)', '2019-10-29', 1, 'Ameerul Zaki', 'Admin', '2019-10-30', 1, 'Ameerul Zaki', 'Admin', '2019-10-30', 1, 'Mohd Asyraf', 'Lecturer (2nd Examiner)', '2019-10-30', 1, 'Mohd Asyraf', 'Lecturer (2nd Examiner)', '2019-10-30', 1, 'Shahrul Nizam', 'Lecturer (1st Examiner)', '2019-10-31', 1, 'Shahrul Nizam', 'Lecturer (1st Examiner)', '2019-10-31', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Final Submission', 1, '2019-10-29 04:28:37', '2019-10-31 07:20:03');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `id` int(11) NOT NULL,
  `category` varchar(50) NOT NULL COMMENT 'admin | lecturer | examination_unit',
  `email` varchar(50) NOT NULL,
  `username` varchar(50) NOT NULL,
  `password` varchar(255) NOT NULL,
  `status` tinyint(2) NOT NULL DEFAULT '1' COMMENT '1 | 0 | -1',
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `category`, `email`, `username`, `password`, `status`, `created_at`, `updated_at`) VALUES
(3, 'admin', 'zaki@gmail.com', 'Ameerul Zaki', '$2y$13$/ueMSTZT8sHnF763BQkAsO.RbsqSoxers7v47kONwzK/iiVl5wcGy', 1, '2019-10-22 02:50:58', '2019-10-26 00:00:00'),
(11, 'lecturer', 'shahrul@gmail.com', 'Shahrul Nizam', '$2y$13$3cfk60W0bUS2f3akJUGR4eiL7UyNakkFdXqkMX2FYOkNV9xdgtBq.', 1, '2019-10-26 09:41:44', NULL),
(12, 'lecturer', 'asyraf@gmail.com', 'Mohd Asyraf', '$2y$13$KdLyH3HdqPyWPF8NZ.kLNOlXZoUrSDAB9Tk7SQJ9K7sc9ESEwBN2i', 1, '2019-10-27 02:41:40', NULL),
(13, 'examination_unit', 'amin@gmail.com', 'Muhammad Amin', '$2y$13$Zd9b.gVSAEG4qL5j6ierTOiK.wv2aahN7EfuF8D2ZNMbnv/JzVuIm', 1, '2019-10-27 02:41:55', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `vetting`
--

CREATE TABLE IF NOT EXISTS `vetting` (
  `id` int(11) NOT NULL,
  `id_submission` int(11) NOT NULL,
  `session` tinyint(1) NOT NULL COMMENT '1 | 2',
  `examination` varchar(25) NOT NULL COMMENT 'The Date of Examination',
  `questions_set` varchar(10) NOT NULL COMMENT 'Set A | Set B',
  `lecturer` varchar(50) NOT NULL COMMENT '1st Examiner Name',
  `course_name_yn` varchar(5) DEFAULT NULL COMMENT 'Yes | No',
  `course_name_remarks` varchar(50) DEFAULT NULL,
  `course_code_yn` varchar(5) DEFAULT NULL COMMENT 'Yes | No',
  `course_code_remarks` varchar(50) DEFAULT NULL,
  `examination_yn` varchar(5) DEFAULT NULL COMMENT 'Yes | No',
  `examination_remarks` varchar(50) DEFAULT NULL,
  `examination_duration_yn` varchar(5) DEFAULT NULL COMMENT 'Yes | No',
  `examination_duration_remarks` varchar(50) DEFAULT NULL,
  `cover_page_instructions_yn` varchar(5) DEFAULT NULL COMMENT 'Yes | No',
  `cover_page_instructions_remarks` varchar(50) DEFAULT NULL,
  `questions_instruction_parts_yn` varchar(5) DEFAULT NULL COMMENT 'Yes | No',
  `questions_instruction_parts_remarks` varchar(50) DEFAULT NULL,
  `mark_distribution_yn` varchar(5) DEFAULT NULL COMMENT 'Yes | No',
  `mark_distribution_remarks` varchar(50) DEFAULT NULL,
  `page_number_yn` varchar(5) DEFAULT NULL COMMENT 'Yes | No',
  `page_number_remarks` varchar(50) DEFAULT NULL,
  `questions_numbering_yn` varchar(5) DEFAULT NULL COMMENT 'Yes | No',
  `questions_numbering_remarks` varchar(50) DEFAULT NULL,
  `course_objectives_ana` varchar(25) DEFAULT NULL COMMENT 'Appropriate | Not Appropriate',
  `course_objectives_remarks` varchar(50) DEFAULT NULL,
  `course_synopsis_content_ana` varchar(25) DEFAULT NULL COMMENT 'Appropriate | Not Appropriate',
  `course_synopsis_content_remarks` varchar(50) DEFAULT NULL,
  `time_given_ana` varchar(25) DEFAULT NULL COMMENT 'Appropriate | Not Appropriate',
  `time_given_remarks` varchar(50) DEFAULT NULL,
  `language_standard_ana` varchar(25) DEFAULT NULL COMMENT 'Appropriate | Not Appropriate',
  `language_standard_remarks` varchar(50) DEFAULT NULL,
  `comment` text,
  `declaration` tinyint(1) DEFAULT NULL COMMENT '1 | 2 | 3',
  `head` varchar(50) DEFAULT NULL COMMENT 'Admin Name',
  `signature_1` tinyint(1) DEFAULT NULL COMMENT '1',
  `members` varchar(50) DEFAULT NULL COMMENT '2nd Examiner Name',
  `signature_2` tinyint(1) DEFAULT NULL,
  `date_1` date DEFAULT NULL,
  `signature_3` tinyint(1) DEFAULT NULL COMMENT '1',
  `date_2` date DEFAULT NULL,
  `chosen_set` tinyint(2) DEFAULT NULL COMMENT '1 | 0',
  `status` tinyint(2) NOT NULL DEFAULT '1' COMMENT '1 | 0 | -1',
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `vetting`
--

INSERT INTO `vetting` (`id`, `id_submission`, `session`, `examination`, `questions_set`, `lecturer`, `course_name_yn`, `course_name_remarks`, `course_code_yn`, `course_code_remarks`, `examination_yn`, `examination_remarks`, `examination_duration_yn`, `examination_duration_remarks`, `cover_page_instructions_yn`, `cover_page_instructions_remarks`, `questions_instruction_parts_yn`, `questions_instruction_parts_remarks`, `mark_distribution_yn`, `mark_distribution_remarks`, `page_number_yn`, `page_number_remarks`, `questions_numbering_yn`, `questions_numbering_remarks`, `course_objectives_ana`, `course_objectives_remarks`, `course_synopsis_content_ana`, `course_synopsis_content_remarks`, `time_given_ana`, `time_given_remarks`, `language_standard_ana`, `language_standard_remarks`, `comment`, `declaration`, `head`, `signature_1`, `members`, `signature_2`, `date_1`, `signature_3`, `date_2`, `chosen_set`, `status`, `created_at`, `updated_at`) VALUES
(3, 2, 1, 'October 2019', 'Set A', 'Shahrul Nizam', 'No', 'cn', 'No', 'cc', 'Yes', 'ac', 'No', 'av', 'Yes', 'wd', 'Yes', 'fe', 'Yes', 'ef', 'Yes', 'qwe', 'No', '2e', 'Not Appropriate', 'wd', 'Not Appropriate', 'qwe', 'Not Appropriate', 'ds', 'Appropriate', 'asd', 'saddfgfdsa', NULL, NULL, NULL, 'Mohd Asyraf', 1, '2019-10-30', NULL, NULL, NULL, 1, '2019-10-29 04:28:37', '2019-10-30 02:00:21'),
(4, 2, 1, 'October 2019', 'Set B', 'Shahrul Nizam', 'Yes', '123', 'No', '434', 'Yes', '123', 'Yes', '434', 'Yes', '1236', 'Yes', '7678', 'No', '0890', 'No', '456', 'No', '234', 'Appropriate', '567', 'Not Appropriate', '678', 'Appropriate', '879', 'Not Appropriate', '532', '123456789', NULL, NULL, NULL, 'Mohd Asyraf', 1, '2019-10-30', NULL, NULL, NULL, 1, '2019-10-29 04:28:37', '2019-10-30 02:00:21');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `announcement`
--
ALTER TABLE `announcement`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_user_fk` (`id_user`);

--
-- Indexes for table `attachment`
--
ALTER TABLE `attachment`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_user_fk` (`id_user`),
  ADD KEY `id_vetting_fk` (`id_vetting`);

--
-- Indexes for table `course`
--
ALTER TABLE `course`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `code_unique` (`code`);

--
-- Indexes for table `examiner`
--
ALTER TABLE `examiner`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_user_fk` (`id_user`) USING BTREE,
  ADD KEY `id_course_fk` (`id_course`) USING BTREE,
  ADD KEY `id_session_fk` (`id_session`) USING BTREE,
  ADD KEY `id_submission_fk` (`id_submission`);

--
-- Indexes for table `session`
--
ALTER TABLE `session`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `session` (`session`);

--
-- Indexes for table `submission`
--
ALTER TABLE `submission`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `email_unique` (`email`);

--
-- Indexes for table `vetting`
--
ALTER TABLE `vetting`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_submission_fk` (`id_submission`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `announcement`
--
ALTER TABLE `announcement`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `attachment`
--
ALTER TABLE `attachment`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `course`
--
ALTER TABLE `course`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `examiner`
--
ALTER TABLE `examiner`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `session`
--
ALTER TABLE `session`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `submission`
--
ALTER TABLE `submission`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `vetting`
--
ALTER TABLE `vetting`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `announcement`
--
ALTER TABLE `announcement`
  ADD CONSTRAINT `announcement_ibfk_1` FOREIGN KEY (`id_user`) REFERENCES `user` (`id`);

--
-- Constraints for table `attachment`
--
ALTER TABLE `attachment`
  ADD CONSTRAINT `attachment_ibfk_1` FOREIGN KEY (`id_user`) REFERENCES `user` (`id`),
  ADD CONSTRAINT `attachment_ibfk_2` FOREIGN KEY (`id_vetting`) REFERENCES `vetting` (`id`);

--
-- Constraints for table `examiner`
--
ALTER TABLE `examiner`
  ADD CONSTRAINT `examiner_ibfk_1` FOREIGN KEY (`id_user`) REFERENCES `user` (`id`),
  ADD CONSTRAINT `examiner_ibfk_2` FOREIGN KEY (`id_course`) REFERENCES `course` (`id`),
  ADD CONSTRAINT `examiner_ibfk_3` FOREIGN KEY (`id_session`) REFERENCES `session` (`id`),
  ADD CONSTRAINT `examiner_ibfk_4` FOREIGN KEY (`id_submission`) REFERENCES `submission` (`id`);

--
-- Constraints for table `vetting`
--
ALTER TABLE `vetting`
  ADD CONSTRAINT `vetting_ibfk_1` FOREIGN KEY (`id_submission`) REFERENCES `submission` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
