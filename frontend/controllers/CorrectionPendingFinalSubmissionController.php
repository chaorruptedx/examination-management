<?php

namespace frontend\controllers;

use Yii;
use yii\base\Model;
use common\models\Attachment;
use common\models\Examiner;
use common\models\Submission;
use common\models\Vetting;
use common\models\CorrectionPendingFinalSubmissionSearch;
use yii\helpers\Url;
use yii\web\UploadedFile;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * CorrectionPendingFinalSubmissionController implements the CRUD actions for Examiner model.
 */
class CorrectionPendingFinalSubmissionController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function init()
    {    
        if (Yii::$app->user->identity->category != 'lecturer') {
            
            return $this->redirect(Url::base(true));
        }
    }

    /**
     * Lists all Examiner models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new CorrectionPendingFinalSubmissionSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionFinalSubmission($id_submission)
    {
        $model = Submission::find()
            ->where([
                'id' => $id_submission
            ])
            ->one();

        $model->scenario = Submission::SCENARIO_FINAL_SUBMISSION;

        $modelVettings = Vetting::find()
            ->where([
                'id_submission' => $id_submission,
                'status' => '1',
            ])
            ->orderBy(['questions_set' => SORT_ASC])
            ->all();

        $modelAttachmentsView = [new Attachment()]; // Array Model
        $modelAttachmentsView[0] = Attachment::find()
            ->where([
                'id_vetting' => $modelVettings[0]->id,
                'category' => 'question_set_a',
                'status' => '1',
            ])
            ->one(); // Question Set A
        $modelAttachmentsView[1] = Attachment::find()
            ->where([
                'id_vetting' => $modelVettings[0]->id,
                'category' => 'answer_set_a',
                'status' => '1',
            ])
            ->one(); // Answer Set A
        $modelAttachmentsView[2] = Attachment::find()
            ->where([
                'id_vetting' => $modelVettings[1]->id,
                'category' => 'question_set_b',
                'status' => '1',
            ])
            ->one(); // Question Set B
        $modelAttachmentsView[3] = Attachment::find()
            ->where([
                'id_vetting' => $modelVettings[1]->id,
                'category' => 'answer_set_b',
                'status' => '1',
            ])
            ->one(); // Answer Set B
        
        $modelAttachments = [new Attachment()]; // Array Model
        $modelAttachments[0] = new Attachment(); // Question Set A
        $modelAttachments[1] = new Attachment(); // Answer Set A
        $modelAttachments[2] = new Attachment(); // Question Set B
        $modelAttachments[3] = new Attachment(); // Answer Set B
        
        if ($model->load(Yii::$app->request->post()) && Model::loadMultiple($modelAttachments, Yii::$app->request->post())) {
            
            $model->c_date_2 = date('Y-m-d');
            $model->d_date_1 = date('Y-m-d');
            $model->progress_status = 'Final Submission';
            $model->updated_at = date('Y-m-d h:i:s', time());
            
            $model->save();

            $modelAttachments[0]->question_set_a = UploadedFile::getInstances($modelAttachments[0], '[0]question_set_a');
            $modelAttachments[1]->answer_set_a = UploadedFile::getInstances($modelAttachments[1], '[1]answer_set_a');
            $modelAttachments[2]->question_set_b = UploadedFile::getInstances($modelAttachments[2], '[2]question_set_b');
            $modelAttachments[3]->answer_set_b = UploadedFile::getInstances($modelAttachments[3], '[3]answer_set_b');

            $tableInfo[id_user] = Yii::$app->user->identity->id;
            $tableInfo[folder] = 'examination_paper';
            
            $tableInfo[id_vetting] = $modelVettings[0]->id;
            $modelAttachments[0]->upload($tableInfo);
            $modelAttachments[1]->upload($tableInfo);

            $tableInfo[id_vetting] = $modelVettings[1]->id;
            $modelAttachments[2]->upload($tableInfo);
            $modelAttachments[3]->upload($tableInfo);
            
            Yii::$app->session->setFlash('success', 'Form has been submitted to Admin.');

            return $this->redirect(['/progress-status/index']);
        }

        return $this->render('final-submission', [
            'model' => $model,
            'modelVettings' => $modelVettings,
            'modelAttachmentsView' => $modelAttachmentsView,
            'modelAttachments' => $modelAttachments,
        ]);
    }

    /**
     * Finds the Examiner model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Examiner the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Examiner::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
