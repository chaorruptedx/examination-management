<?php

namespace frontend\controllers;

use Yii;
use common\models\Examiner;
use common\models\ExaminationArchiveSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * ExaminationArchiveController implements the CRUD actions for Examiner model.
 */
class ExaminationArchiveController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Examiner models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ExaminationArchiveSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionViewFile($id, $filename, $filetype)
    {
        $storagePath = Yii::getAlias('@upload/examination_paper');

        $filefullname = $filename.'.'.$filetype;
        
        if (!is_file("$storagePath/$id/$filefullname")) {
            throw new \yii\web\NotFoundHttpException('The file does not exists.');
        }

        return Yii::$app->response->sendFile("$storagePath/$id/$filefullname", $filefullname);
    }

    /**
     * Finds the Examiner model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Examiner the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Examiner::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
