<?php

namespace frontend\controllers;

use Yii;
use yii\helpers\Url;
use yii\base\Model;
use common\models\Attachment;
use common\models\Examiner;
use common\models\Submission;
use common\models\Vetting;
use common\models\ExaminationPendingSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * ExaminationPendingController implements the CRUD actions for Examiner model.
 */
class ExaminationPendingController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function init()
    {    
        if (Yii::$app->user->identity->category != 'examination_unit') {
            
            return $this->redirect(Url::base(true));
        }
    }

    /**
     * Lists all Examiner models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ExaminationPendingSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionCompleteSubmission($id_submission)
    {
        $model = Submission::find()
            ->where([
                'id' => $id_submission
            ])
            ->one();

        // $model->scenario = Submission::SCENARIO_COMPLETE_SUBMISSION;

        $modelVettings = Vetting::find()
            ->where([
                'id_submission' => $id_submission,
                'status' => '1',
            ])
            ->orderBy(['questions_set' => SORT_ASC])
            ->all();

        $modelAttachmentsView = [new Attachment()]; // Array Model
        $modelAttachmentsView[0] = Attachment::find()
            ->where([
                'id_vetting' => $modelVettings[0]->id,
                'category' => 'question_set_a',
                'status' => '1',
            ])
            ->one(); // Question Set A
        $modelAttachmentsView[1] = Attachment::find()
            ->where([
                'id_vetting' => $modelVettings[0]->id,
                'category' => 'answer_set_a',
                'status' => '1',
            ])
            ->one(); // Answer Set A
        $modelAttachmentsView[2] = Attachment::find()
            ->where([
                'id_vetting' => $modelVettings[1]->id,
                'category' => 'question_set_b',
                'status' => '1',
            ])
            ->one(); // Question Set B
        $modelAttachmentsView[3] = Attachment::find()
            ->where([
                'id_vetting' => $modelVettings[1]->id,
                'category' => 'answer_set_b',
                'status' => '1',
            ])
            ->one(); // Answer Set B
        
        if ($model->load(Yii::$app->request->post())) {
            
            $model->e_date_2 = date('Y-m-d');
            $model->progress_status = 'Completed';
            $model->updated_at = date('Y-m-d h:i:s', time());
            
            $model->save();
            
            Yii::$app->session->setFlash('success', 'Form submission has been completed.');

            return $this->redirect(['/examination-question/index']);
        }

        return $this->render('complete-submission', [
            'model' => $model,
            'modelVettings' => $modelVettings,
            'modelAttachmentsView' => $modelAttachmentsView,
        ]);
    }

    /**
     * Finds the Examiner model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Examiner the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Examiner::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
