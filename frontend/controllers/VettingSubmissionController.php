<?php

namespace frontend\controllers;

use Yii;
use yii\base\Model;
use yii\helpers\Url;
use yii\helpers\Json;
use yii\web\Controller;
use yii\web\UploadedFile;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

use common\models\Session;
use common\models\Attachment;
use common\models\Examiner;
use common\models\Submission;
use common\models\Vetting;

use common\components\CourseHelper;

/**
 * VettingSubmissionController implements the CRUD actions for Submission model.
 */
class VettingSubmissionController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function init()
    {    
        if (Yii::$app->user->identity->category != 'lecturer') {
            
            return $this->redirect(Url::base(true));
        }
    }

    /**
     * Lists all Submission models.
     * @return mixed
     */
    public function actionIndex()
    {
        $session = Session::find()
            ->select(['MAX(session) as max_session'])
            ->where([
                'session.status' => '1'
            ])
            ->asArray()
            ->one(); // Get Max Session
        
        $session_max = Session::find()
            ->select(['id'])
            ->where([
                'session' => $session['max_session'],
                'status' => '1',
            ])
            ->asArray()
            ->one(); // Get Max Session ID
            
        $modelAttachments = [new Attachment()]; // Array Model
        $modelAttachments[0] = new Attachment(); // Question Set A
        $modelAttachments[1] = new Attachment(); // Answer Set A
        $modelAttachments[2] = new Attachment(); // Question Set B
        $modelAttachments[3] = new Attachment(); // Answer Set B

        $model = new Submission(['scenario' => Submission::SCENARIO_FIRST_SUBMISSION]); // Submission Form
        
        $modelVettings = [new Vetting()]; // Array Model
        $modelVettings[0] = new Vetting(); // 1st Vetting Form
        $modelVettings[1] = new Vetting(); // 2nd Vetting Form
        
        if ($model->load(Yii::$app->request->post()) && Model::loadMultiple($modelVettings, Yii::$app->request->post()) && Model::loadMultiple($modelAttachments, Yii::$app->request->post())) {

            $model->a_date_1 = date('Y-m-d');
            $model->progress_status = 'First Submission';
            $model->status = '1';
            $model->created_at = date('Y-m-d h:i:s', time());
            
            $model->save();

            $modelExaminers = Examiner::find()
                ->where([
                    'id_course' => $model->course,
                    'id_session' => $session_max['id'],
                ])
                ->all();

            foreach ($modelExaminers as $modelExaminer) {

                $modelExaminer->id_submission = $model->id;
                $modelExaminer->updated_at = date('Y-m-d h:i:s', time());

                $modelExaminer->save();
            }

            $modelVettings[0]->id_submission = $model->id;
            $modelVettings[0]->status = '1';
            $modelVettings[0]->created_at = date('Y-m-d h:i:s', time());

            $modelVettings[0]->save();

            $modelVettings[1]->id_submission = $model->id;
            $modelVettings[1]->session = $modelVettings[0]->session;
            $modelVettings[1]->examination = $modelVettings[0]->examination;
            $modelVettings[1]->status = '1';
            $modelVettings[1]->created_at = date('Y-m-d h:i:s', time());

            $modelVettings[1]->save();

            $modelAttachments[0]->question_set_a = UploadedFile::getInstances($modelAttachments[0], '[0]question_set_a');
            $modelAttachments[1]->answer_set_a = UploadedFile::getInstances($modelAttachments[1], '[1]answer_set_a');
            $modelAttachments[2]->question_set_b = UploadedFile::getInstances($modelAttachments[2], '[2]question_set_b');
            $modelAttachments[3]->answer_set_b = UploadedFile::getInstances($modelAttachments[3], '[3]answer_set_b');

            $tableInfo[id_user] = Yii::$app->user->identity->id;
            $tableInfo[folder] = 'examination_paper';
            
            $tableInfo[id_vetting] = $modelVettings[0]->id;
            $modelAttachments[0]->upload($tableInfo);
            $modelAttachments[1]->upload($tableInfo);

            $tableInfo[id_vetting] = $modelVettings[1]->id;
            $modelAttachments[2]->upload($tableInfo);
            $modelAttachments[3]->upload($tableInfo);

            Yii::$app->session->setFlash('success', 'Form has been submitted to admin.');

            return $this->redirect(['/progress-status/index']);
        }

        return $this->render('index', [
            'modelAttachments' => $modelAttachments,
            'model' => $model,
            'modelVettings' => $modelVettings,
            'session' => $session,
        ]);
    }

    public function actionGetCourseCodeName()
    {
        $id_course = Yii::$app->request->post('course');
        
        $data = CourseHelper::get_course_code_name($id_course);

        return Json::encode($data);
    }

    /**
     * Finds the Submission model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Submission the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Submission::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
