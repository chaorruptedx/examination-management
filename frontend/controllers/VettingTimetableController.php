<?php

namespace frontend\controllers;

use Yii;
use common\models\Attachment;
use common\models\VettingTimetableSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * VettingTimetableController implements the CRUD actions for Attachment model.
 */
class VettingTimetableController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Attachment models.
     * @return mixed
     */
    public function actionIndex()
    {
        $vettingTimetable = Attachment::find()
            ->where([
                'category' => 'timetable',
                'status' => '1'
            ])
            ->asArray()
            ->one();

        return $this->render('index', [
            'vettingTimetable' => $vettingTimetable,
        ]);
    }

    /**
     * Finds the Attachment model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Attachment the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Attachment::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
