<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use yii\widgets\Pjax;

use common\components\CustomDialog;

Pjax::begin(['id' => 'dialogresfresh']);
echo CustomDialog::Dialogcus($type ="TYPE_DANGER", $title='Remove Course', $btnOKClass='btn-danger',$libName='krajeeDialogdelete');
Pjax::end();

$this->title = 'Course';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="course-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Course', ['create'], ['class' => 'btnmodalcustom btn btn-success', 'data-title' => 'Create Course']) ?>
    </p>

    <?php Pjax::begin(['id' => 'idPjaxGridViewCourse', 'timeout' => false, 'enablePushState' => false]); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'id' => 'idPjaxGridViewCourse',
        'pjax' => true,
        'pjaxSettings' => [
            'options' => [
                'enablePushState' => false,
            ]
        ],

        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,

        'toggleDataOptions' => [
            'all' => Yii::$app->params['gridviewToggleDataOptions']['all'],
            'page' => Yii::$app->params['gridviewToggleDataOptions']['page'],
        ],
        'export' => [
            'header' => Yii::$app->params['gridviewExportCustom']['header'],
            'label' => Yii::$app->params['gridviewExportCustom']['label'],
            'options' => ['class' => Yii::$app->params['gridviewExportCustom']['optionClass']],
            'showConfirmAlert' => Yii::$app->params['gridviewExportCustom']['showConfirmAlert'],
            'target' => GridView::TARGET_SELF
        ],
        'exportConfig' => [
            GridView::EXCEL => [
                'label' => Yii::$app->params['gridviewExportCustom']['labelEXCEL'], 
                'filename' => $this->title,
            ],
        ], 
        'emptyCell' => '-',
        'emptyText' => 'No results found.',
        'formatter' =>  [
                'class' => 'yii\i18n\Formatter',
                'nullDisplay' => '-'
            ],
        'layout'=> Yii::$app->params['gridviewCustomExamination']['layout'],
        'summary' => 'Showing <b>{begin}-{end}</b> of <b>{totalCount}</b> Courses.',
        'pager' => Yii::$app->params['pagecustomexamination'],

        'columns' => [
            [
                'header' => 'No.',
                'class' => 'kartik\grid\SerialColumn'
            ],

            [
                'label' => 'Course Code',
                'attribute' => 'code',
            ],

            [
                'label' => 'Course Name',
                'attribute' => 'name',
            ],

            [
                
                'header' => 'Actions',
                'class' => 'kartik\grid\ActionColumn',
                'template' => '{delete}', //{view} {update}
                'buttons' => [
                    // 'view' => function ($url, $model, $key) {
                    //     $options = [
                    //         'title' => Yii::t('yii', 'Paparan'),
                    //         'aria-label' => Yii::t('yii', 'Paparan'),
                    //         'data-pjax' => '0',
                    //         'data-title' => 'Paparan Maklumat',
                    //         'class'=>'btnmodalcustom',
                    //     ];

                    //     return Html::a('<span class="glyphicon glyphicon-eye-open"></span>', $url, $options);
                    // },

                    // 'update' => function ($url, $model, $key) {
                    //     $options = [
                    //         'title' => Yii::t('yii', 'Kemaskini'),
                    //         'aria-label' => Yii::t('yii', 'Kemaskini'),
                    //         'data-pjax' => '0',
                    //         'data-title' => 'Kemas Kini Maklumat',
                    //         'class' => 'btnmodalcustom',
                    //     ];
                           
                    //     return Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url, $options);
                    // },     
                    
                    'delete' => function ($url, $model, $key) {
                        $options = [
                            'id'=>'course-delete',
                            'title_delete' => Yii::t('yii', 'Remove'),
                            'aria-label' => Yii::t('yii', 'Remove'),
                            'data-pjax' => '0',

                            'class'=>'deletecustomgrid',
                            'data-confirmcustom'=>'Are you sure to remove this course?',
                            'data-url'=>$url,

                        ];

                        return Html::a('<span class="glyphicon glyphicon-trash"></span>', $url, $options);

                    }, 

                ]
            ],
        ],
    ]); ?>

    <?php Pjax::end(); ?>

</div>
