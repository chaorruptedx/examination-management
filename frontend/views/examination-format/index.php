<?php

use yii\helpers\Url;
use yii\helpers\Html;
use yii\widgets\Pjax;
// dd(Yii::$app->request->baseUrl);
// use kartik\grid\GridView;
/* @var $this yii\web\View */
/* @var $searchModel common\models\ExaminationFormatSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Examination Format';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="examination-format-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?//= Html::a('Upload Examination Format', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php Pjax::begin(); ?>

    <table width="100%" border="0">
  		<tbody>
    	<tr><?php if (!empty($examinationFormat['path'])) { ?>
     		<td align="center">&nbsp;<object width="550" height="750" data="<?= Yii::$app->request->baseUrl ?><?= $examinationFormat['path']; ?>">></td>
            <?php } ?>
            <?php if (!empty($answerFormat['path'])) { ?>
     		<td align="center">&nbsp;<object width="550" height="750" data="<?= Yii::$app->request->baseUrl ?><?= $answerFormat['path']; ?>"></td>
             <?php } ?>
    	</tr>
  		</tbody>
	</table>

    <?php Pjax::end(); ?>

</div>
