<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

use kartik\select2\Select2;
use kartik\date\DatePicker;

use common\models\Bankstatus;

$listCourse = Bankstatus::listCourse();

/* @var $this yii\web\View */
/* @var $model common\models\Examiner */

$this->title = 'Complete Submission';
$this->params['breadcrumbs'][] = ['label' => 'Examination Pending', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="examiner-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <?php $form = ActiveForm::begin(); ?>

	<table align="center" width="60%" border="0">
	  <tbody>
		<tr>
		  <td width="30%"><b>Examination Question (Set A)</b></td>
		  <td width="10%">:</td>
		  <td align="center" width="20%"><a href="<?= Yii::$app->request->baseUrl ?><?= $modelAttachmentsView[0]->path; ?>" target="_blank" class="btn btn-block btn-success"><span class="glyphicon glyphicon-file"></span>&nbsp;&nbsp;View File</a></td>
		</tr>
		<tr>
		  <td><b>Answer Scheme Format (Set A)</b></td>
		  <td>:</td>
		  <td align="center"><a href="<?= Yii::$app->request->baseUrl ?><?= $modelAttachmentsView[1]->path; ?>" target="_blank" class="btn btn-block btn-success"><span class="glyphicon glyphicon-file"></span>&nbsp;&nbsp;View File</a></td>
		</tr>
		<tr>
		  <td align="center">&nbsp;</td>
		  <td>&nbsp;</td>
		  <td align="center">&nbsp;</td>
		</tr>
		<tr>
		  <td><b>Examination Question (Set B)</b></td>
		  <td>:</td>
		  <td align="center"><a href="<?= Yii::$app->request->baseUrl ?><?= $modelAttachmentsView[2]->path; ?>" target="_blank" class="btn btn-block btn-success"><span class="glyphicon glyphicon-file"></span>&nbsp;&nbsp;View File</a></td>
		</tr>
		<tr>
		  <td><b>Answer Scheme Format (Set B)</b></td>
		  <td>:</td>
		  <td align="center"><a href="<?= Yii::$app->request->baseUrl ?><?= $modelAttachmentsView[3]->path; ?>" target="_blank" class="btn btn-block btn-success"><span class="glyphicon glyphicon-file"></span>&nbsp;&nbsp;View File</a></td>
		</tr>
	  </tbody>
	</table>

	<br>
    
    <table align="center" width="80%" border="1">
  		<tbody>
    		<tr>
      			<td bgcolor="white">
      			<br>
      			<br>
      			<p align="center"><img src="<?= Yii::$app->request->baseUrl.'/image/Melaka International College of Science and Technology - Logo (New).png'?>" width="143" height="200" alt=""/></p>
      			<br>
      			<h4 align="center"><b><u>BORANG SERAHAN KERTAS SOALAN DAN SKEMA JAWAPAN PEPERIKSAAN AKHIR</u></b></h4>
      			<br>
      			<table align="center" width="80%" border="1">
				<tbody>
					<tr>
					  <td>
					  	<br>
					  	<p><b><i>&nbsp;A. PENYERAHAN KEPADA JAWATANKUASA PENILAIAN DAN PEPERIKSAAN</i></b></p>
					  	<table align="center" width="95%" border="0">
						  <tbody>
							<tr>
							  <td align="center" width="5%">1.</td>
							  <td width="30%">Nama Pensyarah</td>
							  <td width="1%">:</td>
							  <td width="59%"><?= $form->field($model, 'lecturer_name')->textInput(['value' => $model->firstExaminer->user->username, 'disabled' => true])->label(false); ?></td>
							</tr>
							<tr>
							  <td align="center">2.</td>
							  <td>Nama Program / Kod Program</td>
							  <td>:</td>
							  <td>
							  	<?= $form->field($model, 'programme_code')->widget(Select2::classname(), [
                                        'disabled' => true,
										'data' => $listProgramme,
										'options' => ['placeholder' => 'Select a programme ...'],
										'pluginOptions' => [
											'allowClear' => true,
										],
								])->label(false); ?>
							  </td>
							</tr>
							<tr>
							  <td align="center">3.</td>
							  <td>Nama Subjek / Kod Subjek</td>
							  <td>:</td>
							  <td>
								<?= $form->field($model, 'course')->widget(Select2::classname(), [
                                    'disabled' => true,
									'data' => $listCourse,
									'options' => ['placeholder' => 'Select a course ...', 'value' => $model->firstExaminer->course->id],
									'pluginOptions' => [
										'allowClear' => true,
									],
								])->label(false); ?>
							  </td>
							</tr>
							<tr>
							  <td align="center">4.</td>
							  <td>Bilangan Set</td>
							  <td>:</td>
							  <td><?= $form->field($model, 'number_of_set')->textInput(['value' => '2', 'disabled' => true])->label(false); ?></td>
							</tr>
						  </tbody>
						</table>
						<br>
						<table align="center" width="95%" border="0">
						  <tbody>
							<tr>
							  <td colspan="3" width="45%">Diserahkan oleh:</td>
							  <td width="10%">&nbsp;</td>
							  <td colspan="3" width="45%">Diterima oleh:</td>
							</tr>
							<tr>
							  <td colspan="3" align="center"><?= $form->field($model, 'a_signature_1')->checkbox(array('disabled' => true, 'label' => false)); ?></td>
							  <td>&nbsp;</td>
							  <td colspan="3" align="center"><?= $form->field($model, 'a_signature_2')->checkbox(array('disabled' => true, 'label' => false)); ?></td>
							</tr>
							<tr>
							  <td colspan="3" align="center"><?= $form->field($model, 'a_name_1')->textInput(['value' => $model->a_name_1, 'disabled' => true])->label(false); ?></td>
							  <td>&nbsp;</td>
							  <td colspan="3" align="center"><?= $form->field($model, 'a_name_2')->textInput(['value' => $model->a_name_2, 'disabled' => true])->label(false); ?></td>
							</tr>
							<tr>
							  <td width="10%">Jawatan</td>
							  <td width="1%">:</td>
							  <td><?= $form->field($model, 'a_position_1')->textInput(['value' => 'Lecturer (1st Examiner)', 'disabled' => true])->label(false); ?></td>
							  <td>&nbsp;</td>
							  <td width="10%">Jawatan</td>
							  <td width="1%">:</td>
							  <td><?= $form->field($model, 'a_position_2')->textInput(['value' => 'Admin', 'disabled' => true])->label(false); ?></td>
							</tr>
							<tr>
							  <td>Tarikh</td>
							  <td width="1%">:</td>
							  <td>
								<?= $form->field($model, 'a_date_1')->widget(DatePicker::classname(), [
                                    'disabled' => true,
									'type' => DatePicker::TYPE_INPUT,
									'options' => ['value' => str_replace("-", "/", date('d-m-Y', strtotime($model->a_date_1)))],
									'pluginOptions' => [
										'autoclose' => true,
										'format' => 'dd/mm/yyyy'
									]
								])->label(false); ?>
							  </td>
							  <td>&nbsp;</td>
							  <td>Tarikh</td>
							  <td width="1%">:</td>
							  <td>
							  	<?= $form->field($model, 'a_date_2')->widget(DatePicker::classname(), [
                                    'disabled' => true,
                                    'type' => DatePicker::TYPE_INPUT,
                                    'options' => ['value' => str_replace("-", "/", date('d-m-Y', strtotime($model->a_date_2)))],
									'pluginOptions' => [
										'autoclose' => true,
										'format' => 'dd/mm/yyyy'
									]
								])->label(false); ?>
							  </td>
							</tr>
						  </tbody>
						</table>
						<br>
					  </td>
					</tr>
					<tr>
					  <td>
					  	<br>
					  	<p><b><i>&nbsp;B. PENYERAHAN UNTUK PROSES PENYARINGAN</i></b></p>
					  	<br>
					  	<table align="center" width="95%" border="0">
						  <tbody>
							<tr>
							  <td colspan="3" width="45%">Diserahkan oleh:</td>
							  <td width="10%">&nbsp;</td>
							  <td colspan="3" width="45%">Diterima oleh:</td>
							</tr>
							<tr>
							  <td colspan="3" align="center"><?= $form->field($model, 'b_signature_1')->checkbox(array('disabled' => true, 'label' => false)); ?></td>
							  <td>&nbsp;</td>
							  <td colspan="3" align="center"><?= $form->field($model, 'b_signature_2')->checkbox(array('disabled' => true, 'label' => false)); ?></td>
							</tr>
							<tr>
							  <td colspan="3" align="center"><?= $form->field($model, 'b_name_1')->textInput(['value' => $model->b_name_1, 'disabled' => true])->label(false); ?></td>
							  <td>&nbsp;</td>
							  <td colspan="3" align="center"><?= $form->field($model, 'b_name_2')->textInput(['value' => $model->secondExaminer->user->username, 'disabled' => true])->label(false); ?></td>
							</tr>
							<tr>
							  <td width="10%">Jawatan</td>
							  <td width="1%">:</td>
							  <td><?= $form->field($model, 'b_position_1')->textInput(['value' => 'Admin', 'disabled' => true])->label(false); ?></td>
							  <td>&nbsp;</td>
							  <td width="10%">Jawatan</td>
							  <td width="1%">:</td>
							  <td><?= $form->field($model, 'b_position_2')->textInput(['value' => 'Lecturer (2nd Examiner)', 'disabled' => true])->label(false); ?></td>
							</tr>
							<tr>
							  <td>Tarikh</td>
							  <td width="1%">:</td>
							  <td>
							  	<?= $form->field($model, 'b_date_1')->widget(DatePicker::classname(), [
                                        'disabled' => true,
                                        'type' => DatePicker::TYPE_INPUT,
                                        'options' => ['value' => str_replace("-", "/", date('d-m-Y', strtotime($model->b_date_1)))],
										'pluginOptions' => [
											'autoclose' => true,
											'format' => 'dd/mm/yyyy'
										]
									])->label(false);
								?>
							  </td>
							  <td>&nbsp;</td>
							  <td>Tarikh</td>
							  <td width="1%">:</td>
							  <td>
							  	<?= $form->field($model, 'b_date_2')->widget(DatePicker::classname(), [
								  	'disabled' => true,
                                    'type' => DatePicker::TYPE_INPUT,
                                    'options' => ['value' => str_replace("-", "/", date('d-m-Y', strtotime($model->b_date_2)))],
									'pluginOptions' => [
										'autoclose' => true,
										'format' => 'dd/mm/yyyy'
									]
								])->label(false); ?>
							  </td>
							</tr>
						  </tbody>
						</table>
					  	<br>
					  </td>
					</tr>
					<tr>
					  <td>
					  	<br>
					  	<p><b><i>&nbsp;C. SERAHAN SEMULA KEPADA PENSYARAH UNTUK PROSES PEMBETULAN</i></b></p>
					  	<br>
					  	<table align="center" width="95%" border="0">
						  <tbody>
							<tr>
							  <td colspan="3" width="45%">Diserahkan oleh:</td>
							  <td width="10%">&nbsp;</td>
							  <td colspan="3" width="45%">Diterima oleh:</td>
							</tr>
							<tr>
							  <td colspan="3" align="center"><?= $form->field($model, 'c_signature_1')->checkbox(array('disabled' => true, 'label' => false)); ?></td>
							  <td>&nbsp;</td>
							  <td colspan="3" align="center"><?= $form->field($model, 'c_signature_2')->checkbox(array('disabled' => true, 'label' => false)); ?></td>
							</tr>
							<tr>
							  <td colspan="3" align="center"><?= $form->field($model, 'c_name_1')->textInput(['value' => $model->secondExaminer->user->username, 'disabled' => true])->label(false); ?></td>
							  <td>&nbsp;</td>
							  <td colspan="3" align="center"><?= $form->field($model, 'c_name_2')->textInput(['value' => $model->firstExaminer->user->username, 'disabled' => true])->label(false); ?></td>
							</tr>
							<tr>
							  <td width="10%">Jawatan</td>
							  <td width="1%">:</td>
							  <td><?= $form->field($model, 'c_position_1')->textInput(['value' => 'Lecturer (2nd Examiner)', 'disabled' => true])->label(false); ?></td>
							  <td>&nbsp;</td>
							  <td width="10%">Jawatan</td>
							  <td width="1%">:</td>
							  <td><?= $form->field($model, 'c_position_2')->textInput(['value' => 'Lecturer (1st Examiner)', 'disabled' => true])->label(false); ?></td>
							</tr>
							<tr>
							  <td>Tarikh</td>
							  <td width="1%">:</td>
							  <td>
							  	<?= $form->field($model, 'c_date_1')->widget(DatePicker::classname(), [
								  	'disabled' => true,
                                    'type' => DatePicker::TYPE_INPUT,
                                    'options' => ['value' => str_replace("-", "/", date('d-m-Y', strtotime($model->c_date_1)))],
									'pluginOptions' => [
										'autoclose' => true,
										'format' => 'dd/mm/yyyy'
									]
								])->label(false); ?>
							  </td>
							  <td>&nbsp;</td>
							  <td>Tarikh</td>
							  <td width="1%">:</td>
							  <td>
							  	<?= $form->field($model, 'c_date_2')->widget(DatePicker::classname(), [
								  	'disabled' => true,
                                    'type' => DatePicker::TYPE_INPUT,
                                    'options' => ['value' => str_replace("-", "/", date('d-m-Y', strtotime($model->c_date_2)))],
									'pluginOptions' => [
										'autoclose' => true,
										'format' => 'dd/mm/yyyy'
									]
								])->label(false); ?>
							  </td>
							</tr>
						  </tbody>
						</table>
					  	<br>
					  </td>
					</tr>
					<tr>
					  <td>
					  	<br>
					  	<p><b><i>&nbsp;D. SERAHAN KEPADA JAWATANKUASA PENILAIAN DAN PEPERIKSAAN</i></b></p>
					  	<br>
					  	<table align="center" width="95%" border="0">
						  <tbody>
							<tr>
							  <td colspan="3" width="45%">Diserahkan oleh:</td>
							  <td width="10%">&nbsp;</td>
							  <td colspan="3" width="45%">Diterima oleh:</td>
							</tr>
							<tr>
							  <td colspan="3" align="center"><?= $form->field($model, 'd_signature_1')->checkbox(array('disabled' => true, 'label' => false)); ?></td>
							  <td>&nbsp;</td>
							  <td colspan="3" align="center"><?= $form->field($model, 'd_signature_2')->checkbox(array('disabled' => true, 'label' => false)); ?></td>
							</tr>
							<tr>
							  <td colspan="3" align="center"><?= $form->field($model, 'd_name_1')->textInput(['value' => $model->d_name_1, 'disabled' => true])->label(false); ?></td>
							  <td>&nbsp;</td>
							  <td colspan="3" align="center"><?= $form->field($model, 'd_name_2')->textInput(['value' => $model->d_name_2, 'disabled' => true])->label(false); ?></td>
							</tr>
							<tr>
							  <td width="10%">Jawatan</td>
							  <td width="1%">:</td>
							  <td><?= $form->field($model, 'd_position_1')->textInput(['value' => 'Lecturer (1st Examiner)', 'disabled' => true])->label(false); ?></td>
							  <td>&nbsp;</td>
							  <td width="10%">Jawatan</td>
							  <td width="1%">:</td>
							  <td><?= $form->field($model, 'd_position_2')->textInput(['value' => 'Admin', 'disabled' => true])->label(false); ?></td>
							</tr>
							<tr>
							  <td>Tarikh</td>
							  <td width="1%">:</td>
							  <td>
								<?= $form->field($model, 'd_date_1')->widget(DatePicker::classname(), [
								  	'disabled' => true,
                                    'type' => DatePicker::TYPE_INPUT,
                                    'options' => ['value' => str_replace("-", "/", date('d-m-Y', strtotime($model->d_date_1)))],
									'pluginOptions' => [
										'autoclose' => true,
										'format' => 'dd/mm/yyyy'
									]
								])->label(false); ?>
							  </td>
							  <td>&nbsp;</td>
							  <td>Tarikh</td>
							  <td width="1%">:</td>
							  <td>
								<?= $form->field($model, 'd_date_2')->widget(DatePicker::classname(), [
								  	'disabled' => true,
                                    'type' => DatePicker::TYPE_INPUT,
                                    'options' => ['value' => str_replace("-", "/", date('d-m-Y', strtotime($model->d_date_2)))],
									'pluginOptions' => [
										'autoclose' => true,
										'format' => 'dd/mm/yyyy'
									]
								])->label(false); ?>
							  </td>
							</tr>

						  </tbody>
						</table>
					  	<br>
					  </td>
					</tr>
					<tr>
					  <td>
					  	<br>
					  	<p><b><i>&nbsp;E. SERAHAN KEPADA UNIT PEPERIKSAAN</i></b></p>
					  	<br>
					  	<table align="center" width="95%" border="0">
						  <tbody>
							<tr>
							  <td colspan="3" width="45%">Diserahkan oleh:</td>
							  <td width="10%">&nbsp;</td>
							  <td colspan="3" width="45%">Diterima oleh:</td>
							</tr>
							<tr>
							  <td colspan="3" align="center"><?= $form->field($model, 'e_signature_1')->checkbox(array('disabled' => true, 'label' => false)); ?></td>
							  <td>&nbsp;</td>
							  <td colspan="3" align="center"><?= $form->field($model, 'e_signature_2')->checkbox(array('label' => false)); ?></td>
							</tr>
							<tr>
							  <td colspan="3" align="center"><?= $form->field($model, 'e_name_1')->textInput(['value' => $model->e_name_1, 'disabled' => true])->label(false); ?></td>
							  <td>&nbsp;</td>
							  <td colspan="3" align="center"><?= $form->field($model, 'e_name_2')->textInput(['value' => Yii::$app->user->identity->username, 'readonly' => true])->label(false); ?></td>
							</tr>
							<tr>
							  <td width="10%">Jawatan</td>
							  <td width="1%">:</td>
							  <td><?= $form->field($model, 'e_position_1')->textInput(['value' => 'Admin', 'disabled' => true])->label(false); ?></td>
							  <td>&nbsp;</td>
							  <td width="10%">Jawatan</td>
							  <td width="1%">:</td>
							  <td><?= $form->field($model, 'e_position_2')->textInput(['value' => 'Examination Unit', 'readonly' => true])->label(false); ?></td>
							</tr>
							<tr>
							  <td>Tarikh</td>
							  <td width="1%">:</td>
							  <td>
								<?= $form->field($model, 'e_date_1')->widget(DatePicker::classname(), [
								  	'disabled' => true,
                                    'type' => DatePicker::TYPE_INPUT,
                                    'options' => ['value' => str_replace("-", "/", date('d-m-Y', strtotime($model->d_date_2)))],
									'pluginOptions' => [
										'autoclose' => true,
										'format' => 'dd/mm/yyyy'
									]
								])->label(false); ?>
							  </td>
							  <td>&nbsp;</td>
							  <td>Tarikh</td>
							  <td width="1%">:</td>
							  <td>
							  	<?= $form->field($model, 'e_date_2')->widget(DatePicker::classname(), [
								  	'disabled' => true,
                                    'type' => DatePicker::TYPE_INPUT,
                                    'options' => ['value' => date('d/m/Y')],
									'pluginOptions' => [
										'autoclose' => true,
										'format' => 'dd/mm/yyyy'
									]
								])->label(false); ?>
							  </td>
							</tr>
						  </tbody>
						</table>
					  	<br>
					  </td>
					</tr>
				</tbody>
				</table>
				<table align="center" width="80%" border="0">
				  <tbody>
					<tr>
					  <td><p><i>* Penting :Borang yang telah LENGKAP melalui semua proses yang sewajarnya akan disimpan oleh Unit Peperiksaan untuk melengkapkan semua maklumat yang diperlukan oleh Bahagian Percetakan.</i></p></td>
					</tr>
				  </tbody>
				</table>
      			<br>
      			<br>
      			</td>
    		</tr>
  		</tbody>
	</table>

	<br>
  	   	   	   	
   	<table align="center" width="80%" border="1">
  		<tbody>
    		<tr>
      			<td bgcolor="white">
			    <br>
			    <br>
			    <p align="center"><img src="<?= Yii::$app->request->baseUrl.'/image/Melaka International College of Science and Technology - Logo (New).png'?>" width="143" height="200" alt=""/></p>
			    <br>
				<h4 align="center"><b>EXAMINATION VETTING FORM</b></h4>
	    		<br>
		    	<table align="center" width="80%" border="0">
  				<tbody>
    				<tr>
      				<td width="15%">SEMESTER</td>
      				<td width="1%">:</td>
      				<td><?= $form->field($modelVettings[0], '[0]session_session')->textInput(['value' => date("F Y", strtotime($model->firstExaminer->session->session)), 'disabled' => true])->label(false); ?></td>
    				</tr>
    				<tr>
      				<td>SESSION</td>
      				<td>:</td>
      				<td>
					  	<?= $form->field($modelVettings[0], '[0]session')->widget(Select2::classname(), [
                                'disabled' => true,
								'data' => $listSession,
								'options' => ['id' => 'vetting-session', 'placeholder' => 'Select a session ...'],
								'pluginOptions' => [
									'allowClear' => true,
								],
						])->label(false); ?>
					</td>
    				</tr>
    				<tr>
      				<td>EXAMINATION</td>
      				<td>:</td>
      				<td>
					  	<?= $form->field($modelVettings[0], '[0]examination')->widget(DatePicker::classname(), [
                            'disabled' => true,
							'type' => DatePicker::TYPE_INPUT,
							'options' => ['placeholder' => 'Select a date ...'],
							'pluginOptions' => [
								'autoclose' => true,
								'startView' => 'year',
                        		'minViewMode' => 'months',
								'format' => 'MM yyyy'
							]
						])->label(false); ?>  
					</td>
    				</tr>
  				</tbody>
				</table>
				<p align="center"><b>______________________________________________________________________________________________</b></p>
				<table align="center" width="80%" border="0">
  				<tbody>
    				<tr>
      				<td width="15%">Course Code</td>
      				<td width="1%">:</td>
      				<td><?= $form->field($modelVettings[0], '[0]course_code')->textInput(['value' => $model->firstExaminer->course->code, 'disabled' => true])->label(false); ?></td>
    				</tr>
    				<tr>
      				<td>Course Name</td>
      				<td>:</td>
      				<td><?= $form->field($modelVettings[0], '[0]course_name')->textInput(['value' => $model->firstExaminer->course->name, 'disabled' => true])->label(false); ?></td>
    				</tr>
    				<tr>
      				<td>Question's Set</td>
      				<td>:</td>
      				<td><?= $form->field($modelVettings[0], '[0]questions_set')->textInput(['value' => 'Set A', 'disabled' => true])->label(false); ?></td>
    				</tr>
    				<tr>
      				<td>Lecturer</td>
      				<td>:</td>
      				<td><?= $form->field($modelVettings[0], '[0]lecturer')->textInput(['value' => $model->firstExaminer->user->username, 'disabled' => true])->label(false); ?></td>
    				</tr>
  				</tbody>
				</table>
				<br>
				<table align="center" width="80%" border="0">
					<tbody>
					<tr>
					<td><b><u>CHECK LIST</u></b></td>
					</tr>
					</tbody>
				</table>
				<table align="center" width="80%" border="1">
  				<tbody>
    				<tr>
					<th scope="col" width="5%"><p align="center">No.</p></th>
      				<th cope="col" width="30%"><p align="center">Format</p></th>
      				<th scope="col" width="10%"><p align="center">Yes</p></th>
      				<th scope="col" width="10%"><p align="center">No</p></th>
      				<th scope="col" width="25%"><p align="center">Remarks</p></th>
    				</tr>
    				<tr>
      				<td align="center">1.</td>
      				<td>&nbsp;Course Name</td>
      				<td align="center"><?= $form->field($modelVettings[0], '[0]course_name_yn')->radio(array('label' => false, 'value' => 'Yes', 'uncheck' => null, 'disabled' => true)); ?></td>
      				<td align="center"><?= $form->field($modelVettings[0], '[0]course_name_yn')->radio(array('label' => false, 'value' => 'No', 'uncheck' => null, 'disabled' => true)); ?></td>
      				<td><?= $form->field($modelVettings[0], '[0]course_name_remarks')->textInput(['disabled' => true])->label(false); ?></td>
    				</tr>
    				<tr>
      				<td align="center">2.</td>
      				<td>&nbsp;Course Code</td>
      				<td align="center"><?= $form->field($modelVettings[0], '[0]course_code_yn')->radio(array('label' => false, 'value' => 'Yes', 'uncheck' => null, 'disabled' => true)); ?></td>
      				<td align="center"><?= $form->field($modelVettings[0], '[0]course_code_yn')->radio(array('label' => false, 'value' => 'No', 'uncheck' => null, 'disabled' => true)); ?></td>
      				<td><?= $form->field($modelVettings[0], '[0]course_code_remarks')->textInput(['disabled' => true])->label(false); ?></td>
    				</tr>
    				<tr>
      				<td align="center">3.</td>
      				<td>&nbsp;Examination</td>
      				<td align="center"><?= $form->field($modelVettings[0], '[0]examination_yn')->radio(array('label' => false, 'value' => 'Yes', 'uncheck' => null, 'disabled' => true)); ?></td>
      				<td align="center"><?= $form->field($modelVettings[0], '[0]examination_yn')->radio(array('label' => false, 'value' => 'No', 'uncheck' => null, 'disabled' => true)); ?></td>
      				<td><?= $form->field($modelVettings[0], '[0]examination_remarks')->textInput(['disabled' => true])->label(false); ?></td>
    				</tr>
    				<tr>
      				<td align="center">4.</td>
      				<td>&nbsp;Examination Duration</td>
      				<td align="center"><?= $form->field($modelVettings[0], '[0]examination_duration_yn')->radio(array('label' => false, 'value' => 'Yes', 'uncheck' => null, 'disabled' => true)); ?></td>
      				<td align="center"><?= $form->field($modelVettings[0], '[0]examination_duration_yn')->radio(array('label' => false, 'value' => 'No', 'uncheck' => null, 'disabled' => true)); ?></td>
      				<td><?= $form->field($modelVettings[0], '[0]examination_duration_remarks')->textInput(['disabled' => true])->label(false); ?></td>
    				</tr>
    				<tr>
      				<td align="center">5.</td>
      				<td>&nbsp;Cover page instructions</td>
      				<td align="center"><?= $form->field($modelVettings[0], '[0]cover_page_instructions_yn')->radio(array('label' => false, 'value' => 'Yes', 'uncheck' => null, 'disabled' => true)); ?></td>
      				<td align="center"><?= $form->field($modelVettings[0], '[0]cover_page_instructions_yn')->radio(array('label' => false, 'value' => 'No', 'uncheck' => null, 'disabled' => true)); ?></td>
      				<td><?= $form->field($modelVettings[0], '[0]cover_page_instructions_remarks')->textInput(['disabled' => true])->label(false); ?></td>
    				</tr>
    				<tr>
      				<td align="center">6.</td>
      				<td>&nbsp;Question's instruction / Parts</td>
      				<td align="center"><?= $form->field($modelVettings[0], '[0]questions_instruction_parts_yn')->radio(array('label' => false, 'value' => 'Yes', 'uncheck' => null, 'disabled' => true)); ?></td>
      				<td align="center"><?= $form->field($modelVettings[0], '[0]questions_instruction_parts_yn')->radio(array('label' => false, 'value' => 'No', 'uncheck' => null, 'disabled' => true)); ?></td>
      				<td><?= $form->field($modelVettings[0], '[0]questions_instruction_parts_remarks')->textInput(['disabled' => true])->label(false); ?></td>
    				</tr>
    				<tr>
      				<td align="center">7.</td>
      				<td>&nbsp;Marks distribution for each marks</td>
      				<td align="center"><?= $form->field($modelVettings[0], '[0]mark_distribution_yn')->radio(array('label' => false, 'value' => 'Yes', 'uncheck' => null, 'disabled' => true)); ?></td>
      				<td align="center"><?= $form->field($modelVettings[0], '[0]mark_distribution_yn')->radio(array('label' => false, 'value' => 'No', 'uncheck' => null, 'disabled' => true)); ?></td>
      				<td><?= $form->field($modelVettings[0], '[0]mark_distribution_remarks')->textInput(['disabled' => true])->label(false); ?></td>
    				</tr>
    				<tr>
      				<td align="center">8.</td>
      				<td>&nbsp;Number of each page</td>
      				<td align="center"><?= $form->field($modelVettings[0], '[0]page_number_yn')->radio(array('label' => false, 'value' => 'Yes', 'uncheck' => null, 'disabled' => true)); ?></td>
      				<td align="center"><?= $form->field($modelVettings[0], '[0]page_number_yn')->radio(array('label' => false, 'value' => 'No', 'uncheck' => null, 'disabled' => true)); ?></td>
      				<td><?= $form->field($modelVettings[0], '[0]page_number_remarks')->textInput(['disabled' => true])->label(false); ?></td>
    				</tr>
    				<tr>
      				<td align="center">9.</td>
      				<td>&nbsp;Question's numbering system</td>
      				<td align="center"><?= $form->field($modelVettings[0], '[0]questions_numbering_yn')->radio(array('label' => false, 'value' => 'Yes', 'uncheck' => null, 'disabled' => true)); ?></td>
      				<td align="center"><?= $form->field($modelVettings[0], '[0]questions_numbering_yn')->radio(array('label' => false, 'value' => 'No', 'uncheck' => null, 'disabled' => true)); ?></td>
      				<td><?= $form->field($modelVettings[0], '[0]questions_numbering_remarks')->textInput(['disabled' => true])->label(false); ?></td>
    				</tr>
  				</tbody>
				</table>
				<br>
				<table align="center" width="80%" border="0">
					<tbody>
					<tr>
					<td><b><u>CONTENT</u></b></td>
					</tr>
					</tbody>
				</table>
				<table align="center" width="80%" border="1">
  				<tbody>
    				<tr>
					<th scope="col" width="5%"><p align="center">No.</p></th>
      				<th cope="col" width="30%"><p align="center">Contents</p></th>
      				<th scope="col" width="10%"><p align="center">Appropriate</p></th>
      				<th scope="col" width="10%"><p align="center">Not Appropriate</p></th>
      				<th scope="col" width="25%"><p align="center">Remarks</p></th>
    				</tr>
    				<tr>
      				<td align="center">1.</td>
      				<td>&nbsp;Illustrate the objectives of the course</td>
      				<td align="center"><?= $form->field($modelVettings[0], '[0]course_objectives_ana')->radio(array('label' => false, 'value' => 'Appropriate', 'uncheck' => null, 'disabled' => true)); ?></td>
      				<td align="center"><?= $form->field($modelVettings[0], '[0]course_objectives_ana')->radio(array('label' => false, 'value' => 'Not Appropriate', 'uncheck' => null, 'disabled' => true)); ?></td>
      				<td><?= $form->field($modelVettings[0], '[0]course_objectives_remarks')->textInput(['disabled' => true])->label(false); ?></td>
    				</tr>
    				<tr>
      				<td align="center">2.</td>
      				<td>&nbsp;Fulfill course synopsis/content</td>
      				<td align="center"><?= $form->field($modelVettings[0], '[0]course_synopsis_content_ana')->radio(array('label' => false, 'value' => 'Appropriate', 'uncheck' => null, 'disabled' => true)); ?></td>
      				<td align="center"><?= $form->field($modelVettings[0], '[0]course_synopsis_content_ana')->radio(array('label' => false, 'value' => 'Not Appropriate', 'uncheck' => null, 'disabled' => true)); ?></td>
      				<td><?= $form->field($modelVettings[0], '[0]course_synopsis_content_remarks')->textInput(['disabled' => true])->label(false); ?></td>
    				</tr>
    				<tr>
      				<td align="center">3.</td>
      				<td>&nbsp;Allocated time given</td>
      				<td align="center"><?= $form->field($modelVettings[0], '[0]time_given_ana')->radio(array('label' => false, 'value' => 'Appropriate', 'uncheck' => null, 'disabled' => true)); ?></td>
      				<td align="center"><?= $form->field($modelVettings[0], '[0]time_given_ana')->radio(array('label' => false, 'value' => 'Not Appropriate', 'uncheck' => null, 'disabled' => true)); ?></td>
      				<td><?= $form->field($modelVettings[0], '[0]time_given_remarks')->textInput(['disabled'])->label(false); ?></td>
    				</tr>
    				<tr>
      				<td align="center">4.</td>
      				<td>&nbsp;Language standard/spelling/term</td>
      				<td align="center"><?= $form->field($modelVettings[0], '[0]language_standard_ana')->radio(array('label' => false, 'value' => 'Appropriate', 'uncheck' => null, 'disabled' => true)); ?></td>
      				<td align="center"><?= $form->field($modelVettings[0], '[0]language_standard_ana')->radio(array('label' => false, 'value' => 'Not Appropriate', 'uncheck' => null, 'disabled' => true)); ?></td>
      				<td><?= $form->field($modelVettings[0], '[0]language_standard_remarks')->textInput(['disabled' => true])->label(false); ?></td>
    				</tr>
  				</tbody>
				</table>
				<br>
				<table align="center" width="80%" border="0">
  				<tbody>
    				<tr>
      				<td>Comment:</td>
    				</tr>
    				<tr>
      				<td><?= $form->field($modelVettings[0], '[0]comment')->textInput(['disabled' => true])->label(false); ?></td>
    				</tr>
  				</tbody>
				</table>
				<br>
				<table align="center" width="80%" border="0">
					<tbody>
					<tr>
					<td><b>Declaration:</b></td>
					</tr>
					</tbody>
				</table>
				<table align="center" width="80%" border="0">
  				<tbody>
    				<tr>
      				<td width="5%" align="center">1)</td>
      				<td width="10%" align="center"><?= $form->field($modelVettings[0], '[0]declaration')->radio(array('label' => false, 'value' => '1', 'uncheck' => null, 'required' => true)); ?></td>
      				<td width="65%">We have checked this question paper and declare that it is appropriate to be printed out.</td>
    				</tr>
    				<tr>
      				<td align="center">2)</td>
      				<td align="center"><?= $form->field($modelVettings[0], '[0]declaration')->radio(array('label' => false, 'value' => '2', 'uncheck' => null, 'required' => true)); ?></td>
      				<td>We have checked this question paper and declare that it is appropriate to be printed out in accordance with the corrections that have been suggested.
					</td>
    				</tr>
    				<tr>
      				<td align="center">3)</td>
      				<td align="center"><?= $form->field($modelVettings[0], '[0]declaration')->radio(array('label' => false, 'value' => '3', 'uncheck' => null, 'required' => true)); ?></td>
      				<td>We have checked this question paper and declare that it is not appropriate to be printed out.</td>
    				</tr>
  				</tbody>
				</table>
				<br>
				<table align="center" width="80%" border="0">
				<tbody>
					<tr>
					<th scope="col" colspan="3"><p align="center">Examination Question Vetting Committee</p></th>
					<th width="20%" scope="col"><p align="center">Signature</p></th>
					</tr>
					<tr>
					<td width="15%">&nbsp;Head</td>
					<td width="1%">:</td>
					<td width="44%"><?= $form->field($modelVettings[0], '[0]head')->textInput(['value' => $modelVettings[0]->head, 'disabled' => true])->label(false); ?></td>
					<td align="center"><?= $form->field($modelVettings[0], '[0]signature_1')->checkbox(array('disabled' => true, 'label' => false)); ?></td>
					</tr>
					<tr>
					<td>&nbsp;Members</td>
					<td>:</td>
					<td><?= $form->field($modelVettings[0], '[0]members')->textInput(['value' => $model->secondExaminer->user->username, 'disabled' => true])->label(false); ?></td>
					<td align="center"><?= $form->field($modelVettings[0], '[0]signature_2')->checkbox(array('disabled' => true, 'label' => false)); ?></td>
					</tr>
					<tr>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					</tr>
					<tr>
					<td>&nbsp;Date</td>
					<td>:</td>
					<td>
						<?= $form->field($modelVettings[0], '[0]date_1')->widget(DatePicker::classname(), [
							'disabled' => true,
                            'type' => DatePicker::TYPE_INPUT,
                            'options' => ['value' => str_replace("-", "/", date('d-m-Y', strtotime($modelVettings[0]->date_1)))],
							'pluginOptions' => [
								'autoclose' => true,
								'format' => 'dd/mm/yyyy'
							]
						])->label(false); ?>
					</td>
					<td>&nbsp;</td>
					</tr>
				</tbody>
				</table>
				<p align="center">______________________________________________________________________________________________</p>
				<table align="center" width="80%" border="0">
					<tbody>
					<tr>
					<td><b>Declaration to accept the amendment of the question paper :</b></td>
					</tr>
					</tbody>
				</table>
				<table align="center" width="80%" border="0">
				<tbody>
					<tr>
					  <td colspan="7">&nbsp;I have checked and declare that this question paper has been corrected and appropriate to be printed out.</td>
					</tr>
					<tr>
					  <td width="10%">&nbsp;Signature</td>
					  <td width="1%">:</td>
					  <td width="20%" align="center"><?= $form->field($modelVettings[0], '[0]signature_3')->checkbox(array('disabled' => true, 'label' => false)); ?></td>
					  <td width="18%">&nbsp;</td>
					  <td width="10%">&nbsp;Date</td>
					  <td width="1%">:</td>
					  <td width="20%">
					  	<?= $form->field($modelVettings[0], '[0]date_2')->widget(DatePicker::classname(), [
							'disabled' => true,
                            'type' => DatePicker::TYPE_INPUT,
                            'options' => ['value' => str_replace("-", "/", date('d-m-Y', strtotime($modelVettings[0]->date_2)))],
							'pluginOptions' => [
								'autoclose' => true,
								'format' => 'dd/mm/yyyy'
							]
						])->label(false); ?>
					</td>
					</tr>
					<tr>
					  <td>&nbsp;</td>
					  <td>&nbsp;</td>
					  <td align="center"><b>Head Examination Question Vetting</b></td>
					  <td>&nbsp;</td>
					  <td>&nbsp;</td>
					  <td>&nbsp;</td>
					  <td>&nbsp;</td>
					</tr>
				</tbody>
				</table>
				<br>
				<br>
			    </td>
    		</tr>
  		</tbody>
	</table>
   	
   	<br>

	   <table align="center" width="80%" border="1">
  		<tbody>
    		<tr>
      			<td bgcolor="white">
			    <br>
			    <br>
			    <p align="center"><img src="<?= Yii::$app->request->baseUrl.'/image/Melaka International College of Science and Technology - Logo (New).png'?>" width="143" height="200" alt=""/></p>
			    <br>
				<h4 align="center"><b>EXAMINATION VETTING FORM</b></h4>
	    		<br>
		    	<table align="center" width="80%" border="0">
  				<tbody>
    				<tr>
      				<td width="15%">SEMESTER</td>
      				<td width="1%">:</td>
      				<td><?= $form->field($modelVettings[1], '[1]session_session')->textInput(['value' => date("F Y", strtotime($model->firstExaminer->session->session)), 'disabled' => true])->label(false); ?></td>
    				</tr>
    				<tr>
      				<td>SESSION</td>
      				<td>:</td>
      				<td>
					  	<?= $form->field($modelVettings[1], '[1]session')->widget(Select2::classname(), [
							  	'disabled' => true,
								'data' => $listSession,
								'options' => ['placeholder' => 'Select a session ...'],
								'pluginOptions' => [
									'allowClear' => true,
								],
						])->label(false); ?>
					</td>
    				</tr>
    				<tr>
      				<td>EXAMINATION</td>
      				<td>:</td>
      				<td>
					  	<?= $form->field($modelVettings[1], '[1]examination')->widget(DatePicker::classname(), [
							'disabled' => true,
							'type' => DatePicker::TYPE_INPUT,
							'options' => ['placeholder' => 'Select a date ...'],
							'pluginOptions' => [
								'autoclose' => true,
								'startView' => 'year',
                        		'minViewMode' => 'months',
								'format' => 'MM yyyy'
							]
						])->label(false); ?>  
					</td>
    				</tr>
  				</tbody>
				</table>
				<p align="center"><b>______________________________________________________________________________________________</b></p>
				<table align="center" width="80%" border="0">
  				<tbody>
    				<tr>
      				<td width="15%">Course Code</td>
      				<td width="1%">:</td>
      				<td><?= $form->field($modelVettings[1], '[1]course_code')->textInput(['value' => $model->firstExaminer->course->code, 'disabled' => true])->label(false); ?></td>
    				</tr>
    				<tr>
      				<td>Course Name</td>
      				<td>:</td>
      				<td><?= $form->field($modelVettings[1], '[1]course_name')->textInput(['value' => $model->firstExaminer->course->name, 'disabled' => true])->label(false); ?></td>
    				</tr>
    				<tr>
      				<td>Question's Set</td>
      				<td>:</td>
      				<td><?= $form->field($modelVettings[1], '[1]questions_set')->textInput(['value' => 'Set B', 'disabled' => true])->label(false); ?></td>
    				</tr>
    				<tr>
      				<td>Lecturer</td>
      				<td>:</td>
      				<td><?= $form->field($modelVettings[1], '[1]lecturer')->textInput(['value' => $model->firstExaminer->user->username, 'disabled' => true])->label(false); ?></td>
    				</tr>
  				</tbody>
				</table>
				<br>
				<table align="center" width="80%" border="0">
					<tbody>
					<tr>
					<td><b><u>CHECK LIST</u></b></td>
					</tr>
					</tbody>
				</table>
				<table align="center" width="80%" border="1">
  				<tbody>
    				<tr>
					<th scope="col" width="5%"><p align="center">No.</p></th>
      				<th cope="col" width="30%"><p align="center">Format</p></th>
      				<th scope="col" width="10%"><p align="center">Yes</p></th>
      				<th scope="col" width="10%"><p align="center">No</p></th>
      				<th scope="col" width="25%"><p align="center">Remarks</p></th>
    				</tr>
    				<tr>
      				<td align="center">1.</td>
      				<td>&nbsp;Course Name</td>
      				<td align="center"><?= $form->field($modelVettings[1], '[1]course_name_yn')->radio(array('label' => false, 'value' => 'Yes', 'uncheck' => null, 'disabled' => true)); ?></td>
      				<td align="center"><?= $form->field($modelVettings[1], '[1]course_name_yn')->radio(array('label' => false, 'value' => 'No', 'uncheck' => null, 'disabled' => true)); ?></td>
      				<td><?= $form->field($modelVettings[1], '[1]course_name_remarks')->textInput(['disabled' => true])->label(false); ?></td>
    				</tr>
    				<tr>
      				<td align="center">2.</td>
      				<td>&nbsp;Course Code</td>
      				<td align="center"><?= $form->field($modelVettings[1], '[1]course_code_yn')->radio(array('label' => false, 'value' => 'Yes', 'uncheck' => null, 'disabled' => true)); ?></td>
      				<td align="center"><?= $form->field($modelVettings[1], '[1]course_code_yn')->radio(array('label' => false, 'value' => 'No', 'uncheck' => null, 'disabled' => true)); ?></td>
      				<td><?= $form->field($modelVettings[1], '[1]course_code_remarks')->textInput(['disabled' => true])->label(false); ?></td>
    				</tr>
    				<tr>
      				<td align="center">3.</td>
      				<td>&nbsp;Examination</td>
      				<td align="center"><?= $form->field($modelVettings[1], '[1]examination_yn')->radio(array('label' => false, 'value' => 'Yes', 'uncheck' => null, 'disabled' => true)); ?></td>
      				<td align="center"><?= $form->field($modelVettings[1], '[1]examination_yn')->radio(array('label' => false, 'value' => 'No', 'uncheck' => null, 'disabled' => true)); ?></td>
      				<td><?= $form->field($modelVettings[1], '[1]examination_remarks')->textInput(['disabled' => true])->label(false); ?></td>
    				</tr>
    				<tr>
      				<td align="center">4.</td>
      				<td>&nbsp;Examination Duration</td>
      				<td align="center"><?= $form->field($modelVettings[1], '[1]examination_duration_yn')->radio(array('label' => false, 'value' => 'Yes', 'uncheck' => null, 'disabled' => true)); ?></td>
      				<td align="center"><?= $form->field($modelVettings[1], '[1]examination_duration_yn')->radio(array('label' => false, 'value' => 'No', 'uncheck' => null, 'disabled' => true)); ?></td>
      				<td><?= $form->field($modelVettings[1], '[1]examination_duration_remarks')->textInput(['disabled' => true])->label(false); ?></td>
    				</tr>
    				<tr>
      				<td align="center">5.</td>
      				<td>&nbsp;Cover page instructions</td>
      				<td align="center"><?= $form->field($modelVettings[1], '[1]cover_page_instructions_yn')->radio(array('label' => false, 'value' => 'Yes', 'uncheck' => null, 'disabled' => true)); ?></td>
      				<td align="center"><?= $form->field($modelVettings[1], '[1]cover_page_instructions_yn')->radio(array('label' => false, 'value' => 'No', 'uncheck' => null, 'disabled' => true)); ?></td>
      				<td><?= $form->field($modelVettings[1], '[1]cover_page_instructions_remarks')->textInput(['disabled' => true])->label(false); ?></td>
    				</tr>
    				<tr>
      				<td align="center">6.</td>
      				<td>&nbsp;Question's instruction / Parts</td>
      				<td align="center"><?= $form->field($modelVettings[1], '[1]questions_instruction_parts_yn')->radio(array('label' => false, 'value' => 'Yes', 'uncheck' => null, 'disabled' => true)); ?></td>
      				<td align="center"><?= $form->field($modelVettings[1], '[1]questions_instruction_parts_yn')->radio(array('label' => false, 'value' => 'No', 'uncheck' => null, 'disabled' => true)); ?></td>
      				<td><?= $form->field($modelVettings[1], '[1]questions_instruction_parts_remarks')->textInput(['disabled' => true])->label(false); ?></td>
    				</tr>
    				<tr>
      				<td align="center">7.</td>
      				<td>&nbsp;Marks distribution for each marks</td>
      				<td align="center"><?= $form->field($modelVettings[1], '[1]mark_distribution_yn')->radio(array('label' => false, 'value' => 'Yes', 'uncheck' => null, 'disabled' => true)); ?></td>
      				<td align="center"><?= $form->field($modelVettings[1], '[1]mark_distribution_yn')->radio(array('label' => false, 'value' => 'No', 'uncheck' => null, 'disabled' => true)); ?></td>
      				<td><?= $form->field($modelVettings[1], '[1]mark_distribution_remarks')->textInput(['disabled' => true])->label(false); ?></td>
    				</tr>
    				<tr>
      				<td align="center">8.</td>
      				<td>&nbsp;Number of each page</td>
      				<td align="center"><?= $form->field($modelVettings[1], '[1]page_number_yn')->radio(array('label' => false, 'value' => 'Yes', 'uncheck' => null, 'disabled' => true)); ?></td>
      				<td align="center"><?= $form->field($modelVettings[1], '[1]page_number_yn')->radio(array('label' => false, 'value' => 'No', 'uncheck' => null, 'disabled' => true)); ?></td>
      				<td><?= $form->field($modelVettings[1], '[1]page_number_remarks')->textInput(['disabled' => true])->label(false); ?></td>
    				</tr>
    				<tr>
      				<td align="center">9.</td>
      				<td>&nbsp;Question's numbering system</td>
      				<td align="center"><?= $form->field($modelVettings[1], '[1]questions_numbering_yn')->radio(array('label' => false, 'value' => 'Yes', 'uncheck' => null, 'disabled' => true)); ?></td>
      				<td align="center"><?= $form->field($modelVettings[1], '[1]questions_numbering_yn')->radio(array('label' => false, 'value' => 'No', 'uncheck' => null, 'disabled' => true)); ?></td>
      				<td><?= $form->field($modelVettings[1], '[1]questions_numbering_remarks')->textInput(['disabled' => true])->label(false); ?></td>
    				</tr>
  				</tbody>
				</table>
				<br>
				<table align="center" width="80%" border="0">
					<tbody>
					<tr>
					<td><b><u>CONTENT</u></b></td>
					</tr>
					</tbody>
				</table>
				<table align="center" width="80%" border="1">
  				<tbody>
    				<tr>
					<th scope="col" width="5%"><p align="center">No.</p></th>
      				<th cope="col" width="30%"><p align="center">Contents</p></th>
      				<th scope="col" width="10%"><p align="center">Appropriate</p></th>
      				<th scope="col" width="10%"><p align="center">Not Appropriate</p></th>
      				<th scope="col" width="25%"><p align="center">Remarks</p></th>
    				</tr>
    				<tr>
      				<td align="center">1.</td>
      				<td>&nbsp;Illustrate the objectives of the course</td>
      				<td align="center"><?= $form->field($modelVettings[1], '[1]course_objectives_ana')->radio(array('label' => false, 'value' => 'Appropriate', 'uncheck' => null, 'disabled' => true)); ?></td>
      				<td align="center"><?= $form->field($modelVettings[1], '[1]course_objectives_ana')->radio(array('label' => false, 'value' => 'Not Appropriate', 'uncheck' => null, 'disabled' => true)); ?></td>
      				<td><?= $form->field($modelVettings[1], '[1]course_objectives_remarks')->textInput(['disabled' => true])->label(false); ?></td>
    				</tr>
    				<tr>
      				<td align="center">2.</td>
      				<td>&nbsp;Fulfill course synopsis/content</td>
      				<td align="center"><?= $form->field($modelVettings[1], '[1]course_synopsis_content_ana')->radio(array('label' => false, 'value' => 'Appropriate', 'uncheck' => null, 'disabled' => true)); ?></td>
      				<td align="center"><?= $form->field($modelVettings[1], '[1]course_synopsis_content_ana')->radio(array('label' => false, 'value' => 'Not Appropriate', 'uncheck' => null, 'disabled' => true)); ?></td>
      				<td><?= $form->field($modelVettings[1], '[1]course_synopsis_content_remarks')->textInput(['disabled' => true])->label(false); ?></td>
    				</tr>
    				<tr>
      				<td align="center">3.</td>
      				<td>&nbsp;Allocated time given</td>
      				<td align="center"><?= $form->field($modelVettings[1], '[1]time_given_ana')->radio(array('label' => false, 'value' => 'Appropriate', 'uncheck' => null, 'disabled' => true)); ?></td>
      				<td align="center"><?= $form->field($modelVettings[1], '[1]time_given_ana')->radio(array('label' => false, 'value' => 'Not Appropriate', 'uncheck' => null, 'disabled' => true)); ?></td>
      				<td><?= $form->field($modelVettings[1], '[1]time_given_remarks')->textInput(['disabled' => true])->label(false); ?></td>
    				</tr>
    				<tr>
      				<td align="center">4.</td>
      				<td>&nbsp;Language standard/spelling/term</td>
      				<td align="center"><?= $form->field($modelVettings[1], '[1]language_standard_ana')->radio(array('label' => false, 'value' => 'Appropriate', 'uncheck' => null, 'disabled' => true)); ?></td>
      				<td align="center"><?= $form->field($modelVettings[1], '[1]language_standard_ana')->radio(array('label' => false, 'value' => 'Not Appropriate', 'uncheck' => null, 'disabled' => true)); ?></td>
      				<td><?= $form->field($modelVettings[1], '[1]language_standard_remarks')->textInput(['disabled' => true])->label(false); ?></td>
    				</tr>
  				</tbody>
				</table>
				<br>
				<table align="center" width="80%" border="0">
  				<tbody>
    				<tr>
      				<td>Comment:</td>
    				</tr>
    				<tr>
      				<td><?= $form->field($modelVettings[1], '[1]comment')->textInput(['disabled' => true])->label(false); ?></td>
    				</tr>
  				</tbody>
				</table>
				<br>
				<table align="center" width="80%" border="0">
					<tbody>
					<tr>
					<td><b>Declaration:</b></td>
					</tr>
					</tbody>
				</table>
				<table align="center" width="80%" border="0">
  				<tbody>
    				<tr>
      				<td width="5%" align="center">1)</td>
      				<td width="10%" align="center"><?= $form->field($modelVettings[1], '[1]declaration')->radio(array('label' => false, 'value' => '1', 'uncheck' => null, 'required' => true)); ?></td>
      				<td width="65%">We have checked this question paper and declare that it is appropriate to be printed out.</td>
    				</tr>
    				<tr>
      				<td align="center">2)</td>
      				<td align="center"><?= $form->field($modelVettings[1], '[1]declaration')->radio(array('label' => false, 'value' => '2', 'uncheck' => null, 'required' => true)); ?></td>
      				<td>We have checked this question paper and declare that it is appropriate to be printed out in accordance with the corrections that have been suggested.
					</td>
    				</tr>
    				<tr>
      				<td align="center">3)</td>
      				<td align="center"><?= $form->field($modelVettings[1], '[1]declaration')->radio(array('label' => false, 'value' => '3', 'uncheck' => null, 'required' => true)); ?></td>
      				<td>We have checked this question paper and declare that it is not appropriate to be printed out.</td>
    				</tr>
  				</tbody>
				</table>
				<br>
				<table align="center" width="80%" border="0">
				<tbody>
					<tr>
					<th scope="col" colspan="3"><p align="center">Examination Question Vetting Committee</p></th>
					<th width="20%" scope="col"><p align="center">Signature</p></th>
					</tr>
					<tr>
					<td width="15%">&nbsp;Head</td>
					<td width="1%">:</td>
					<td width="44%"><?= $form->field($modelVettings[1], '[1]head')->textInput(['value' => $modelVettings[0]->head, 'disabled' => true])->label(false); ?></td>
					<td align="center"><?= $form->field($modelVettings[1], '[1]signature_1')->checkbox(array('disabled' => true, 'label' => false)); ?></td>
					</tr>
					<tr>
					<td>&nbsp;Members</td>
					<td>:</td>
					<td><?= $form->field($modelVettings[1], '[1]members')->textInput(['value' => $modelVettings[0]->members, 'disabled' => true])->label(false); ?></td>
					<td align="center"><?= $form->field($modelVettings[1], '[1]signature_2')->checkbox(array('label' => false, 'disabled' => true)); ?></td>
					</tr>
					<tr>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					</tr>
					<tr>
					<td>&nbsp;Date</td>
					<td>:</td>
					<td>
						<?= $form->field($modelVettings[1], '[1]date_1')->widget(DatePicker::classname(), [
							'disabled' => true,
                            'type' => DatePicker::TYPE_INPUT,
                            'options' => ['value' => str_replace("-", "/", date('d-m-Y', strtotime($modelVettings[0]->date_1)))],
							'pluginOptions' => [
								'autoclose' => true,
								'format' => 'dd/mm/yyyy'
							]
						])->label(false); ?>
					</td>
					<td>&nbsp;</td>
					</tr>
				</tbody>
				</table>
				<p align="center">______________________________________________________________________________________________</p>
				<table align="center" width="80%" border="0">
					<tbody>
					<tr>
					<td><b>Declaration to accept the amendment of the question paper :</b></td>
					</tr>
					</tbody>
				</table>
				<table align="center" width="80%" border="0">
				<tbody>
					<tr>
					  <td colspan="7">&nbsp;I have checked and declare that this question paper has been corrected and appropriate to be printed out.</td>
					</tr>
					<tr>
					  <td width="10%">&nbsp;Signature</td>
					  <td width="1%">:</td>
					  <td width="20%" align="center"><?= $form->field($modelVettings[1], '[1]signature_3')->checkbox(array('disabled' => true, 'label' => false)); ?></td>
					  <td width="18%">&nbsp;</td>
					  <td width="10%">&nbsp;Date</td>
					  <td width="1%">:</td>
					  <td width="20%">
					  	<?= $form->field($modelVettings[1], '[1]date_2')->widget(DatePicker::classname(), [
							'disabled' => true,
                            'type' => DatePicker::TYPE_INPUT,
                            'options' => ['value' => str_replace("-", "/", date('d-m-Y', strtotime($modelVettings[0]->date_1)))],
							'pluginOptions' => [
								'autoclose' => true,
								'format' => 'dd/mm/yyyy'
							]
						])->label(false); ?>
					</td>
					</tr>
					<tr>
					  <td>&nbsp;</td>
					  <td>&nbsp;</td>
					  <td align="center"><b>Head Examination Question Vetting</b></td>
					  <td>&nbsp;</td>
					  <td>&nbsp;</td>
					  <td>&nbsp;</td>
					  <td>&nbsp;</td>
					</tr>
				</tbody>
				</table>
				<br>
				<br>
			    </td>
    		</tr>
  		</tbody>
	</table>
   	
	<br>
  	
  	<table width="40%" align="center" border="0">
  	<tbody>
    <tr>
      <td colspan="3"><h4 align="center"><b>The chosen set is <u>
	  <?php
	 	if ($modelVettings[0]->chosen_set == '1') {

			echo $modelVettings[0]->questions_set;

		 } elseif ($modelVettings[1]->chosen_set == '1') {

			echo $modelVettings[1]->questions_set;
		 }
	  ?>
	  </u>.</b></h4></td>
    </tr>
  	</tbody>
	</table>
 	
 	<br>

    <div class="form-group" style="text-align: center">
        <?= Html::submitButton('Submit', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
