<?php

use yii\helpers\Url;
use yii\helpers\Html;
use yii\widgets\Pjax;

use kartik\grid\GridView;
/* @var $this yii\web\View */
/* @var $searchModel common\models\ExaminationPendingSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Examination Pending';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="examiner-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <?php Pjax::begin(['id' => 'idPjaxGridViewCourse', 'timeout' => false, 'enablePushState' => false]); ?>
    
    <?= GridView::widget([
        'id' => 'idPjaxGridViewCourse',
        'pjax' => true,
        'pjaxSettings' => [
            'options' => [
                'enablePushState' => false,
            ]
        ],
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'toggleDataOptions' => [
            'all' => Yii::$app->params['gridviewToggleDataOptions']['all'],
            'page' => Yii::$app->params['gridviewToggleDataOptions']['page'],
        ],
        'export' => [
            'header' => Yii::$app->params['gridviewExportCustom']['header'],
            'label' => Yii::$app->params['gridviewExportCustom']['label'],
            'options' => ['class' => Yii::$app->params['gridviewExportCustom']['optionClass']],
            'showConfirmAlert' => Yii::$app->params['gridviewExportCustom']['showConfirmAlert'],
            'target' => GridView::TARGET_SELF
        ],
        'exportConfig' => [
            GridView::EXCEL => [
                'label' => Yii::$app->params['gridviewExportCustom']['labelEXCEL'], 
                'filename' => $this->title,
            ],
        ], 
        'emptyCell' => '-',
        'emptyText' => 'No results found.',
        'formatter' =>  [
                'class' => 'yii\i18n\Formatter',
                'nullDisplay' => '-'
            ],
        'layout'=> Yii::$app->params['gridviewCustomExamination']['layout'],
        'summary' => 'Showing <b>{begin}-{end}</b> of <b>{totalCount}</b> Examiners.',
        'pager' => Yii::$app->params['pagecustomexamination'],

        'columns' => [
            [
                'header' => 'No.',
                'class' => 'kartik\grid\SerialColumn'
            ],

            [
                'label' => 'Course Code',
                'value' => function ($model) {

                    return $model['course_code'];
                },
            ],
            [
                'label' => 'Course Name',
                'value' => function ($model) {

                    return $model['course_name'];
                },
            ],
            [
                'label' => '1st Examiner',
                'value' => function ($model) {

                    return $model['1st_examiner'];
                },
            ],
            [
                'label' => '2nd Examiner',
                'value' => function ($model) {

                    return $model['2nd_examiner'];
                },
            ],
            [
                'label' => 'Progress Status',
                'value' => function ($model) {

                    return $model['submission_progress_status'];
                },
            ],

            [
                
                'header' => 'Actions',
                'class' => 'kartik\grid\ActionColumn',
                'template' => '{complete-submission}',
                'buttons' => [
                    'complete-submission' => function ($url, $model, $key) {

                        $url = Url::to(['complete-submission', 'id_submission' => $model['id_submission']]);

                        return Html::a('<span class="glyphicon glyphicon-open-file"></span>', $url, $options);
                    },
                ]
            ],
        ],
    ]); ?>

    <?php Pjax::end(); ?>

</div>
