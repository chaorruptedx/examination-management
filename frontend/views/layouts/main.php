<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use frontend\assets\AppAsset;
use common\widgets\Alert;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php $this->registerCsrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<div class="wrap">
    <?php
    NavBar::begin([
        'brandLabel' => Yii::$app->name,
        'brandUrl' => Yii::$app->homeUrl,
        'options' => [
            'class' => 'navbar-inverse navbar-fixed-top',
        ],
    ]);
    $menuItems = [
        ['label' => '<span class="glyphicon glyphicon-home"></span>&nbsp;&nbsp;Home', 'url' => ['/site/index']],
    ];

    if (Yii::$app->user->identity->category == 'lecturer') {
        $menuItems[] = ['label' => '<span class="glyphicon glyphicon-save-file"></span>&nbsp;&nbsp;Exam Format', 'url' => ['/examination-format/index']];
        $menuItems[] = [
            'label' => '<span class="glyphicon glyphicon-user"></span>&nbsp;&nbsp;1st Examiner',
            'options' => ['class'=>'dropdown'],
            'items' => [
                ['label' => '<span class="glyphicon glyphicon-duplicate"></span>&nbsp;&nbsp;First Submission', 'url' => ['/vetting-submission/index']],
                ['label' => '<span class="glyphicon glyphicon-open-file"></span>&nbsp;&nbsp;Correction Pending / Final Submission', 'url' => ['/correction-pending-final-submission/index']],
        ]];
        $menuItems[] = [
            'label' => '<span class="glyphicon glyphicon-user"></span>&nbsp;&nbsp;2nd Examiner',
            'options' => ['class'=>'dropdown'],
            'items' => [
                ['label' => '<span class="glyphicon glyphicon-facetime-video"></span>&nbsp;&nbsp;Vetting Pending / Correction Submission', 'url' => ['/vetting-pending-correction-submission/index']],
        ]];
        $menuItems[] = ['label' => '<span class="glyphicon glyphicon-time"></span>&nbsp;&nbsp;Status', 'url' => ['/progress-status/index']];
        $menuItems[] = ['label' => '<span class="glyphicon glyphicon-calendar"></span>&nbsp;&nbsp;Timetable', 'url' => ['/vetting-timetable/index']];
        $menuItems[] = [
            'label' => '<span class="glyphicon glyphicon-education"></span>&nbsp;&nbsp;Course',
            'options' => ['class'=>'dropdown'],
            'items' => [
                ['label' => '<span class="glyphicon glyphicon-list"></span>&nbsp;&nbsp;Course List', 'url' => ['/course/index']],
                ['label' => '<span class="glyphicon glyphicon-list"></span>&nbsp;&nbsp;Examiner List', 'url' => ['/examiner-list/index']],
        ]];
        // $menuItems[] = ['label' => '<span class="glyphicon glyphicon-folder-open"></span>&nbsp;&nbsp;Archive', 'url' => ['/examination-paper-archive/index']];
    }

    if (Yii::$app->user->identity->category == 'examination_unit') {
        $menuItems[] = ['label' => '<span class="glyphicon glyphicon-asterisk"></span>&nbsp;&nbsp;Examination Pending', 'url' => ['/examination-pending/index']];
        $menuItems[] = ['label' => '<span class="glyphicon glyphicon-save-file"></span>&nbsp;&nbsp;Examination Question', 'url' => ['/examination-question/index']];
        $menuItems[] = ['label' => '<span class="glyphicon glyphicon-folder-open"></span>&nbsp;&nbsp;Examination Archive', 'url' => ['/examination-archive/index']];
    }

    if (Yii::$app->user->isGuest) {
        $menuItems[] = ['label' => '<span class="glyphicon glyphicon-list-alt"></span>&nbsp;&nbsp;Signup', 'url' => ['/site/signup']];
        $menuItems[] = ['label' => '<span class="glyphicon glyphicon-log-in"></span>&nbsp;&nbsp;Login', 'url' => ['/site/login']];
    } else {
        $menuItems[] = '<li>'
            . Html::beginForm(['/site/logout'], 'post')
            . Html::submitButton(
                '<span class="glyphicon glyphicon-log-out"></span>&nbsp;&nbsp;Logout (' . Yii::$app->user->identity->username . ')',
                ['class' => 'btn btn-link logout']
            )
            . Html::endForm()
            . '</li>';
    }

    echo Nav::widget([
        'options' => ['class' => 'navbar-nav navbar-right'],
        'activateParents' => true,
        'encodeLabels' => false,
        'items' => $menuItems,
    ]);
    NavBar::end();
    ?>
    <br><br>
    <div class="container">
        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
        <?= Alert::widget() ?>
        <?= $content ?>
    </div>
</div>

<footer class="footer">
    <div class="container">
        <p class="pull-left">&copy; MiCoST <?= Html::encode(Yii::$app->name) ?> System <?= date('Y') ?></p>

        <p class="pull-right"><?= Yii::powered() ?></p>
    </div>
</footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
