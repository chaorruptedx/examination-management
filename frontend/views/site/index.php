<?php
use common\models\Announcement;
/* @var $this yii\web\View */

?>

<style>
.panel-light-yellow {
    border-color: #FFEB3B !important;
}
.panel-light-yellow>.panel-heading {
    color: white;
    background-color: #FFC107 !important;
    border-color: #FFEB3B !important;
}
.panel-announcement{
    padding: 10px;
    border-spacing:0px;
}
</style>

<div class="site-index">

<div class="panel-announcement">
    <div class="panel panel-light-yellow">
        <div class="panel-heading" style="padding-heading:0px;">
            <span class="glyphicon glyphicon-bullhorn"></span>
            <b>&nbsp;ANNOUNCEMENT</b>
        </div>
        <div class="panel-body">
            <div class="row">
                <div class="col-xs-12">
                    
                    <ul class="demo1">
                    
                        <?php
                            $announcement = Announcement::find()->all();

                            if(!empty($announcement)) { ?>
                            <?php foreach($announcement as $data) { ?>
                                <li class="news-item">
                                <table cellpadding="4">
                                    <tr>
                                        <td style="display:flex"><b>Announcement&nbsp;</b></td>
                                        <td style="width:5%"><b>:</b></td>
                                        <td><?= $data->announcement ?></td>
                                    </tr>
                                </table>
                                </li>
                            <?php } ?>
                        <?php } ?>
                    
                    </ul>

                </div>
            </div>
        </div>
    </div>
</div>


    <div class="body-content">

    </div>
</div>
