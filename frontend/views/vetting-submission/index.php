<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\Pjax;
use kartik\form\ActiveForm;

use kartik\select2\Select2;
use kartik\date\DatePicker;
use kartik\file\FileInput;

use common\models\Bankstatus;

$listProgramme = Bankstatus::listProgramme();
$listSession = Bankstatus::listSession();
$listVettingSubmissionCourse = Bankstatus::listVettingSubmissionCourse(Yii::$app->user->identity->id);

/* @var $this yii\web\View */
/* @var $searchModel common\models\VettingSubmissionSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'First Submission';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="submission-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?//= Html::a('Create Submission', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

	<br>

    <?php Pjax::begin(); ?>

	<?php 
    
        $form = ActiveForm::begin([
            'id' => 'submission_form',
            // 'type' => ActiveForm::TYPE_HORIZONTAL,
            // 'formConfig' => ['labelSpan' => 0, 'deviceSize' => ActiveForm::SIZE_SMALL],
            'validateOnSubmit' => true,
            'enableAjaxValidation' => false,
            'enableClientValidation'=>true,
        ]);
    
    ?>

	<table align="center" width="70%" border="0">
	<tr><td>
	<?= $form->field($modelAttachments[0], '[0]question_set_a')->widget(FileInput::classname(), [
			'pluginOptions' => [
				'showPreview' => false,
				'showUpload' => false, 
				'showRemove' => true,  
				'allowedFileExtensions' => ['pdf'],
			],
	])->label("Examination Question (Set A)"); ?>

	<?= $form->field($modelAttachments[1], '[1]answer_set_a')->widget(FileInput::classname(), [
		'pluginOptions' => [
			'showPreview' => false,
			'showUpload' => false, 
			'showRemove' => true,  
			'allowedFileExtensions' => ['pdf'],
		],
	])->label("Answer Scheme Format (Set A)"); ?>

	<?= $form->field($modelAttachments[2], '[2]question_set_b')->widget(FileInput::classname(), [
			'pluginOptions' => [
				'showPreview' => false,
				'showUpload' => false, 
				'showRemove' => true,  
				'allowedFileExtensions' => ['pdf'],
			],
	])->label("Examination Question (Set B)"); ?>

	<?= $form->field($modelAttachments[3], '[3]answer_set_b')->widget(FileInput::classname(), [
		'pluginOptions' => [
			'showPreview' => false,
			'showUpload' => false, 
			'showRemove' => true,  
			'allowedFileExtensions' => ['pdf'],
		],
	])->label("Answer Scheme Format (Set B)"); ?>
	</td>
	</tr>
	</table>

	<br>
    
    <table align="center" width="80%" border="1">
  		<tbody>
    		<tr>
      			<td bgcolor="white">
      			<br>
      			<br>
      			<p align="center"><img src="<?= Yii::$app->request->baseUrl.'/image/Melaka International College of Science and Technology - Logo (New).png'?>" width="143" height="200" alt=""/></p>
      			<br>
      			<h4 align="center"><b><u>BORANG SERAHAN KERTAS SOALAN DAN SKEMA JAWAPAN PEPERIKSAAN AKHIR</u></b></h4>
      			<br>
      			<table align="center" width="80%" border="1">
				<tbody>
					<tr>
					  <td>
					  	<br>
					  	<p><b><i>&nbsp;A. PENYERAHAN KEPADA JAWATANKUASA PENILAIAN DAN PEPERIKSAAN</i></b></p>
					  	<table align="center" width="95%" border="0">
						  <tbody>
							<tr>
							  <td align="center" width="5%">1.</td>
							  <td width="30%">Nama Pensyarah</td>
							  <td width="1%">:</td>
							  <td width="59%"><?= $form->field($model, 'lecturer_name')->textInput(['value' => Yii::$app->user->identity->username, 'readonly' => true])->label(false); ?></td>
							</tr>
							<tr>
							  <td align="center">2.</td>
							  <td>Nama Program / Kod Program</td>
							  <td>:</td>
							  <td>
							  	<?= $form->field($model, 'programme_code')->widget(Select2::classname(), [
										'data' => $listProgramme,
										'options' => ['placeholder' => 'Select a programme ...'],
										'pluginOptions' => [
											'allowClear' => true,
										],
								])->label(false); ?>
							  </td>
							</tr>
							<tr>
							  <td align="center">3.</td>
							  <td>Nama Subjek / Kod Subjek</td>
							  <td>:</td>
							  <td>
								<?= $form->field($model, 'course')->widget(Select2::classname(), [
									'id' => 'submission-course',
									'data' => $listVettingSubmissionCourse,
									'options' => ['placeholder' => 'Select a course ...'],
									'pluginOptions' => [
										'allowClear' => true,
									],
								])->label(false); ?>
							  </td>
							</tr>
							<tr>
							  <td align="center">4.</td>
							  <td>Bilangan Set</td>
							  <td>:</td>
							  <td><?= $form->field($model, 'number_of_set')->textInput(['value' => '2', 'readonly' => true])->label(false); ?></td>
							</tr>
						  </tbody>
						</table>
						<br>
						<table align="center" width="95%" border="0">
						  <tbody>
							<tr>
							  <td colspan="3" width="45%">Diserahkan oleh:</td>
							  <td width="10%">&nbsp;</td>
							  <td colspan="3" width="45%">Diterima oleh:</td>
							</tr>
							<tr>
							  <td colspan="3" align="center"><?= $form->field($model, 'a_signature_1')->checkbox(array('label' => false)); ?></td>
							  <td>&nbsp;</td>
							  <td colspan="3" align="center"><?= $form->field($model, 'a_signature_2')->checkbox(array('disabled' => true, 'label' => false)); ?></td>
							</tr>
							<tr>
							  <td colspan="3" align="center"><?= $form->field($model, 'a_name_1')->textInput(['value' => Yii::$app->user->identity->username, 'readonly' => true])->label(false); ?></td>
							  <td>&nbsp;</td>
							  <td colspan="3" align="center"><?= $form->field($model, 'a_name_2')->textInput(['disabled' => true])->label(false); ?></td>
							</tr>
							<tr>
							  <td width="10%">Jawatan</td>
							  <td width="1%">:</td>
							  <td><?= $form->field($model, 'a_position_1')->textInput(['value' => 'Lecturer (1st Examiner)', 'readonly' => true])->label(false); ?></td>
							  <td>&nbsp;</td>
							  <td width="10%">Jawatan</td>
							  <td width="1%">:</td>
							  <td><?= $form->field($model, 'a_position_2')->textInput(['disabled' => true])->label(false); ?></td>
							</tr>
							<tr>
							  <td>Tarikh</td>
							  <td width="1%">:</td>
							  <td>
								<?= $form->field($model, 'a_date_1')->widget(DatePicker::classname(), [
									'disabled' => true,
									'type' => DatePicker::TYPE_INPUT,
									'options' => ['value' => date("d/m/Y")],
									'pluginOptions' => [
										'autoclose' => true,
										'format' => 'dd/mm/yyyy'
									]
								])->label(false); ?>
							  </td>
							  <td>&nbsp;</td>
							  <td>Tarikh</td>
							  <td width="1%">:</td>
							  <td>
							  	<?= $form->field($model, 'a_date_2')->widget(DatePicker::classname(), [
								  	'disabled' => true,
									'type' => DatePicker::TYPE_INPUT,
									'pluginOptions' => [
										'autoclose' => true,
										'format' => 'dd/mm/yyyy'
									]
								])->label(false); ?>
							  </td>
							</tr>
						  </tbody>
						</table>
						<br>
					  </td>
					</tr>
					<tr>
					  <td>
					  	<br>
					  	<p><b><i>&nbsp;B. PENYERAHAN UNTUK PROSES PENYARINGAN</i></b></p>
					  	<br>
					  	<table align="center" width="95%" border="0">
						  <tbody>
							<tr>
							  <td colspan="3" width="45%">Diserahkan oleh:</td>
							  <td width="10%">&nbsp;</td>
							  <td colspan="3" width="45%">Diterima oleh:</td>
							</tr>
							<tr>
							  <td colspan="3" align="center"><?= $form->field($model, 'b_signature_1')->checkbox(array('disabled' => true, 'label' => false)); ?></td>
							  <td>&nbsp;</td>
							  <td colspan="3" align="center"><?= $form->field($model, 'b_signature_2')->checkbox(array('disabled' => true, 'label' => false)); ?></td>
							</tr>
							<tr>
							  <td colspan="3" align="center"><?= $form->field($model, 'b_name_1')->textInput(['disabled' => true])->label(false); ?></td>
							  <td>&nbsp;</td>
							  <td colspan="3" align="center"><?= $form->field($model, 'b_name_2')->textInput(['disabled' => true])->label(false); ?></td>
							</tr>
							<tr>
							  <td width="10%">Jawatan</td>
							  <td width="1%">:</td>
							  <td><?= $form->field($model, 'b_position_1')->textInput(['disabled' => true])->label(false); ?></td>
							  <td>&nbsp;</td>
							  <td width="10%">Jawatan</td>
							  <td width="1%">:</td>
							  <td><?= $form->field($model, 'b_position_2')->textInput(['disabled' => true])->label(false); ?></td>
							</tr>
							<tr>
							  <td>Tarikh</td>
							  <td width="1%">:</td>
							  <td>
							  	<?= $form->field($model, 'b_date_1')->widget(DatePicker::classname(), [
										'disabled' => true,
										'type' => DatePicker::TYPE_INPUT,
										'pluginOptions' => [
											'autoclose' => true,
											'format' => 'dd/mm/yyyy'
										]
									])->label(false);
								?>
							  </td>
							  <td>&nbsp;</td>
							  <td>Tarikh</td>
							  <td width="1%">:</td>
							  <td>
							  	<?= $form->field($model, 'b_date_2')->widget(DatePicker::classname(), [
								  	'disabled' => true,
									'type' => DatePicker::TYPE_INPUT,
									'pluginOptions' => [
										'autoclose' => true,
										'format' => 'dd/mm/yyyy'
									]
								])->label(false); ?>
							  </td>
							</tr>
						  </tbody>
						</table>
					  	<br>
					  </td>
					</tr>
					<tr>
					  <td>
					  	<br>
					  	<p><b><i>&nbsp;C. SERAHAN SEMULA KEPADA PENSYARAH UNTUK PROSES PEMBETULAN</i></b></p>
					  	<br>
					  	<table align="center" width="95%" border="0">
						  <tbody>
							<tr>
							  <td colspan="3" width="45%">Diserahkan oleh:</td>
							  <td width="10%">&nbsp;</td>
							  <td colspan="3" width="45%">Diterima oleh:</td>
							</tr>
							<tr>
							  <td colspan="3" align="center"><?= $form->field($model, 'c_signature_1')->checkbox(array('disabled' => true, 'label' => false)); ?></td>
							  <td>&nbsp;</td>
							  <td colspan="3" align="center"><?= $form->field($model, 'c_signature_2')->checkbox(array('disabled' => true, 'label' => false)); ?></td>
							</tr>
							<tr>
							  <td colspan="3" align="center"><?= $form->field($model, 'c_name_1')->textInput(['disabled' => true])->label(false); ?></td>
							  <td>&nbsp;</td>
							  <td colspan="3" align="center"><?= $form->field($model, 'c_name_2')->textInput(['disabled' => true])->label(false); ?></td>
							</tr>
							<tr>
							  <td width="10%">Jawatan</td>
							  <td width="1%">:</td>
							  <td><?= $form->field($model, 'c_position_1')->textInput(['disabled' => true])->label(false); ?></td>
							  <td>&nbsp;</td>
							  <td width="10%">Jawatan</td>
							  <td width="1%">:</td>
							  <td><?= $form->field($model, 'c_position_2')->textInput(['disabled' => true])->label(false); ?></td>
							</tr>
							<tr>
							  <td>Tarikh</td>
							  <td width="1%">:</td>
							  <td>
							  	<?= $form->field($model, 'c_date_1')->widget(DatePicker::classname(), [
								  	'disabled' => true,
									'type' => DatePicker::TYPE_INPUT,
									'pluginOptions' => [
										'autoclose' => true,
										'format' => 'dd/mm/yyyy'
									]
								])->label(false); ?>
							  </td>
							  <td>&nbsp;</td>
							  <td>Tarikh</td>
							  <td width="1%">:</td>
							  <td>
							  	<?= $form->field($model, 'c_date_2')->widget(DatePicker::classname(), [
								  	'disabled' => true,
									'type' => DatePicker::TYPE_INPUT,
									'pluginOptions' => [
										'autoclose' => true,
										'format' => 'dd/mm/yyyy'
									]
								])->label(false); ?>
							  </td>
							</tr>
						  </tbody>
						</table>
					  	<br>
					  </td>
					</tr>
					<tr>
					  <td>
					  	<br>
					  	<p><b><i>&nbsp;D. SERAHAN KEPADA JAWATANKUASA PENILAIAN DAN PEPERIKSAAN</i></b></p>
					  	<br>
					  	<table align="center" width="95%" border="0">
						  <tbody>
							<tr>
							  <td colspan="3" width="45%">Diserahkan oleh:</td>
							  <td width="10%">&nbsp;</td>
							  <td colspan="3" width="45%">Diterima oleh:</td>
							</tr>
							<tr>
							  <td colspan="3" align="center"><?= $form->field($model, 'd_signature_1')->checkbox(array('disabled' => true, 'label' => false)); ?></td>
							  <td>&nbsp;</td>
							  <td colspan="3" align="center"><?= $form->field($model, 'd_signature_2')->checkbox(array('disabled' => true, 'label' => false)); ?></td>
							</tr>
							<tr>
							  <td colspan="3" align="center"><?= $form->field($model, 'd_name_1')->textInput(['disabled' => true])->label(false); ?></td>
							  <td>&nbsp;</td>
							  <td colspan="3" align="center"><?= $form->field($model, 'd_name_2')->textInput(['disabled' => true])->label(false); ?></td>
							</tr>
							<tr>
							  <td width="10%">Jawatan</td>
							  <td width="1%">:</td>
							  <td><?= $form->field($model, 'd_position_1')->textInput(['disabled' => true])->label(false); ?></td>
							  <td>&nbsp;</td>
							  <td width="10%">Jawatan</td>
							  <td width="1%">:</td>
							  <td><?= $form->field($model, 'd_position_2')->textInput(['disabled' => true])->label(false); ?></td>
							</tr>
							<tr>
							  <td>Tarikh</td>
							  <td width="1%">:</td>
							  <td>
								<?= $form->field($model, 'd_date_1')->widget(DatePicker::classname(), [
								  	'disabled' => true,
									'type' => DatePicker::TYPE_INPUT,
									'pluginOptions' => [
										'autoclose' => true,
										'format' => 'dd/mm/yyyy'
									]
								])->label(false); ?>
							  </td>
							  <td>&nbsp;</td>
							  <td>Tarikh</td>
							  <td width="1%">:</td>
							  <td>
								<?= $form->field($model, 'd_date_2')->widget(DatePicker::classname(), [
								  	'disabled' => true,
									'type' => DatePicker::TYPE_INPUT,
									'pluginOptions' => [
										'autoclose' => true,
										'format' => 'dd/mm/yyyy'
									]
								])->label(false); ?>
							  </td>
							</tr>

						  </tbody>
						</table>
					  	<br>
					  </td>
					</tr>
					<tr>
					  <td>
					  	<br>
					  	<p><b><i>&nbsp;E. SERAHAN KEPADA UNIT PEPERIKSAAN</i></b></p>
					  	<br>
					  	<table align="center" width="95%" border="0">
						  <tbody>
							<tr>
							  <td colspan="3" width="45%">Diserahkan oleh:</td>
							  <td width="10%">&nbsp;</td>
							  <td colspan="3" width="45%">Diterima oleh:</td>
							</tr>
							<tr>
							  <td colspan="3" align="center"><?= $form->field($model, 'e_signature_1')->checkbox(array('disabled' => true, 'label' => false)); ?></td>
							  <td>&nbsp;</td>
							  <td colspan="3" align="center"><?= $form->field($model, 'e_signature_2')->checkbox(array('disabled' => true, 'label' => false)); ?></td>
							</tr>
							<tr>
							  <td colspan="3" align="center"><?= $form->field($model, 'e_name_1')->textInput(['disabled' => true])->label(false); ?></td>
							  <td>&nbsp;</td>
							  <td colspan="3" align="center"><?= $form->field($model, 'e_name_2')->textInput(['disabled' => true])->label(false); ?></td>
							</tr>
							<tr>
							  <td width="10%">Jawatan</td>
							  <td width="1%">:</td>
							  <td><?= $form->field($model, 'e_position_1')->textInput(['disabled' => true])->label(false); ?></td>
							  <td>&nbsp;</td>
							  <td width="10%">Jawatan</td>
							  <td width="1%">:</td>
							  <td><?= $form->field($model, 'e_position_2')->textInput(['disabled' => true])->label(false); ?></td>
							</tr>
							<tr>
							  <td>Tarikh</td>
							  <td width="1%">:</td>
							  <td>
								<?= $form->field($model, 'e_date_1')->widget(DatePicker::classname(), [
								  	'disabled' => true,
									'type' => DatePicker::TYPE_INPUT,
									'pluginOptions' => [
										'autoclose' => true,
										'format' => 'dd/mm/yyyy'
									]
								])->label(false); ?>
							  </td>
							  <td>&nbsp;</td>
							  <td>Tarikh</td>
							  <td width="1%">:</td>
							  <td>
							  	<?= $form->field($model, 'e_date_2')->widget(DatePicker::classname(), [
								  	'disabled' => true,
									'type' => DatePicker::TYPE_INPUT,
									'pluginOptions' => [
										'autoclose' => true,
										'format' => 'dd/mm/yyyy'
									]
								])->label(false); ?>
							  </td>
							</tr>
						  </tbody>
						</table>
					  	<br>
					  </td>
					</tr>
				</tbody>
				</table>
				<table align="center" width="80%" border="0">
				  <tbody>
					<tr>
					  <td><p><i>* Penting :Borang yang telah LENGKAP melalui semua proses yang sewajarnya akan disimpan oleh Unit Peperiksaan untuk melengkapkan semua maklumat yang diperlukan oleh Bahagian Percetakan.</i></p></td>
					</tr>
				  </tbody>
				</table>
      			<br>
      			<br>
      			</td>
    		</tr>
  		</tbody>
	</table>

	<br>
  	   	   	   	
   	<table align="center" width="80%" border="1">
  		<tbody>
    		<tr>
      			<td bgcolor="white">
			    <br>
			    <br>
			    <p align="center"><img src="<?= Yii::$app->request->baseUrl.'/image/Melaka International College of Science and Technology - Logo (New).png'?>" width="143" height="200" alt=""/></p>
			    <br>
				<h4 align="center"><b>EXAMINATION VETTING FORM</b></h4>
	    		<br>
		    	<table align="center" width="80%" border="0">
  				<tbody>
    				<tr>
      				<td width="15%">SEMESTER</td>
      				<td width="1%">:</td>
      				<td><?= $form->field($modelVettings[0], '[0]session_session')->textInput(['value' => date("F Y", strtotime($session['max_session'])), 'readOnly' => true])->label(false); ?></td>
    				</tr>
    				<tr>
      				<td>SESSION</td>
      				<td>:</td>
      				<td>
					  	<?= $form->field($modelVettings[0], '[0]session')->widget(Select2::classname(), [
								'data' => $listSession,
								'options' => ['id' => 'vetting-session', 'placeholder' => 'Select a session ...'],
								'pluginOptions' => [
									'allowClear' => true,
								],
						])->label(false); ?>
					</td>
    				</tr>
    				<tr>
      				<td>EXAMINATION</td>
      				<td>:</td>
      				<td>
					  	<?= $form->field($modelVettings[0], '[0]examination')->widget(DatePicker::classname(), [
							'type' => DatePicker::TYPE_INPUT,
							'options' => ['id' => 'vetting-examination', 'placeholder' => 'Select a date ...'],
							'pluginOptions' => [
								'autoclose' => true,
								'startView' => 'year',
                        		'minViewMode' => 'months',
								'format' => 'MM yyyy'
							]
						])->label(false); ?>  
					</td>
    				</tr>
  				</tbody>
				</table>
				<p align="center"><b>______________________________________________________________________________________________</b></p>
				<table align="center" width="80%" border="0">
  				<tbody>
    				<tr>
      				<td width="15%">Course Code</td>
      				<td width="1%">:</td>
      				<td><?= $form->field($modelVettings[0], '[0]course_code')->textInput(['id' => 'vetting-course-code', 'readOnly' => true])->label(false); ?></td>
    				</tr>
    				<tr>
      				<td>Course Name</td>
      				<td>:</td>
      				<td><?= $form->field($modelVettings[0], '[0]course_name')->textInput(['id' => 'vetting-course-name', 'readOnly' => true])->label(false); ?></td>
    				</tr>
    				<tr>
      				<td>Question's Set</td>
      				<td>:</td>
      				<td><?= $form->field($modelVettings[0], '[0]questions_set')->textInput(['value' => 'Set A', 'readOnly' => true])->label(false); ?></td>
    				</tr>
    				<tr>
      				<td>Lecturer</td>
      				<td>:</td>
      				<td><?= $form->field($modelVettings[0], '[0]lecturer')->textInput(['value' => Yii::$app->user->identity->username, 'readOnly' => true])->label(false); ?></td>
    				</tr>
  				</tbody>
				</table>
				<br>
				<table align="center" width="80%" border="0">
					<tbody>
					<tr>
					<td><b><u>CHECK LIST</u></b></td>
					</tr>
					</tbody>
				</table>
				<table align="center" width="80%" border="1">
  				<tbody>
    				<tr>
					<th scope="col" width="5%"><p align="center">No.</p></th>
      				<th cope="col" width="30%"><p align="center">Format</p></th>
      				<th scope="col" width="10%"><p align="center">Yes</p></th>
      				<th scope="col" width="10%"><p align="center">No</p></th>
      				<th scope="col" width="25%"><p align="center">Remarks</p></th>
    				</tr>
    				<tr>
      				<td align="center">1.</td>
      				<td>&nbsp;Course Name</td>
      				<td align="center"><?= $form->field($modelVettings[0], '[0]course_name_yn')->radio(array('label' => false, 'value' => 'Yes', 'uncheck' => null, 'disabled' => true)); ?></td>
      				<td align="center"><?= $form->field($modelVettings[0], '[0]course_name_yn')->radio(array('label' => false, 'value' => 'No', 'uncheck' => null, 'disabled' => true)); ?></td>
      				<td><?= $form->field($modelVettings[0], '[0]course_name_remarks')->textInput(['disabled' => true])->label(false); ?></td>
    				</tr>
    				<tr>
      				<td align="center">2.</td>
      				<td>&nbsp;Course Code</td>
      				<td align="center"><?= $form->field($modelVettings[0], '[0]course_code_yn')->radio(array('label' => false, 'value' => 'Yes', 'uncheck' => null, 'disabled' => true)); ?></td>
      				<td align="center"><?= $form->field($modelVettings[0], '[0]course_code_yn')->radio(array('label' => false, 'value' => 'No', 'uncheck' => null, 'disabled' => true)); ?></td>
      				<td><?= $form->field($modelVettings[0], '[0]course_code_remarks')->textInput(['disabled' => true])->label(false); ?></td>
    				</tr>
    				<tr>
      				<td align="center">3.</td>
      				<td>&nbsp;Examination</td>
      				<td align="center"><?= $form->field($modelVettings[0], '[0]examination_yn')->radio(array('label' => false, 'value' => 'Yes', 'uncheck' => null, 'disabled' => true)); ?></td>
      				<td align="center"><?= $form->field($modelVettings[0], '[0]examination_yn')->radio(array('label' => false, 'value' => 'No', 'uncheck' => null, 'disabled' => true)); ?></td>
      				<td><?= $form->field($modelVettings[0], '[0]examination_remarks')->textInput(['disabled' => true])->label(false); ?></td>
    				</tr>
    				<tr>
      				<td align="center">4.</td>
      				<td>&nbsp;Examination Duration</td>
      				<td align="center"><?= $form->field($modelVettings[0], '[0]examination_duration_yn')->radio(array('label' => false, 'value' => 'Yes', 'uncheck' => null, 'disabled' => true)); ?></td>
      				<td align="center"><?= $form->field($modelVettings[0], '[0]examination_duration_yn')->radio(array('label' => false, 'value' => 'No', 'uncheck' => null, 'disabled' => true)); ?></td>
      				<td><?= $form->field($modelVettings[0], '[0]examination_duration_remarks')->textInput(['disabled' => true])->label(false); ?></td>
    				</tr>
    				<tr>
      				<td align="center">5.</td>
      				<td>&nbsp;Cover page instructions</td>
      				<td align="center"><?= $form->field($modelVettings[0], '[0]cover_page_instructions_yn')->radio(array('label' => false, 'value' => 'Yes', 'uncheck' => null, 'disabled' => true)); ?></td>
      				<td align="center"><?= $form->field($modelVettings[0], '[0]cover_page_instructions_yn')->radio(array('label' => false, 'value' => 'No', 'uncheck' => null, 'disabled' => true)); ?></td>
      				<td><?= $form->field($modelVettings[0], '[0]cover_page_instructions_remarks')->textInput(['disabled' => true])->label(false); ?></td>
    				</tr>
    				<tr>
      				<td align="center">6.</td>
      				<td>&nbsp;Question's instruction / Parts</td>
      				<td align="center"><?= $form->field($modelVettings[0], '[0]questions_instruction_parts_yn')->radio(array('label' => false, 'value' => 'Yes', 'uncheck' => null, 'disabled' => true)); ?></td>
      				<td align="center"><?= $form->field($modelVettings[0], '[0]questions_instruction_parts_yn')->radio(array('label' => false, 'value' => 'No', 'uncheck' => null, 'disabled' => true)); ?></td>
      				<td><?= $form->field($modelVettings[0], '[0]questions_instruction_parts_remarks')->textInput(['disabled' => true])->label(false); ?></td>
    				</tr>
    				<tr>
      				<td align="center">7.</td>
      				<td>&nbsp;Marks distribution for each marks</td>
      				<td align="center"><?= $form->field($modelVettings[0], '[0]mark_distribution_yn')->radio(array('label' => false, 'value' => 'Yes', 'uncheck' => null, 'disabled' => true)); ?></td>
      				<td align="center"><?= $form->field($modelVettings[0], '[0]mark_distribution_yn')->radio(array('label' => false, 'value' => 'No', 'uncheck' => null, 'disabled' => true)); ?></td>
      				<td><?= $form->field($modelVettings[0], '[0]mark_distribution_remarks')->textInput(['disabled' => true])->label(false); ?></td>
    				</tr>
    				<tr>
      				<td align="center">8.</td>
      				<td>&nbsp;Number of each page</td>
      				<td align="center"><?= $form->field($modelVettings[0], '[0]page_number_yn')->radio(array('label' => false, 'value' => 'Yes', 'uncheck' => null, 'disabled' => true)); ?></td>
      				<td align="center"><?= $form->field($modelVettings[0], '[0]page_number_yn')->radio(array('label' => false, 'value' => 'No', 'uncheck' => null, 'disabled' => true)); ?></td>
      				<td><?= $form->field($modelVettings[0], '[0]page_number_remarks')->textInput(['disabled' => true])->label(false); ?></td>
    				</tr>
    				<tr>
      				<td align="center">9.</td>
      				<td>&nbsp;Question's numbering system</td>
      				<td align="center"><?= $form->field($modelVettings[0], '[0]questions_numbering_yn')->radio(array('label' => false, 'value' => 'Yes', 'uncheck' => null, 'disabled' => true)); ?></td>
      				<td align="center"><?= $form->field($modelVettings[0], '[0]questions_numbering_yn')->radio(array('label' => false, 'value' => 'No', 'uncheck' => null, 'disabled' => true)); ?></td>
      				<td><?= $form->field($modelVettings[0], '[0]questions_numbering_remarks')->textInput(['disabled' => true])->label(false); ?></td>
    				</tr>
  				</tbody>
				</table>
				<br>
				<table align="center" width="80%" border="0">
					<tbody>
					<tr>
					<td><b><u>CONTENT</u></b></td>
					</tr>
					</tbody>
				</table>
				<table align="center" width="80%" border="1">
  				<tbody>
    				<tr>
					<th scope="col" width="5%"><p align="center">No.</p></th>
      				<th cope="col" width="30%"><p align="center">Contents</p></th>
      				<th scope="col" width="10%"><p align="center">Appropriate</p></th>
      				<th scope="col" width="10%"><p align="center">Not Appropriate</p></th>
      				<th scope="col" width="25%"><p align="center">Remarks</p></th>
    				</tr>
    				<tr>
      				<td align="center">1.</td>
      				<td>&nbsp;Illustrate the objectives of the course</td>
      				<td align="center"><?= $form->field($modelVettings[0], '[0]course_objectives_ana')->radio(array('label' => false, 'value' => 'Appropriate', 'uncheck' => null, 'disabled' => true)); ?></td>
      				<td align="center"><?= $form->field($modelVettings[0], '[0]course_objectives_ana')->radio(array('label' => false, 'value' => 'Not Appropriate', 'uncheck' => null, 'disabled' => true)); ?></td>
      				<td><?= $form->field($modelVettings[0], '[0]course_objectives_remarks')->textInput(['disabled' => true])->label(false); ?></td>
    				</tr>
    				<tr>
      				<td align="center">2.</td>
      				<td>&nbsp;Fulfill course synopsis/content</td>
      				<td align="center"><?= $form->field($modelVettings[0], '[0]course_synopsis_content_ana')->radio(array('label' => false, 'value' => 'Appropriate', 'uncheck' => null, 'disabled' => true)); ?></td>
      				<td align="center"><?= $form->field($modelVettings[0], '[0]course_synopsis_content_ana')->radio(array('label' => false, 'value' => 'Not Appropriate', 'uncheck' => null, 'disabled' => true)); ?></td>
      				<td><?= $form->field($modelVettings[0], '[0]course_synopsis_content_remarks')->textInput(['disabled' => true])->label(false); ?></td>
    				</tr>
    				<tr>
      				<td align="center">3.</td>
      				<td>&nbsp;Allocated time given</td>
      				<td align="center"><?= $form->field($modelVettings[0], '[0]time_given_ana')->radio(array('label' => false, 'value' => 'Appropriate', 'uncheck' => null, 'disabled' => true)); ?></td>
      				<td align="center"><?= $form->field($modelVettings[0], '[0]time_given_ana')->radio(array('label' => false, 'value' => 'Not Appropriate', 'uncheck' => null, 'disabled' => true)); ?></td>
      				<td><?= $form->field($modelVettings[0], '[0]time_given_remarks')->textInput(['disabled' => true])->label(false); ?></td>
    				</tr>
    				<tr>
      				<td align="center">4.</td>
      				<td>&nbsp;Language standard/spelling/term</td>
      				<td align="center"><?= $form->field($modelVettings[0], '[0]language_standard_ana')->radio(array('label' => false, 'value' => 'Appropriate', 'uncheck' => null, 'disabled' => true)); ?></td>
      				<td align="center"><?= $form->field($modelVettings[0], '[0]language_standard_ana')->radio(array('label' => false, 'value' => 'Not Appropriate', 'uncheck' => null, 'disabled' => true)); ?></td>
      				<td><?= $form->field($modelVettings[0], '[0]language_standard_remarks')->textInput(['disabled' => true])->label(false); ?></td>
    				</tr>
  				</tbody>
				</table>
				<br>
				<table align="center" width="80%" border="0">
  				<tbody>
    				<tr>
      				<td>Comment:</td>
    				</tr>
    				<tr>
      				<td><?= $form->field($modelVettings[0], '[0]comment')->textInput(['disabled' => true])->label(false); ?></td>
    				</tr>
  				</tbody>
				</table>
				<br>
				<table align="center" width="80%" border="0">
					<tbody>
					<tr>
					<td><b>Declaration:</b></td>
					</tr>
					</tbody>
				</table>
				<table align="center" width="80%" border="0">
  				<tbody>
    				<tr>
      				<td width="5%" align="center">1)</td>
      				<td width="10%" align="center"><?= $form->field($modelVettings[0], '[0]declaration')->radio(array('label' => false, 'value' => '1', 'uncheck' => null, 'disabled' => true)); ?></td>
      				<td width="65%">We have checked this question paper and declare that it is appropriate to be printed out.</td>
    				</tr>
    				<tr>
      				<td align="center">2)</td>
      				<td align="center"><?= $form->field($modelVettings[0], '[0]declaration')->radio(array('label' => false, 'value' => '2', 'uncheck' => null, 'disabled' => true)); ?></td>
      				<td>We have checked this question paper and declare that it is appropriate to be printed out in accordance with the corrections that have been suggested.
					</td>
    				</tr>
    				<tr>
      				<td align="center">3)</td>
      				<td align="center"><?= $form->field($modelVettings[0], '[0]declaration')->radio(array('label' => false, 'value' => '3', 'uncheck' => null, 'disabled' => true)); ?></td>
      				<td>We have checked this question paper and declare that it is not appropriate to be printed out.</td>
    				</tr>
  				</tbody>
				</table>
				<br>
				<table align="center" width="80%" border="0">
				<tbody>
					<tr>
					<th scope="col" colspan="3"><p align="center">Examination Question Vetting Committee</p></th>
					<th width="20%" scope="col"><p align="center">Signature</p></th>
					</tr>
					<tr>
					<td width="15%">&nbsp;Head</td>
					<td width="1%">:</td>
					<td width="44%"><?= $form->field($modelVettings[0], '[0]head')->textInput(['disabled' => true])->label(false); ?></td>
					<td align="center"><?= $form->field($modelVettings[0], '[0]signature_1')->checkbox(array('label' => false, 'disabled' => true)); ?></td>
					</tr>
					<tr>
					<td>&nbsp;Members</td>
					<td>:</td>
					<td><?= $form->field($modelVettings[0], '[0]members')->textInput(['disabled' => true])->label(false); ?></td>
					<td align="center"><?= $form->field($modelVettings[0], '[0]signature_2')->checkbox(array('label' => false, 'disabled' => true)); ?></td>
					</tr>
					<tr>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					</tr>
					<tr>
					<td>&nbsp;Date</td>
					<td>:</td>
					<td>
						<?= $form->field($modelVettings[0], '[0]date_1')->widget(DatePicker::classname(), [
							'disabled' => true,
							'type' => DatePicker::TYPE_INPUT,
							'pluginOptions' => [
								'autoclose' => true,
								'format' => 'dd/mm/yyyy'
							]
						])->label(false); ?>
					</td>
					<td>&nbsp;</td>
					</tr>
				</tbody>
				</table>
				<p align="center">______________________________________________________________________________________________</p>
				<table align="center" width="80%" border="0">
					<tbody>
					<tr>
					<td><b>Declaration to accept the amendment of the question paper :</b></td>
					</tr>
					</tbody>
				</table>
				<table align="center" width="80%" border="0">
				<tbody>
					<tr>
					  <td colspan="7">&nbsp;I have checked and declare that this question paper has been corrected and appropriate to be printed out.</td>
					</tr>
					<tr>
					  <td width="10%">&nbsp;Signature</td>
					  <td width="1%">:</td>
					  <td width="20%" align="center"><?= $form->field($modelVettings[0], '[0]signature_3')->checkbox(array('label' => false, 'disabled' => true)); ?></td>
					  <td width="18%">&nbsp;</td>
					  <td width="10%">&nbsp;Date</td>
					  <td width="1%">:</td>
					  <td width="20%">
					  	<?= $form->field($modelVettings[0], '[0]date_2')->widget(DatePicker::classname(), [
							'disabled' => true,
							'type' => DatePicker::TYPE_INPUT,
							'pluginOptions' => [
								'autoclose' => true,
								'format' => 'dd/mm/yyyy'
							]
						])->label(false); ?>
					</td>
					</tr>
					<tr>
					  <td>&nbsp;</td>
					  <td>&nbsp;</td>
					  <td align="center"><b>Head Examination Question Vetting</b></td>
					  <td>&nbsp;</td>
					  <td>&nbsp;</td>
					  <td>&nbsp;</td>
					  <td>&nbsp;</td>
					</tr>
				</tbody>
				</table>
				<br>
				<br>
			    </td>
    		</tr>
  		</tbody>
	</table>
   	
   	<br>

	   <table align="center" width="80%" border="1">
  		<tbody>
    		<tr>
      			<td bgcolor="white">
			    <br>
			    <br>
			    <p align="center"><img src="<?= Yii::$app->request->baseUrl.'/image/Melaka International College of Science and Technology - Logo (New).png'?>" width="143" height="200" alt=""/></p>
			    <br>
				<h4 align="center"><b>EXAMINATION VETTING FORM</b></h4>
	    		<br>
		    	<table align="center" width="80%" border="0">
  				<tbody>
    				<tr>
      				<td width="15%">SEMESTER</td>
      				<td width="1%">:</td>
      				<td><?= $form->field($modelVettings[1], '[1]session_session')->textInput(['value' => date("F Y", strtotime($session['max_session'])), 'readOnly' => true])->label(false); ?></td>
    				</tr>
    				<tr>
      				<td>SESSION</td>
      				<td>:</td>
      				<td>
					  	<?= $form->field($modelVettings[1], '[1]session')->widget(Select2::classname(), [
							  	'disabled' => true,
								'data' => $listSession,
								'options' => ['id' => 'vetting2-session', 'placeholder' => 'Select a session ...'],
								'pluginOptions' => [
									'allowClear' => true,
								],
						])->label(false); ?>
					</td>
    				</tr>
    				<tr>
      				<td>EXAMINATION</td>
      				<td>:</td>
      				<td>
					  	<?= $form->field($modelVettings[1], '[1]examination')->widget(DatePicker::classname(), [
							'disabled' => true,
							'type' => DatePicker::TYPE_INPUT,
							'options' => ['id' => 'vetting2-examination', 'placeholder' => 'Select a date ...'],
							'pluginOptions' => [
								'autoclose' => true,
								'startView' => 'year',
                        		'minViewMode' => 'months',
								'format' => 'MM yyyy'
							]
						])->label(false); ?>  
					</td>
    				</tr>
  				</tbody>
				</table>
				<p align="center"><b>______________________________________________________________________________________________</b></p>
				<table align="center" width="80%" border="0">
  				<tbody>
    				<tr>
      				<td width="15%">Course Code</td>
      				<td width="1%">:</td>
      				<td><?= $form->field($modelVettings[1], '[1]course_code')->textInput(['id' => 'vetting2-course-code', 'readOnly' => true])->label(false); ?></td>
    				</tr>
    				<tr>
      				<td>Course Name</td>
      				<td>:</td>
      				<td><?= $form->field($modelVettings[1], '[1]course_name')->textInput(['id' => 'vetting2-course-name', 'readOnly' => true])->label(false); ?></td>
    				</tr>
    				<tr>
      				<td>Question's Set</td>
      				<td>:</td>
      				<td><?= $form->field($modelVettings[1], '[1]questions_set')->textInput(['value' => 'Set B', 'readOnly' => true])->label(false); ?></td>
    				</tr>
    				<tr>
      				<td>Lecturer</td>
      				<td>:</td>
      				<td><?= $form->field($modelVettings[1], '[1]lecturer')->textInput(['value' => Yii::$app->user->identity->username, 'readOnly' => true])->label(false); ?></td>
    				</tr>
  				</tbody>
				</table>
				<br>
				<table align="center" width="80%" border="0">
					<tbody>
					<tr>
					<td><b><u>CHECK LIST</u></b></td>
					</tr>
					</tbody>
				</table>
				<table align="center" width="80%" border="1">
  				<tbody>
    				<tr>
					<th scope="col" width="5%"><p align="center">No.</p></th>
      				<th cope="col" width="30%"><p align="center">Format</p></th>
      				<th scope="col" width="10%"><p align="center">Yes</p></th>
      				<th scope="col" width="10%"><p align="center">No</p></th>
      				<th scope="col" width="25%"><p align="center">Remarks</p></th>
    				</tr>
    				<tr>
      				<td align="center">1.</td>
      				<td>&nbsp;Course Name</td>
      				<td align="center"><?= $form->field($modelVettings[1], '[1]course_name_yn')->radio(array('label' => false, 'value' => 'Yes', 'uncheck' => null, 'disabled' => true)); ?></td>
      				<td align="center"><?= $form->field($modelVettings[1], '[1]course_name_yn')->radio(array('label' => false, 'value' => 'No', 'uncheck' => null, 'disabled' => true)); ?></td>
      				<td><?= $form->field($modelVettings[1], '[1]course_name_remarks')->textInput(['disabled' => true])->label(false); ?></td>
    				</tr>
    				<tr>
      				<td align="center">2.</td>
      				<td>&nbsp;Course Code</td>
      				<td align="center"><?= $form->field($modelVettings[1], '[1]course_code_yn')->radio(array('label' => false, 'value' => 'Yes', 'uncheck' => null, 'disabled' => true)); ?></td>
      				<td align="center"><?= $form->field($modelVettings[1], '[1]course_code_yn')->radio(array('label' => false, 'value' => 'No', 'uncheck' => null, 'disabled' => true)); ?></td>
      				<td><?= $form->field($modelVettings[1], '[1]course_code_remarks')->textInput(['disabled' => true])->label(false); ?></td>
    				</tr>
    				<tr>
      				<td align="center">3.</td>
      				<td>&nbsp;Examination</td>
      				<td align="center"><?= $form->field($modelVettings[1], '[1]examination_yn')->radio(array('label' => false, 'value' => 'Yes', 'uncheck' => null, 'disabled' => true)); ?></td>
      				<td align="center"><?= $form->field($modelVettings[1], '[1]examination_yn')->radio(array('label' => false, 'value' => 'No', 'uncheck' => null, 'disabled' => true)); ?></td>
      				<td><?= $form->field($modelVettings[1], '[1]examination_remarks')->textInput(['disabled' => true])->label(false); ?></td>
    				</tr>
    				<tr>
      				<td align="center">4.</td>
      				<td>&nbsp;Examination Duration</td>
      				<td align="center"><?= $form->field($modelVettings[1], '[1]examination_duration_yn')->radio(array('label' => false, 'value' => 'Yes', 'uncheck' => null, 'disabled' => true)); ?></td>
      				<td align="center"><?= $form->field($modelVettings[1], '[1]examination_duration_yn')->radio(array('label' => false, 'value' => 'No', 'uncheck' => null, 'disabled' => true)); ?></td>
      				<td><?= $form->field($modelVettings[1], '[1]examination_duration_remarks')->textInput(['disabled' => true])->label(false); ?></td>
    				</tr>
    				<tr>
      				<td align="center">5.</td>
      				<td>&nbsp;Cover page instructions</td>
      				<td align="center"><?= $form->field($modelVettings[1], '[1]cover_page_instructions_yn')->radio(array('label' => false, 'value' => 'Yes', 'uncheck' => null, 'disabled' => true)); ?></td>
      				<td align="center"><?= $form->field($modelVettings[1], '[1]cover_page_instructions_yn')->radio(array('label' => false, 'value' => 'No', 'uncheck' => null, 'disabled' => true)); ?></td>
      				<td><?= $form->field($modelVettings[1], '[1]cover_page_instructions_remarks')->textInput(['disabled' => true])->label(false); ?></td>
    				</tr>
    				<tr>
      				<td align="center">6.</td>
      				<td>&nbsp;Question's instruction / Parts</td>
      				<td align="center"><?= $form->field($modelVettings[1], '[1]questions_instruction_parts_yn')->radio(array('label' => false, 'value' => 'Yes', 'uncheck' => null, 'disabled' => true)); ?></td>
      				<td align="center"><?= $form->field($modelVettings[1], '[1]questions_instruction_parts_yn')->radio(array('label' => false, 'value' => 'No', 'uncheck' => null, 'disabled' => true)); ?></td>
      				<td><?= $form->field($modelVettings[1], '[1]questions_instruction_parts_remarks')->textInput(['disabled' => true])->label(false); ?></td>
    				</tr>
    				<tr>
      				<td align="center">7.</td>
      				<td>&nbsp;Marks distribution for each marks</td>
      				<td align="center"><?= $form->field($modelVettings[1], '[1]mark_distribution_yn')->radio(array('label' => false, 'value' => 'Yes', 'uncheck' => null, 'disabled' => true)); ?></td>
      				<td align="center"><?= $form->field($modelVettings[1], '[1]mark_distribution_yn')->radio(array('label' => false, 'value' => 'No', 'uncheck' => null, 'disabled' => true)); ?></td>
      				<td><?= $form->field($modelVettings[1], '[1]mark_distribution_remarks')->textInput(['disabled' => true])->label(false); ?></td>
    				</tr>
    				<tr>
      				<td align="center">8.</td>
      				<td>&nbsp;Number of each page</td>
      				<td align="center"><?= $form->field($modelVettings[1], '[1]page_number_yn')->radio(array('label' => false, 'value' => 'Yes', 'uncheck' => null, 'disabled' => true)); ?></td>
      				<td align="center"><?= $form->field($modelVettings[1], '[1]page_number_yn')->radio(array('label' => false, 'value' => 'No', 'uncheck' => null, 'disabled' => true)); ?></td>
      				<td><?= $form->field($modelVettings[1], '[1]page_number_remarks')->textInput(['disabled' => true])->label(false); ?></td>
    				</tr>
    				<tr>
      				<td align="center">9.</td>
      				<td>&nbsp;Question's numbering system</td>
      				<td align="center"><?= $form->field($modelVettings[1], '[1]questions_numbering_yn')->radio(array('label' => false, 'value' => 'Yes', 'uncheck' => null, 'disabled' => true)); ?></td>
      				<td align="center"><?= $form->field($modelVettings[1], '[1]questions_numbering_yn')->radio(array('label' => false, 'value' => 'No', 'uncheck' => null, 'disabled' => true)); ?></td>
      				<td><?= $form->field($modelVettings[1], '[1]questions_numbering_remarks')->textInput(['disabled' => true])->label(false); ?></td>
    				</tr>
  				</tbody>
				</table>
				<br>
				<table align="center" width="80%" border="0">
					<tbody>
					<tr>
					<td><b><u>CONTENT</u></b></td>
					</tr>
					</tbody>
				</table>
				<table align="center" width="80%" border="1">
  				<tbody>
    				<tr>
					<th scope="col" width="5%"><p align="center">No.</p></th>
      				<th cope="col" width="30%"><p align="center">Contents</p></th>
      				<th scope="col" width="10%"><p align="center">Appropriate</p></th>
      				<th scope="col" width="10%"><p align="center">Not Appropriate</p></th>
      				<th scope="col" width="25%"><p align="center">Remarks</p></th>
    				</tr>
    				<tr>
      				<td align="center">1.</td>
      				<td>&nbsp;Illustrate the objectives of the course</td>
      				<td align="center"><?= $form->field($modelVettings[1], '[1]course_objectives_ana')->radio(array('label' => false, 'value' => 'Appropriate', 'uncheck' => null, 'disabled' => true)); ?></td>
      				<td align="center"><?= $form->field($modelVettings[1], '[1]course_objectives_ana')->radio(array('label' => false, 'value' => 'Not Appropriate', 'uncheck' => null, 'disabled' => true)); ?></td>
      				<td><?= $form->field($modelVettings[1], '[1]course_objectives_remarks')->textInput(['disabled' => true])->label(false); ?></td>
    				</tr>
    				<tr>
      				<td align="center">2.</td>
      				<td>&nbsp;Fulfill course synopsis/content</td>
      				<td align="center"><?= $form->field($modelVettings[1], '[1]course_synopsis_content_ana')->radio(array('label' => false, 'value' => 'Appropriate', 'uncheck' => null, 'disabled' => true)); ?></td>
      				<td align="center"><?= $form->field($modelVettings[1], '[1]course_synopsis_content_ana')->radio(array('label' => false, 'value' => 'Not Appropriate', 'uncheck' => null, 'disabled' => true)); ?></td>
      				<td><?= $form->field($modelVettings[1], '[1]course_synopsis_content_remarks')->textInput(['disabled' => true])->label(false); ?></td>
    				</tr>
    				<tr>
      				<td align="center">3.</td>
      				<td>&nbsp;Allocated time given</td>
      				<td align="center"><?= $form->field($modelVettings[1], '[1]time_given_ana')->radio(array('label' => false, 'value' => 'Appropriate', 'uncheck' => null, 'disabled' => true)); ?></td>
      				<td align="center"><?= $form->field($modelVettings[1], '[1]time_given_ana')->radio(array('label' => false, 'value' => 'Not Appropriate', 'uncheck' => null, 'disabled' => true)); ?></td>
      				<td><?= $form->field($modelVettings[1], '[1]time_given_remarks')->textInput(['disabled' => true])->label(false); ?></td>
    				</tr>
    				<tr>
      				<td align="center">4.</td>
      				<td>&nbsp;Language standard/spelling/term</td>
      				<td align="center"><?= $form->field($modelVettings[1], '[1]language_standard_ana')->radio(array('label' => false, 'value' => 'Appropriate', 'uncheck' => null, 'disabled' => true)); ?></td>
      				<td align="center"><?= $form->field($modelVettings[1], '[1]language_standard_ana')->radio(array('label' => false, 'value' => 'Not Appropriate', 'uncheck' => null, 'disabled' => true)); ?></td>
      				<td><?= $form->field($modelVettings[1], '[1]language_standard_remarks')->textInput(['disabled' => true])->label(false); ?></td>
    				</tr>
  				</tbody>
				</table>
				<br>
				<table align="center" width="80%" border="0">
  				<tbody>
    				<tr>
      				<td>Comment:</td>
    				</tr>
    				<tr>
      				<td><?= $form->field($modelVettings[1], '[1]comment')->textInput(['disabled' => true])->label(false); ?></td>
    				</tr>
  				</tbody>
				</table>
				<br>
				<table align="center" width="80%" border="0">
					<tbody>
					<tr>
					<td><b>Declaration:</b></td>
					</tr>
					</tbody>
				</table>
				<table align="center" width="80%" border="0">
  				<tbody>
    				<tr>
      				<td width="5%" align="center">1)</td>
      				<td width="10%" align="center"><?= $form->field($modelVettings[1], '[1]declaration')->radio(array('label' => false, 'value' => '1', 'uncheck' => null, 'disabled' => true)); ?></td>
      				<td width="65%">We have checked this question paper and declare that it is appropriate to be printed out.</td>
    				</tr>
    				<tr>
      				<td align="center">2)</td>
      				<td align="center"><?= $form->field($modelVettings[1], '[1]declaration')->radio(array('label' => false, 'value' => '2', 'uncheck' => null, 'disabled' => true)); ?></td>
      				<td>We have checked this question paper and declare that it is appropriate to be printed out in accordance with the corrections that have been suggested.
					</td>
    				</tr>
    				<tr>
      				<td align="center">3)</td>
      				<td align="center"><?= $form->field($modelVettings[1], '[1]declaration')->radio(array('label' => false, 'value' => '3', 'uncheck' => null, 'disabled' => true)); ?></td>
      				<td>We have checked this question paper and declare that it is not appropriate to be printed out.</td>
    				</tr>
  				</tbody>
				</table>
				<br>
				<table align="center" width="80%" border="0">
				<tbody>
					<tr>
					<th scope="col" colspan="3"><p align="center">Examination Question Vetting Committee</p></th>
					<th width="20%" scope="col"><p align="center">Signature</p></th>
					</tr>
					<tr>
					<td width="15%">&nbsp;Head</td>
					<td width="1%">:</td>
					<td width="44%"><?= $form->field($modelVettings[1], '[1]head')->textInput(['disabled' => true])->label(false); ?></td>
					<td align="center"><?= $form->field($modelVettings[1], '[1]signature_1')->checkbox(array('label' => false, 'disabled' => true)); ?></td>
					</tr>
					<tr>
					<td>&nbsp;Members</td>
					<td>:</td>
					<td><?= $form->field($modelVettings[1], '[1]members')->textInput(['disabled' => true])->label(false); ?></td>
					<td align="center"><?= $form->field($modelVettings[1], '[1]signature_2')->checkbox(array('label' => false, 'disabled' => true)); ?></td>
					</tr>
					<tr>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					</tr>
					<tr>
					<td>&nbsp;Date</td>
					<td>:</td>
					<td>
						<?= $form->field($modelVettings[1], '[1]date_1')->widget(DatePicker::classname(), [
							'disabled' => true,
							'type' => DatePicker::TYPE_INPUT,
							'pluginOptions' => [
								'autoclose' => true,
								'format' => 'dd/mm/yyyy'
							]
						])->label(false); ?>
					</td>
					<td>&nbsp;</td>
					</tr>
				</tbody>
				</table>
				<p align="center">______________________________________________________________________________________________</p>
				<table align="center" width="80%" border="0">
					<tbody>
					<tr>
					<td><b>Declaration to accept the amendment of the question paper :</b></td>
					</tr>
					</tbody>
				</table>
				<table align="center" width="80%" border="0">
				<tbody>
					<tr>
					  <td colspan="7">&nbsp;I have checked and declare that this question paper has been corrected and appropriate to be printed out.</td>
					</tr>
					<tr>
					  <td width="10%">&nbsp;Signature</td>
					  <td width="1%">:</td>
					  <td width="20%" align="center"><?= $form->field($modelVettings[1], '[1]signature_3')->checkbox(array('label' => false, 'disabled' => true)); ?></td>
					  <td width="18%">&nbsp;</td>
					  <td width="10%">&nbsp;Date</td>
					  <td width="1%">:</td>
					  <td width="20%">
					  	<?= $form->field($modelVettings[1], '[1]date_2')->widget(DatePicker::classname(), [
							'disabled' => true,
							'type' => DatePicker::TYPE_INPUT,
							'pluginOptions' => [
								'autoclose' => true,
								'format' => 'dd/mm/yyyy'
							]
						])->label(false); ?>
					</td>
					</tr>
					<tr>
					  <td>&nbsp;</td>
					  <td>&nbsp;</td>
					  <td align="center"><b>Head Examination Question Vetting</b></td>
					  <td>&nbsp;</td>
					  <td>&nbsp;</td>
					  <td>&nbsp;</td>
					  <td>&nbsp;</td>
					</tr>
				</tbody>
				</table>
				<br>
				<br>
			    </td>
    		</tr>
  		</tbody>
	</table>
   	
	<br>
  	
  	<h4 align="center"><b>The form will be submit to Admin.</b></h4>
  	
  	<br>

    <div class="form-group" style="text-align: center">
        <?= Html::submitButton('Submit', ['data-idform' => 'submission_form', 'class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

    <?php Pjax::end(); ?>

</div>

<?php

$urlCourseCodeName = Url::to(['get-course-code-name']);

$js = <<< JS

    $(document).ready(function(){

        $("#submission-course").change(function() {

			var id_course = $("#submission-course").val();
			
			var request = $.ajax({
				url: "$urlCourseCodeName",
				method: "POST",
				data: {course: id_course},
				dataType: "json"
			});

			request.done(function(data) {
				
				$("#vetting-course-code").val(data.code);
				$("#vetting-course-name").val(data.name);
				$("#vetting2-course-code").val(data.code);
				$("#vetting2-course-name").val(data.name);
			});

			request.fail(function(jqXHR, textStatus) {

				console.log(jqXHR);
				alert("Request failed: " + textStatus);
			}); 
		});

		$("#vetting-session").change(function() {

			$("#vetting2-session").val($("#vetting-session").val()).change();
		});

		$("#vetting-examination").change(function() {

			$("#vetting2-examination").val($("#vetting-examination").val());
		});
    });

JS;
$this->registerJs($js);
?>