<?php

use yii\helpers\Html;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel common\models\VettingTimetableSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Vetting Timetable';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="attachment-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <?php Pjax::begin(); ?>

    <?php if (!empty($vettingTimetable['path'])) { ?>

    <table width="100%" border="0">
  		<tbody>
    	<tr>
     		<td align="center"><object width="1000" height="750" data="<?= Yii::$app->request->baseUrl ?><?= $vettingTimetable['path']; ?>"></td>
    	</tr>
  		</tbody>
	</table>

    <?php } ?>

    <?php Pjax::end(); ?>

</div>
